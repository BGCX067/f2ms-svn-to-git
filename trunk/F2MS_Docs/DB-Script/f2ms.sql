delimiter $$;

CREATE DATABASE `f2ms` /*!40100 DEFAULT CHARACTER SET utf8 */$$;

CREATE TABLE `booking` (
  `Doc_No` varchar(30) NOT NULL,
  `State` bigint(20) NOT NULL,
  `Shipment_Type` varchar(2) DEFAULT NULL,
  `Transshipment_Via` varchar(2) DEFAULT NULL,
  `Shipper_Id` bigint(20) DEFAULT NULL,
  `Consignee_Id` bigint(20) DEFAULT NULL,
  `Place_Of_Receipt_Id` bigint(20) DEFAULT NULL,
  `POL_Id` bigint(20) DEFAULT NULL,
  `POD_Id` bigint(20) DEFAULT NULL,
  `Final_Destination_Id` bigint(20) DEFAULT NULL,
  `Goods_Desc` varchar(500) DEFAULT NULL,
  `Mark_And_No` varchar(500) DEFAULT NULL,
  `Inner_Pkg_No` int(11) DEFAULT NULL,
  `Inner_Pkg_Kind` int(11) DEFAULT NULL,
  `Outer_Pkg_No` int(11) DEFAULT NULL,
  `Outer_Pkg_Kind` int(11) DEFAULT NULL,
  `Gross_Weight` decimal(15,3) DEFAULT NULL,
  `Measurement` decimal(15,3) DEFAULT NULL,
  `HSCode` varchar(30) DEFAULT NULL,
  `Permit_No` varchar(30) DEFAULT NULL,
  `Remark` varchar(500) DEFAULT NULL,
  `Status` varchar(2) DEFAULT NULL,
  `Currency_Id` varchar(2) DEFAULT NULL,
  `Booking_Type` varchar(1) NOT NULL,
  `CreatedBy` varchar(20) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `ChangedBy` varchar(20) NOT NULL,
  `ChangedOn` datetime NOT NULL,
  `NeedPickup` int(11) DEFAULT NULL,
  `ShipmentMode` bigint(20) DEFAULT NULL,
  `GoodsValue` decimal(15,3) DEFAULT NULL,
  `GoodsValueCurrency` int(1) DEFAULT NULL,
  `Warehouse` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`Doc_No`),
  UNIQUE KEY `Booking_No_UNIQUE` (`Doc_No`)
)  ENGINE=InnoDB DEFAULT CHARSET=utf8$$;

CREATE TABLE `charges` (
  `IdNo` bigint(20) NOT NULL AUTO_INCREMENT,
  `State` bigint(20) NOT NULL,
  `Charge_Type` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Effective_Date` date DEFAULT NULL,
  `Expiry_Date` date DEFAULT NULL,
  `Fee` double DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `Remarks` varchar(255) DEFAULT NULL,
  `CreatedBy` varchar(255) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `ChangedBy` varchar(255) DEFAULT NULL,
  `ChangedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`IdNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8$$;

CREATE TABLE `customer` (
  `Customer_Code` bigint(20) NOT NULL AUTO_INCREMENT,
  `State` bigint(20) NOT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `AddressLine1` varchar(255) DEFAULT NULL,
  `AddressLine2` varchar(255) DEFAULT NULL,
  `HPTel` varchar(255) DEFAULT NULL,
  `WorkTel` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Id_Type` varchar(255) DEFAULT NULL,
  `Id_No` varchar(255) DEFAULT NULL,
  `CreatedBy` varchar(255) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `ChangedBy` varchar(255) DEFAULT NULL,
  `ChangedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Customer_Code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8$$;


CREATE TABLE `deliveryorder` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `State` bigint(20) NOT NULL,
  `BookingNo` varchar(100) DEFAULT NULL,
  `DocNo` varchar(120) DEFAULT NULL,
  `DocDate` datetime DEFAULT NULL,
  `TruckerId` int(11) DEFAULT NULL,
  `DeliveryTo` int(11) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `StatusReason` varchar(255) DEFAULT NULL COMMENT 'Use for Cancellation Reason\n',
  `DeliveryAddressLine1` varchar(255) DEFAULT NULL,
  `DeliveryAddressLine2` varchar(255) DEFAULT NULL,
  `Remarks` varchar(255) DEFAULT NULL,
  `CreatedBy` varchar(255) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `ChangedBy` varchar(255) DEFAULT NULL,
  `ChangedOn` datetime DEFAULT NULL,
  `DeliveryAddressLine3` varchar(255) DEFAULT NULL,
  `DeliveryAddressLine4` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1$$

CREATE TABLE `location` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `State` bigint(20) NOT NULL,
  `Code` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `Remarks` varchar(255) DEFAULT NULL,
  `CreatedBy` varchar(255) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `ChangedBy` varchar(255) DEFAULT NULL,
  `ChangedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8$$;

CREATE TABLE `pickuporder` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `State` bigint(20) NOT NULL,
  `DocNo` varchar(15) DEFAULT NULL,
  `PickUpDate` datetime DEFAULT NULL,
  `BookingNo` varchar(30) DEFAULT NULL,
  `TruckerId` int(11) DEFAULT NULL,
  `Mode` int(11) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `StatusReason` varchar(255) DEFAULT NULL COMMENT 'Use for Cancellation Reason\n',
  `WarehouseId` bigint(20) DEFAULT NULL,
  `Remarks` varchar(255) DEFAULT NULL,
  `CreatedBy` varchar(255) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `ChangedBy` varchar(255) DEFAULT NULL,
  `ChangedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8$$;


CREATE TABLE `receiveorder` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `State` bigint(20) NOT NULL,
  `DocNo` varchar(15) DEFAULT NULL,
  `DocDate` datetime DEFAULT NULL,
  `BookingNo` varchar(30) DEFAULT NULL,
  `ReceivedFrom` bigint(20) DEFAULT NULL,
  `ReceivedBy` bigint(20) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `StatusReason` varchar(255) DEFAULT NULL COMMENT 'Use for Cancellation Reason\n',
  `Description` varchar(255) DEFAULT NULL,
  `DeliveredTo` bigint(20) DEFAULT NULL,
  `DeliveredAddressLine` varchar(255) DEFAULT NULL,
  `ShipperRefNo` varchar(50) DEFAULT NULL,
  `Quantity` int(11) DEFAULT NULL,
  `Volume` decimal(10,2) DEFAULT NULL,
  `Weight` decimal(10,2) DEFAULT NULL,
  `Marking` varchar(50) DEFAULT NULL,
  `Remarks` varchar(255) DEFAULT NULL,
  `CreatedBy` varchar(255) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `ChangedBy` varchar(255) DEFAULT NULL,
  `ChangedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKD2E26A4BA8262E1F` (`BookingNo`),
  KEY `FKD2E26A4B2D6B247D` (`ReceivedFrom`),
  KEY `FKD2E26A4B67A1FD6A` (`ReceivedBy`),
  CONSTRAINT `FKD2E26A4B2D6B247D` FOREIGN KEY (`ReceivedFrom`) REFERENCES `customer` (`Customer_Code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FKD2E26A4B67A1FD6A` FOREIGN KEY (`ReceivedBy`) REFERENCES `customer` (`Customer_Code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FKD2E26A4BA8262E1F` FOREIGN KEY (`BookingNo`) REFERENCES `booking` (`Doc_No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8$$;

CREATE TABLE `shipmentmode` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `State` bigint(20) NOT NULL,
  `Code` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `Remarks` varchar(255) DEFAULT NULL,
  `CreatedBy` varchar(255) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `ChangedBy` varchar(255) DEFAULT NULL,
  `ChangedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8$$;


CREATE TABLE `warehouse` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `State` bigint(20) NOT NULL,
  `Code` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Address` varchar(255) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `Remarks` varchar(255) DEFAULT NULL,
  `CreatedBy` varchar(255) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `ChangedBy` varchar(255) DEFAULT NULL,
  `ChangedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8$$;


CREATE TABLE `role` (
  `RoleCode` varchar(25) NOT NULL,
  `Description` varchar(60) NOT NULL,
  PRIMARY KEY (`RoleCode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$


CREATE TABLE `userrole` (
  `UserName` varchar(25) NOT NULL,
  `RoleCode` varchar(25) NOT NULL,
  PRIMARY KEY (`UserName`,`RoleCode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$


CREATE TABLE `users` (
  `IDNo` bigint(20) NOT NULL AUTO_INCREMENT,
  `State` bigint(20) DEFAULT NULL,
  `UserName` varchar(30) DEFAULT NULL,
  `Password` varchar(30) DEFAULT NULL,
  `StaffCode` varchar(10) DEFAULT NULL,
  `Status` varchar(1) DEFAULT NULL,
  `CreatedBy` varchar(25) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `ChangedBy` varchar(25) NOT NULL,
  `ChangedOn` datetime NOT NULL,
  PRIMARY KEY (`IDNo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$


CREATE TABLE `staff` (
  `StaffCode` bigint(20) NOT NULL,
  `Name` varchar(60) DEFAULT NULL,
  `AddressLine1` varchar(45) DEFAULT NULL,
  `AddressLine2` varchar(45) DEFAULT NULL,
  `HPTel` varchar(20) DEFAULT NULL,
  `WorkTel` varchar(20) DEFAULT NULL,
  `Email` varchar(25) DEFAULT NULL,
  `Designation` varchar(45) DEFAULT NULL,
  `CreatedBy` varchar(30) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `ChangedBy` varchar(30) DEFAULT NULL,
  `ChangedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`StaffCode`)
)


CREATE TABLE `cargotype` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Code` varchar(4) NOT NULL,
  `State` bigint(20) NOT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `Remarks` varchar(255) DEFAULT NULL,
  `CreatedBy` varchar(255) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `ChangedBy` varchar(255) DEFAULT NULL,
  `ChangedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB;



CREATE TABLE `shipment` (
  `ShipmentNo` varchar(20) NOT NULL,
  `State` bigint(20) NOT NULL,
  `PackagingDate` datetime DEFAULT NULL,
  `Warehouse` bigint(20) DEFAULT NULL,
  `ShipmentType` int(11) DEFAULT NULL,
  `ShipmentMode` int(11) DEFAULT NULL,
  `Consignee` bigint(20) DEFAULT NULL,
  `Carrier` bigint(20) DEFAULT NULL,
  `VesselName` varchar(255) DEFAULT NULL,
  `VoyageNumber` int(11) DEFAULT NULL,
  `POL` bigint(20) DEFAULT NULL,
  `POD` bigint(20) DEFAULT NULL,
  `ShipmentDate` datetime DEFAULT NULL,
  `ContainerNo` int(11) DEFAULT NULL,
  `ContainerType` int(11) DEFAULT NULL,
  `MaximumLoad` double DEFAULT NULL,
  `MaximumMeasurement` double DEFAULT NULL,
  `TotalVolume` double DEFAULT NULL,
  `TotalWeight` double DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `StatusReason` varchar(255) DEFAULT NULL,
  `Remarks` varchar(255) DEFAULT NULL,
  `CreatedBy` varchar(255) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `ChangedBy` varchar(255) DEFAULT NULL,
  `ChangedOn` datetime DEFAULT NULL,
  `Id` bigint(20) NOT NULL,
  `PolID` bigint(20) DEFAULT NULL,
  `PodID` bigint(20) DEFAULT NULL,
  `WarehouseId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ShipmentNo`),
  KEY `idx_fk_warehouse` (`Warehouse`),
  KEY `idx_fk_consignee` (`Consignee`),
  KEY `idx_fk_carrier` (`Carrier`),
  KEY `idx_fk_pol` (`POL`),
  KEY `idx_fk_pod` (`POD`),
  KEY `FKE2DB77633CE2D4CD` (`WarehouseId`),
  KEY `FKE2DB7763AEB55D69` (`PodID`),
  KEY `FKE2DB7763AEB57B71` (`PolID`),
  CONSTRAINT `FKE2DB77633CE2D4CD` FOREIGN KEY (`WarehouseId`) REFERENCES `warehouse` (`Id`),
  CONSTRAINT `FKE2DB7763AEB55D69` FOREIGN KEY (`PodID`) REFERENCES `location` (`Id`),
  CONSTRAINT `FKE2DB7763AEB57B71` FOREIGN KEY (`PolID`) REFERENCES `location` (`Id`),
  CONSTRAINT `idx_fk_carrier` FOREIGN KEY (`Carrier`) REFERENCES `customer` (`Customer_Code`),
  CONSTRAINT `idx_fk_consignee` FOREIGN KEY (`Consignee`) REFERENCES `customer` (`Customer_Code`),
  CONSTRAINT `idx_fk_pod` FOREIGN KEY (`POD`) REFERENCES `location` (`Id`),
  CONSTRAINT `idx_fk_pol` FOREIGN KEY (`POL`) REFERENCES `location` (`Id`),
  CONSTRAINT `idx_fk_warehouse` FOREIGN KEY (`Warehouse`) REFERENCES `warehouse` (`Id`)
) ENGINE=InnoDB;


CREATE TABLE `shipmentitem` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ShipmentNo` varchar(20) NOT NULL,
  `BookingNo` varchar(30) NOT NULL,
  `State` bigint(20) NOT NULL,
  `ExportCertDate` datetime DEFAULT NULL,
  `ShipmentPermitNo` varchar(255) DEFAULT NULL,
  `HSCode` varchar(255) DEFAULT NULL,
  `Marking` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `CreatedBy` varchar(255) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `ChangedBy` varchar(255) DEFAULT NULL,
  `ChangedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `idx_fk_shipment` (`ShipmentNo`),
  KEY `idx_fk_booking` (`BookingNo`),
  CONSTRAINT `idx_fk_booking` FOREIGN KEY (`BookingNo`) REFERENCES `booking` (`Doc_No`),
  CONSTRAINT `idx_fk_shipment` FOREIGN KEY (`ShipmentNo`) REFERENCES `packaging` (`ShipmentNo`)
) ENGINE=InnoDB;

delimiter $$

CREATE TABLE `bookingfiles` (
  `IdNo` bigint(20) NOT NULL AUTO_INCREMENT,
  `BookingNo` varchar(100) NOT NULL,
  `ContentType` varchar(45) NOT NULL,
  `FilePath` varchar(200) NOT NULL,
  `FileName` varchar(100) NOT NULL,
  `FileSize` varchar(100) NOT NULL,
  PRIMARY KEY (`IdNo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$












