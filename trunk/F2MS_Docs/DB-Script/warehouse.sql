delimiter $$

CREATE TABLE `warehouse` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `State` bigint(20) DEFAULT NULL,
  `Code` varchar(10) DEFAULT NULL,
  `Description` varchar(250) DEFAULT NULL,
  `Address` varchar(500) DEFAULT NULL,
  `Status` tinyint(4) DEFAULT NULL,
  `Remarks` text,
  `CreatedBy` varchar(20) DEFAULT NULL,
  `CreatedOn` timestamp NULL DEFAULT NULL,
  `ChangedBy` varchar(20) DEFAULT NULL,
  `ChangedOn` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Code_UNIQUE` (`Code`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8$$

