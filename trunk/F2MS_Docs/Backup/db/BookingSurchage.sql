USE [F2MS-DB]
GO

/****** Object:  Table [dbo].[BookingSurcharge]    Script Date: 10/23/2010 16:12:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[BookingSurcharge](
	[RowID] [bigint] IDENTITY(1,1) NOT NULL,
	[State] [bigint] NOT NULL,
	[CreatedBy] [varchar](30) NULL,
	[CreatedOn] [datetime] NULL,
	[changedBy] [varchar](30) NULL,
	[changedon] [varchar](10) NULL,
	[BookingID] [bigint] NULL,
	[SurchargeID] [bigint] NULL,
	[Basis] [decimal](15, 3) NULL,
	[Rate] [decimal](15, 3) NULL,
	[Amount] [decimal](15, 3) NULL,
 CONSTRAINT [BookingSurcharge_PK] PRIMARY KEY CLUSTERED 
(
	[RowID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[BookingSurcharge]  WITH CHECK ADD  CONSTRAINT [BookingSurcharge_FK1] FOREIGN KEY([BookingID])
REFERENCES [dbo].[Booking] ([RowID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[BookingSurcharge] CHECK CONSTRAINT [BookingSurcharge_FK1]
GO

ALTER TABLE [dbo].[BookingSurcharge]  WITH CHECK ADD  CONSTRAINT [BookingSurcharge_FK2] FOREIGN KEY([SurchargeID])
REFERENCES [dbo].[Surcharge] ([RowID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[BookingSurcharge] CHECK CONSTRAINT [BookingSurcharge_FK2]
GO

