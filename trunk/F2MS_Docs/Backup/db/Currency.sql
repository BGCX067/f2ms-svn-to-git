USE [F2MS-DB]
GO

/****** Object:  Table [dbo].[Currency]    Script Date: 10/23/2010 16:13:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Currency](
	[RowID] [bigint] IDENTITY(1,1) NOT NULL,
	[State] [bigint] NOT NULL,
	[CreatedBy] [varchar](30) NULL,
	[CreatedOn] [datetime] NULL,
	[changedBy] [varchar](30) NULL,
	[changedon] [varchar](10) NULL,
	[Code] [varchar](5) NOT NULL,
	[Name] [varchar](60) NULL,
 CONSTRAINT [Currency_PK] PRIMARY KEY CLUSTERED 
(
	[RowID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

