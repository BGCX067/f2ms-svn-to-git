USE [F2MS-DB]
GO

/****** Object:  Table [dbo].[Booking]    Script Date: 10/23/2010 16:12:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Booking](
	[RowID] [bigint] IDENTITY(1,1) NOT NULL,
	[State] [bigint] NOT NULL,
	[CreatedBy] [varchar](30) NULL,
	[CreatedOn] [datetime] NULL,
	[changedBy] [varchar](30) NULL,
	[changedon] [varchar](10) NULL,
	[DocumentNumber] [varchar](5) NOT NULL,
	[ShipmentType] [varchar](15) NULL,
	[TransshipmentVia] [varchar](15) NULL,
	[ShipperID] [bigint] NULL,
	[ConsigneeID] [bigint] NULL,
	[PlaceOfReceiptID] [bigint] NULL,
	[POLID] [bigint] NULL,
	[PODID] [bigint] NULL,
	[FinalDestinationID] [bigint] NULL,
	[DescOfGoods] [varchar](250) NULL,
	[MarkAndNumber] [varchar](250) NULL,
	[NrofPkgInner] [int] NULL,
	[NrofPkgOuter] [int] NULL,
	[KindOfPkgInner] [varchar](15) NULL,
	[KindOfPkgouter] [varchar](15) NULL,
	[GrossWeight] [decimal](15, 3) NULL,
	[Measurement] [decimal](15, 3) NULL,
	[HSCode] [varchar](30) NULL,
	[PermitNumber] [varchar](30) NULL,
	[Remark] [varchar](30) NULL,
	[Status] [varchar](10) NULL,
	[CurrencyID] [bigint] NULL,
 CONSTRAINT [Booking_PK] PRIMARY KEY CLUSTERED 
(
	[RowID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Booking]  WITH CHECK ADD  CONSTRAINT [Booking_FK1] FOREIGN KEY([ShipperID])
REFERENCES [dbo].[Customer] ([RowID])
GO

ALTER TABLE [dbo].[Booking] CHECK CONSTRAINT [Booking_FK1]
GO

ALTER TABLE [dbo].[Booking]  WITH CHECK ADD  CONSTRAINT [Booking_FK2] FOREIGN KEY([ConsigneeID])
REFERENCES [dbo].[Customer] ([RowID])
GO

ALTER TABLE [dbo].[Booking] CHECK CONSTRAINT [Booking_FK2]
GO

ALTER TABLE [dbo].[Booking]  WITH CHECK ADD  CONSTRAINT [Booking_FK3] FOREIGN KEY([PlaceOfReceiptID])
REFERENCES [dbo].[Location] ([RowID])
GO

ALTER TABLE [dbo].[Booking] CHECK CONSTRAINT [Booking_FK3]
GO

ALTER TABLE [dbo].[Booking]  WITH CHECK ADD  CONSTRAINT [Booking_FK4] FOREIGN KEY([POLID])
REFERENCES [dbo].[Location] ([RowID])
GO

ALTER TABLE [dbo].[Booking] CHECK CONSTRAINT [Booking_FK4]
GO

ALTER TABLE [dbo].[Booking]  WITH CHECK ADD  CONSTRAINT [Booking_FK5] FOREIGN KEY([PODID])
REFERENCES [dbo].[Location] ([RowID])
GO

ALTER TABLE [dbo].[Booking] CHECK CONSTRAINT [Booking_FK5]
GO

ALTER TABLE [dbo].[Booking]  WITH CHECK ADD  CONSTRAINT [Booking_FK6] FOREIGN KEY([FinalDestinationID])
REFERENCES [dbo].[Location] ([RowID])
GO

ALTER TABLE [dbo].[Booking] CHECK CONSTRAINT [Booking_FK6]
GO

ALTER TABLE [dbo].[Booking]  WITH CHECK ADD  CONSTRAINT [Booking_FK7] FOREIGN KEY([CurrencyID])
REFERENCES [dbo].[Currency] ([RowID])
GO

ALTER TABLE [dbo].[Booking] CHECK CONSTRAINT [Booking_FK7]
GO

