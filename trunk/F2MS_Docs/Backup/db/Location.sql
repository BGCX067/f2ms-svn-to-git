USE [F2MS-DB]
GO

/****** Object:  Table [dbo].[Location]    Script Date: 10/23/2010 16:13:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Location](
	[RowID] [bigint] IDENTITY(1,1) NOT NULL,
	[State] [bigint] NOT NULL,
	[CreatedBy] [varchar](30) NULL,
	[CreatedOn] [datetime] NULL,
	[changedBy] [varchar](30) NULL,
	[changedon] [varchar](10) NULL,
	[Code] [varchar](10) NOT NULL,
	[Name] [varchar](60) NULL,
	[Type] [varchar](10) NULL,
	[AddressLine1] [varchar](60) NULL,
	[AddressLine2] [varchar](60) NULL,
	[AddressLine3] [varchar](60) NULL,
	[AddressLine4] [varchar](60) NULL,
	[AddressLine5] [varchar](60) NULL,
 CONSTRAINT [Location_PK] PRIMARY KEY CLUSTERED 
(
	[RowID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

