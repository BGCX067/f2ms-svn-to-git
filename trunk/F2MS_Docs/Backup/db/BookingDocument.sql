USE [F2MS-DB]
GO

/****** Object:  Table [dbo].[BookingDocument]    Script Date: 10/23/2010 16:12:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[BookingDocument](
	[RowID] [bigint] IDENTITY(1,1) NOT NULL,
	[State] [bigint] NOT NULL,
	[CreatedBy] [varchar](30) NULL,
	[CreatedOn] [datetime] NULL,
	[changedBy] [varchar](30) NULL,
	[changedon] [varchar](10) NULL,
	[BookingID] [bigint] NULL,
	[DocumentName] [varchar](120) NULL,
	[InternalLocation] [varchar](120) NULL,
 CONSTRAINT [BookingDocument_PK] PRIMARY KEY CLUSTERED 
(
	[RowID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[BookingDocument]  WITH CHECK ADD  CONSTRAINT [BookingDocument_FK1] FOREIGN KEY([BookingID])
REFERENCES [dbo].[Booking] ([RowID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[BookingDocument] CHECK CONSTRAINT [BookingDocument_FK1]
GO

