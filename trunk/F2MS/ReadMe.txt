

Requirements

1. To use and run, you need to have 
        - Java installed on your machine (Recommended Java 1.6 or above)
        - MYSQL 5 installed on your machine
        - Tomcat 6.0 or above installed on your machine
2. To build the system, you need to have
        - Java installed on your machine (Recommended Java 1.6 or above)
        - Ant installed on your machine
        



How to build

1. Once you already have all the pre-requisities installed on your computer, just double click
