package com.f2ms.service.shipmentmode;

import java.util.List;

import com.f2ms.exception.DAOException;
import com.f2ms.model.ShipmentMode;

public interface IShipmentModeService {

    public List<ShipmentMode> findAllShipmentModes() throws DAOException;

    public ShipmentMode create(ShipmentMode shipmentMode) throws DAOException;

    public ShipmentMode edit(ShipmentMode shipmentMode) throws DAOException;

    public ShipmentMode findShipmentModeById(long shipmentModeId)
            throws DAOException;

    public List<ShipmentMode> findShipmentModeByCriteria(
            ShipmentMode shipmentMode) throws DAOException;
}
