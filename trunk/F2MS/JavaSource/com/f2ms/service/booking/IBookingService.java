package com.f2ms.service.booking;

import java.util.Date;
import java.util.List;

import com.f2ms.exception.DAOException;
import com.f2ms.model.Booking;
import com.f2ms.model.BookingFiles;

public interface IBookingService {

	public Booking createBooking(Booking booking) throws DAOException;

	public Booking editBooking(Booking booking) throws DAOException;
	
	public Booking editBookingOnly(Booking booking) throws DAOException;

	public List<Booking> findAllBooking() throws DAOException;

	public Booking findBookingById(String bookingNo) throws DAOException;

	public List<Booking> findBookingByCriteria(Booking booking)
			throws DAOException;

	public List<Booking> findAvailableBooking(Date startPeriod, Date endPeriod,
			Long pol, Long pod, Long customerCode) throws DAOException;
	
	public BookingFiles createBookingFiles(BookingFiles bookingFiles) throws DAOException;
	
	public List<BookingFiles> findBookingFilesByBookingNo(String bookingNo) throws DAOException;
	
	public void deleteBookingFiles(BookingFiles bookingFile) throws DAOException;
	
	public BookingFiles findBookingFileById(Long id) throws DAOException;
}
