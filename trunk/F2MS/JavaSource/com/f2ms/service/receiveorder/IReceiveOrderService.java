package com.f2ms.service.receiveorder;

import java.util.List;

import com.f2ms.exception.DAOException;
import com.f2ms.model.ReceiveOrder;

public interface IReceiveOrderService {

    public List<ReceiveOrder> findAllReceiveOrders() throws DAOException;

    public ReceiveOrder create(ReceiveOrder receiveOrder) throws DAOException;

    public ReceiveOrder edit(ReceiveOrder receiveOrder) throws DAOException;

    public ReceiveOrder findReceiveOrderById(long receiveOrderId)
            throws DAOException;

    public List<ReceiveOrder> findReceiveOrderByCriteria(
            ReceiveOrder receiveOrder) throws DAOException;
}
