package com.f2ms.service.receiveorder;

import java.util.List;

import com.f2ms.dao.DAOFactory;
import com.f2ms.dao.booking.IBookingDAO;
import com.f2ms.dao.receiveorder.IReceiveOrderDAO;
import com.f2ms.enumeration.BookingStatus;
import com.f2ms.enumeration.DocumentStatus;
import com.f2ms.exception.DAOException;
import com.f2ms.model.Booking;
import com.f2ms.model.ReceiveOrder;

public class ReceiveOrderServiceImpl implements IReceiveOrderService {

	private DAOFactory daoFactory;

	public void setDaoFactory(DAOFactory daoFactory) {
		this.daoFactory = daoFactory;
	}

	@Override
	public List<ReceiveOrder> findAllReceiveOrders() throws DAOException {
		return daoFactory.getReceiveOrderDAO().findAllReceiveOrders();
	}

	@Override
	public ReceiveOrder create(ReceiveOrder receiveOrder) throws DAOException {

		validateReceiveOrder(receiveOrder);

		IReceiveOrderDAO receiveOrderDAO = daoFactory.getReceiveOrderDAO();
		ReceiveOrder newReceiveOrder = receiveOrderDAO.create(receiveOrder);

		String docNo = receiveOrder.getBooking().getDocNo();
		updateBookingStatus(docNo);

		return newReceiveOrder;
	}

	@Override
	public ReceiveOrder edit(ReceiveOrder receiveOrder) throws DAOException {
		try {
			IReceiveOrderDAO receiveOrderDAO = daoFactory.getReceiveOrderDAO();
			ReceiveOrder updatedReceiveOrder = receiveOrderDAO
					.edit(receiveOrder);

			return updatedReceiveOrder;
		} catch (DAOException daoEx) {

		}

		return null;
	}

	@Override
	public ReceiveOrder findReceiveOrderById(long receiveOrderId)
			throws DAOException {
		return daoFactory.getReceiveOrderDAO().findReceiveOrderById(
				receiveOrderId);
	}

	@Override
	public List<ReceiveOrder> findReceiveOrderByCriteria(
			ReceiveOrder receiveOrder) throws DAOException {
		return daoFactory.getReceiveOrderDAO().findReceiveOrderByCriteria(
				receiveOrder);
	}

	private void validateReceiveOrder(ReceiveOrder receiveOrder)
			throws DAOException {

		daoFactory.getCustomerDAO().findCustomerById(
				receiveOrder.getReceivedFrom().getCustCode());
		daoFactory.getCustomerDAO().findCustomerById(
				receiveOrder.getReceivedBy().getCustCode());
		daoFactory.getCustomerDAO().findCustomerById(
				receiveOrder.getDeliveredTo().getCustCode());

		if (receiveOrder.getStatus() == DocumentStatus.CANCELLED.getValue()
				&& "".equalsIgnoreCase(receiveOrder.getStatusReason())) {
			// throw error
		}

	}

	private void updateBookingStatus(String docNo) throws DAOException {
		IBookingDAO bookingDAO = daoFactory.getBookingDAO();
		Booking booking = bookingDAO.findBookingById(docNo);
		if (booking == null) {
			// booking is not available --> throw error
		} else {
			if (!String.valueOf(BookingStatus.NEW.getValue()).equalsIgnoreCase(
					booking.getStatus())) {
				// only new booking can be received otherwise throw error
			} else {
				booking.setStatus(String.valueOf(BookingStatus.RECEIVED
						.getValue()));
				bookingDAO.edit(booking);
			}
		}
	}
}
