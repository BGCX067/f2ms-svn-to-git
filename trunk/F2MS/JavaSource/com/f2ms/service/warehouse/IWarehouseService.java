package com.f2ms.service.warehouse;

import java.util.List;

import com.f2ms.exception.DAOException;
import com.f2ms.model.Warehouse;

public interface IWarehouseService {

    public List<Warehouse> findAllWarehouses() throws DAOException;

    public Warehouse create(Warehouse warehouse) throws DAOException;

    public Warehouse edit(Warehouse warehouse) throws DAOException;

    public Warehouse findWarehouseById(long warehouseId) throws DAOException;

    public List<Warehouse> findWarehouseByCriteria(Warehouse warehouse)
            throws DAOException;
}
