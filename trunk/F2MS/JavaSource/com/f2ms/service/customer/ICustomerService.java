package com.f2ms.service.customer;

import java.util.List;

import com.f2ms.exception.DAOException;
import com.f2ms.model.Customer;

public interface ICustomerService {

    public List<Customer> findAllCustomers() throws DAOException;

    public Customer create(Customer customer) throws DAOException;

    public Customer edit(Customer customer) throws DAOException;

    public Customer findCustomerById(long custCode) throws DAOException;

    public List<Customer> findCustomerByCriteria(Customer customer) throws DAOException;
}
