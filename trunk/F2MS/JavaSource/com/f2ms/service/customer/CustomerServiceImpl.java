package com.f2ms.service.customer;

import java.util.List;

import com.f2ms.dao.DAOFactory;
import com.f2ms.exception.DAOException;
import com.f2ms.model.Customer;

public class CustomerServiceImpl implements ICustomerService {

	private DAOFactory daoFactory;		

	public void setDaoFactory(DAOFactory daoFactory) {
		this.daoFactory = daoFactory;
	}

    public List<Customer> findAllCustomers() throws DAOException {
        return daoFactory.getCustomerDAO().findAllCustomers();
    }

    @Override
    public Customer create(Customer customer) throws DAOException {
        return daoFactory.getCustomerDAO().create(customer);
    }

    @Override
    public Customer edit(Customer customer) throws DAOException {
        return daoFactory.getCustomerDAO().edit(customer);
    }

    @Override
    public Customer findCustomerById(long custCode) throws DAOException {
        return daoFactory.getCustomerDAO().findCustomerById(custCode);
    }

    @Override
    public List<Customer> findCustomerByCriteria(Customer customer)
            throws DAOException {
        return daoFactory.getCustomerDAO().findCustomerByCriteria(customer);
    }
}
