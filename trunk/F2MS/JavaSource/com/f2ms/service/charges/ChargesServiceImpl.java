package com.f2ms.service.charges;

import java.util.List;

import com.f2ms.dao.DAOFactory;
import com.f2ms.exception.DAOException;
import com.f2ms.model.Charges;

public class ChargesServiceImpl implements IChargesService {
	private DAOFactory daoFactory;		

	public void setDaoFactory(DAOFactory daoFactory) {
		this.daoFactory = daoFactory;
	}

	@Override
	public Charges createCharge(Charges charge) throws DAOException {
		return daoFactory.getChargesDAO().create(charge);
	}

	@Override
	public Charges editCharge(Charges charge) throws DAOException {
		return daoFactory.getChargesDAO().edit(charge);
	}

	@Override
	public List<Charges> findChargesByCriteria(Charges charges)
			throws DAOException {
		return daoFactory.getChargesDAO().findChargesByCriteria(charges);
	}

	@Override
	public Charges findChargeById(Long id) throws DAOException { 
		return daoFactory.getChargesDAO().findChargesById(id);
	}
	
}
