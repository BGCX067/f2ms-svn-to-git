package com.f2ms.service.charges;

import java.util.List;

import com.f2ms.exception.DAOException;
import com.f2ms.model.Charges;

public interface IChargesService {
	public Charges createCharge(Charges charge) throws DAOException;
	public Charges editCharge(Charges charge) throws DAOException;
	public List<Charges> findChargesByCriteria(Charges charges) throws DAOException;
	public Charges findChargeById(Long id) throws DAOException;
}
