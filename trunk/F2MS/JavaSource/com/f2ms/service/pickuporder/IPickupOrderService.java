package com.f2ms.service.pickuporder;

import java.util.List;

import com.f2ms.exception.DAOException;
import com.f2ms.model.PickupOrder;

public interface IPickupOrderService {

    public List<PickupOrder> findAllPickupOrders() throws DAOException;

    public PickupOrder create(PickupOrder pickupOrder) throws DAOException;

    public PickupOrder edit(PickupOrder pickupOrder) throws DAOException;

    public PickupOrder findPickupOrderById(long pickupOrderId)
            throws DAOException;

    public List<PickupOrder> findPickupOrderByCriteria(PickupOrder pickupOrder)
            throws DAOException;
}
