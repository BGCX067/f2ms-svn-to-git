package com.f2ms.service.pickuporder;

import java.util.List;

import com.f2ms.dao.DAOFactory;
import com.f2ms.exception.DAOException;
import com.f2ms.model.Customer;
import com.f2ms.model.PickupOrder;
import com.f2ms.model.Warehouse;

public class PickupOrderServiceImpl implements IPickupOrderService {

	private DAOFactory daoFactory;

	public void setDaoFactory(DAOFactory daoFactory) {
		this.daoFactory = daoFactory;
	}

	@Override
	public List<PickupOrder> findAllPickupOrders() throws DAOException {
		return daoFactory.getPickupOrderDAO().findAllPickupOrders();
	}

	@Override
	public PickupOrder create(PickupOrder pickupOrder) throws DAOException {
		return daoFactory.getPickupOrderDAO().create(pickupOrder);
	}

	@Override
	public PickupOrder edit(PickupOrder pickupOrder) throws DAOException {
		return daoFactory.getPickupOrderDAO().edit(pickupOrder);
	}

	@Override
	public PickupOrder findPickupOrderById(long pickupOrderId)
			throws DAOException {

		// TODO: Need to refactor to use object relationship

		PickupOrder pickupOrder = daoFactory.getPickupOrderDAO()
				.findPickupOrderById(pickupOrderId);

		Customer customer = daoFactory.getCustomerDAO().findCustomerById(
				pickupOrder.getTruckerId());
		pickupOrder.setTruckerName(customer.getName());

		Warehouse warehouse = daoFactory.getWarehouseDAO().findWarehouseById(
				pickupOrder.getWarehouseId());
		pickupOrder.setWarehouseName(warehouse.getDescription());

		return pickupOrder;
	}

	@Override
	public List<PickupOrder> findPickupOrderByCriteria(PickupOrder pickupOrder)
			throws DAOException {
		return daoFactory.getPickupOrderDAO().findPickupOrderByCriteria(
				pickupOrder);
	}
}
