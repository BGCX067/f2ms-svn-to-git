package com.f2ms.service.common;

import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import com.f2ms.exception.DAOException;
import com.f2ms.exception.ServiceException;
import com.f2ms.model.Role;
import com.f2ms.model.Staff;
import com.f2ms.model.User;

public interface ICommonService {
    // User

    public User createUser(User user) throws DAOException, ServiceException;

    public User editUser(User user) throws DAOException;

    public List<User> findAllUser() throws DAOException;

    public User findUserById(long rowId) throws DAOException;
    
    public List<User> findUserByCriteria(User searchCriteria) throws DAOException;
    
    public User findUserByUserName(String userName) throws DAOException;
    
    public User editUserOnly(User user) throws DAOException;
    
    public void sendUserRegistrationEmail(User user) throws AddressException, MessagingException, DAOException;

    // Staff
    public Staff createStaff(Staff staff) throws DAOException;

    public Staff editStaff(Staff staff) throws DAOException;

    public List<Staff> findAllStaff() throws DAOException;

    public Staff findStaffById(long rowId) throws DAOException;
    
    public List<Staff> findStaffByCriteria(Staff staff) throws DAOException;
    
    // Role
    public Role createRole(Role role) throws DAOException;
    public Role editRole(Role role) throws DAOException;
    public List<Role> findRoleByCriteria(Role role) throws DAOException;
    public Role findRoleById(String roleCode) throws DAOException;
    public List<Role> findAllRoles() throws DAOException;
}
