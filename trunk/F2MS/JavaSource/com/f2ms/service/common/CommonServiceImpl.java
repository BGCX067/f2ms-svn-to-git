package com.f2ms.service.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.f2ms.dao.DAOFactory;
import com.f2ms.exception.DAOException;
import com.f2ms.exception.ServiceException;
import com.f2ms.model.Role;
import com.f2ms.model.Staff;
import com.f2ms.model.User;
import com.f2ms.model.UserRole;
import com.f2ms.util.EmailUtil;
import com.f2ms.util.F2MSUtil;
 
public class CommonServiceImpl implements ICommonService, UserDetailsService {

	private DAOFactory daoFactory;		

	public void setDaoFactory(DAOFactory daoFactory) {
		this.daoFactory = daoFactory;
	}

	public User createUser(User user) throws DAOException, ServiceException {		
        List<String> roles = user.getSelectedRoles();
        
        for (Iterator<String> iterator = roles.iterator(); iterator.hasNext();) {
			String role = (String) iterator.next();
			UserRole userRole = new UserRole();
			userRole.setRoleCode(role);
			userRole.setUsername(user.getUsername());
			
			daoFactory.getUserRoleDAO().create(userRole);
		}

        String defaultPassword = F2MSUtil.getRandomString(10);
        user.setPassword(defaultPassword);
        
        User created = daoFactory.getUserDAO().create(user);
                
        // Sending email
        if(created != null) {
        	try {
        		User found = daoFactory.getUserDAO().findUserById(user.getIdNo());
        		
				sendUserRegistrationEmail(found);
			} catch (AddressException e) {				
				e.printStackTrace();
				throw new ServiceException(e);
			} catch (MessagingException e) {
				e.printStackTrace();
				throw new ServiceException(e);
			}
        }
                        
        return created;
    }

	public void sendUserRegistrationEmail(User user) throws AddressException, MessagingException, DAOException {	 				
		if(user != null) {
			Staff staff = daoFactory.getStaffDAO().findStaffById(new Long(user.getStaffCode()));
			
			if(staff != null) {
				String subject = "F2MS Password";
		        StringBuffer message = new StringBuffer();
		        message.append("Dear ").append(staff.getName()).append("<br><br>"); 
		        message.append("We are pleased to inform your user name and password to access F2MS").append("<br>");
		        message.append("<ul>");
		        message.append("<li>User Name: <b>").append(user.getUsername()).append("</b>");
		        message.append("<li>Password: <b>").append(user.getPassword()).append("</b>");
		        message.append("</ul>").append("<br>"); 
		        message.append("Best Regards,").append("<br>");
		        message.append("F2MS Administrator");
		        
		        String[] recipients = new String[]{staff.getEmail()};        
		        
		        EmailUtil.sendEmail(recipients, subject, message.toString());
			}
						
		}				
	}

    public User editUser(User user) throws DAOException {
    	List<UserRole> roles = daoFactory.getUserRoleDAO().findRolesByUserName(user.getUsername());
    	
    	daoFactory.getUserRoleDAO().deleteAllRoles(roles);
    	
    	List<String> selectedRoles = user.getSelectedRoles();            	
    	
        for (Iterator<String> iterator = selectedRoles.iterator(); iterator.hasNext();) {
			String role = (String) iterator.next();
			UserRole userRole = new UserRole();
			userRole.setRoleCode(role);
			userRole.setUsername(user.getUsername());
			
			daoFactory.getUserRoleDAO().create(userRole);
		}
        
        User found = daoFactory.getUserDAO().findUserById(user.getIdNo());
    	
        return daoFactory.getUserDAO().edit(found);
    }

    @Override
    public List<User> findAllUser() throws DAOException {
        return daoFactory.getUserDAO().findAllUsers();
    }

    @Override
    public User findUserById(long rowId) throws DAOException {
    	User found = daoFactory.getUserDAO().findUserById(rowId);
    	
    	if(found != null) {
    		List<UserRole> roles = daoFactory.getUserRoleDAO().findRolesByUserName(found.getUsername());
    		List<String> selectedRoles = null;
    		
    		if(roles != null && !roles.isEmpty()) {
    			selectedRoles = new ArrayList<String>();
    			
    			for (Iterator<UserRole> iterator = roles.iterator(); iterator
						.hasNext();) {
					UserRole userRole = iterator.next();
					
					if(userRole != null) {
						selectedRoles.add(userRole.getRoleCode());
					}
				}
    			
    			found.setSelectedRoles(selectedRoles);
    		}
    	}
    	    	
        return found;
    }

    public Staff createStaff(Staff staff) throws DAOException {
        return daoFactory.getStaffDAO().create(staff);
    }

    public Staff editStaff(Staff staff) throws DAOException {
        return daoFactory.getStaffDAO().edit(staff);
    }

    @Override
    public List<Staff> findAllStaff() throws DAOException {
        return daoFactory.getStaffDAO().findAllStaff();
    }

    @Override
    public Staff findStaffById(long rowId) throws DAOException {
        return daoFactory.getStaffDAO().findStaffById(rowId);
    }

    @Override
    public UserDetails loadUserByUsername(String userName)
            throws UsernameNotFoundException, DataAccessException {

    	org.springframework.security.core.userdetails.User userSecurity = null;
        String username = null;
        String password = null;
        boolean enabled = false;
        boolean accountNonExpired = false;
        boolean credentialsNonExpired = false;
        boolean accountNonLocked = false;
        Collection<GrantedAuthority> authorities = null;
        
        try {
            User user = daoFactory.getUserDAO().findUserByName(userName);
            
            if(user != null) {
            	List<UserRole> roles = daoFactory.getUserRoleDAO().findRolesByUserName(user.getUsername());
            	
            	authorities = new ArrayList<GrantedAuthority>();            	
            	for (UserRole userRole : roles) {
					authorities.add(new GrantedAuthorityImpl(userRole.getRoleCode()));
				}
            	
            	username = user.getUsername();
            	password = user.getPassword();
            	enabled = user.isEnabled();
            	accountNonExpired = user.isAccountNonExpired();
            	credentialsNonExpired = user.isCredentialsNonExpired();
            	accountNonLocked = user.isAccountNonLocked();
            	
            	userSecurity = 
            		new org.springframework.security.core.userdetails.User(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);

            } else {
            	throw new UsernameNotFoundException("User or password do not match");
            }
            
            
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return userSecurity;
    }

	@Override
	public Role createRole(Role role) throws DAOException {
		return daoFactory.getRoleDAO().create(role); 
	}

	@Override
	public Role editRole(Role role) throws DAOException {
		return daoFactory.getRoleDAO().edit(role); 
	}

	@Override
	public List<Role> findRoleByCriteria(Role role) throws DAOException {
		return daoFactory.getRoleDAO().findByCriteria(role); 
	}

	@Override
	public Role findRoleById(String roleCode) throws DAOException {
		return daoFactory.getRoleDAO().findById(roleCode); 
	}

	@Override
	public List<Staff> findStaffByCriteria(Staff staff) throws DAOException {
		return daoFactory.getStaffDAO().findByCriteria(staff); 
	}

	@Override
	public List<User> findUserByCriteria(User searchCriteria)
			throws DAOException {
		return daoFactory.getUserDAO().findByCriteria(searchCriteria); 
	}

	@Override
	public List<Role> findAllRoles() throws DAOException {		
		return daoFactory.getRoleDAO().findAllRoles();
	}

	@Override
	public User findUserByUserName(String userName) throws DAOException {		
		return daoFactory.getUserDAO().findUserByName(userName); 
	}

	@Override
	public User editUserOnly(User user) throws DAOException {		
		return daoFactory.getUserDAO().edit(user); 
	}		   			
}
