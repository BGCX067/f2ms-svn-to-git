package com.f2ms.service.location;

import java.util.List;

import com.f2ms.exception.DAOException;
import com.f2ms.model.Location;

public interface ILocationService {

    public List<Location> findAllLocations() throws DAOException;

    public Location create(Location location) throws DAOException;

    public Location edit(Location location) throws DAOException;

    public Location findLocationById(long locationId) throws DAOException;

    public List<Location> findLocationByCriteria(Location location)
            throws DAOException;
}
