package com.f2ms.service.location;

import java.util.List;

import com.f2ms.dao.DAOFactory;
import com.f2ms.exception.DAOException;
import com.f2ms.model.Location;

public class LocationServiceImpl implements ILocationService {

	private DAOFactory daoFactory;		

	public void setDaoFactory(DAOFactory daoFactory) {
		this.daoFactory = daoFactory;
	}

    @Override
    public List<Location> findAllLocations() throws DAOException {
        return daoFactory.getLocationDAO().findAllLocations();
    }

    @Override
    public Location create(Location location) throws DAOException {
        return daoFactory.getLocationDAO().create(location);
    }

    @Override
    public Location edit(Location location) throws DAOException {
        return daoFactory.getLocationDAO().edit(location);
    }

    @Override
    public Location findLocationById(long locationId) throws DAOException {
        return daoFactory.getLocationDAO().findLocationById(locationId);
    }

    @Override
    public List<Location> findLocationByCriteria(Location location)
            throws DAOException {
        return daoFactory.getLocationDAO().findLocationByCriteria(location);
    }
}
