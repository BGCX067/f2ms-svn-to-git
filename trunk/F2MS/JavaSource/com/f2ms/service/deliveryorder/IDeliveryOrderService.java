package com.f2ms.service.deliveryorder;

import java.util.List;

import com.f2ms.exception.DAOException;
import com.f2ms.model.DeliveryOrder;

public interface IDeliveryOrderService {
	public DeliveryOrder createDeliveryOrder(DeliveryOrder deliveryOrder) throws DAOException;
	public DeliveryOrder editDeliveryOrder(DeliveryOrder deliveryOrder) throws DAOException;
	public DeliveryOrder findDeliveryOrderById(Long id) throws DAOException;
	public List<DeliveryOrder> findDeliveryOrderByCriteria(DeliveryOrder deliveryOrder) throws DAOException;
}
