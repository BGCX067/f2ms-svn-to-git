package com.f2ms.service.deliveryorder;

import java.util.Date;
import java.util.List;

import com.f2ms.dao.DAOFactory;
import com.f2ms.enumeration.BookingStatus;
import com.f2ms.enumeration.DOStatus;
import com.f2ms.exception.DAOException;
import com.f2ms.model.Booking;
import com.f2ms.model.DeliveryOrder;

public class DeliveryOrderServiceImpl implements IDeliveryOrderService {
	private DAOFactory daoFactory;		

	public void setDaoFactory(DAOFactory daoFactory) {
		this.daoFactory = daoFactory;
	}

	@Override
	public DeliveryOrder createDeliveryOrder(DeliveryOrder deliveryOrder)
			throws DAOException {
		String bookingNo = deliveryOrder.getBookingNo();
		
		int seqNo = daoFactory.getDeliveryOrderDAO().getMaxSeqNo(bookingNo);		
		String doNo = bookingNo + "-" + seqNo;
		
		deliveryOrder.setDocNo(doNo);
		deliveryOrder.setStatus(DOStatus.NEW.getValue());
		deliveryOrder.setDocDate(new Date());
		
		Booking found = daoFactory.getBookingDAO().findBookingById(bookingNo);
		if(found != null) {
			found.setStatus(String.valueOf(BookingStatus.DELIVERED.getValue()));
		}
		
		return daoFactory.getDeliveryOrderDAO().create(deliveryOrder); 
	}

	@Override
	public DeliveryOrder editDeliveryOrder(DeliveryOrder deliveryOrder)
			throws DAOException {
		return daoFactory.getDeliveryOrderDAO().edit(deliveryOrder);
	}

	@Override
	public DeliveryOrder findDeliveryOrderById(Long id) throws DAOException {
		DeliveryOrder found = daoFactory.getDeliveryOrderDAO().findById(id);
		found.setTruckerName(found.getTruckerObj().getName());
		found.setDeliveryToName(found.getDeliveryToObj().getName());
		
		return found; 
	}

	@Override
	public List<DeliveryOrder> findDeliveryOrderByCriteria(
			DeliveryOrder deliveryOrder) throws DAOException {
		return daoFactory.getDeliveryOrderDAO().findByCriteria(deliveryOrder); 
	}

}
