package com.f2ms.service.shipment;

import java.util.List;

import com.f2ms.exception.DAOException;
import com.f2ms.model.Shipment;

public interface IShipmentService {

	public List<Shipment> findAllShipments() throws DAOException;

	public Shipment create(Shipment shipment) throws DAOException;

	public Shipment edit(Shipment shipment) throws DAOException;

	public Shipment findShipmentById(String shipmentNo) throws DAOException;

	public List<Shipment> findShipmentByCriteria(Shipment shipment)
			throws DAOException;

}
