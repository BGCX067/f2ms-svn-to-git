package com.f2ms.service.shipment;

import java.util.Iterator;
import java.util.List;

import com.f2ms.dao.DAOFactory;
import com.f2ms.dao.booking.IBookingDAO;
import com.f2ms.dao.shipment.IShipmentDAO;
import com.f2ms.enumeration.BookingStatus;
import com.f2ms.exception.DAOException;
import com.f2ms.model.Booking;
import com.f2ms.model.Shipment;
import com.f2ms.model.ShipmentItem;

public class ShipmentServiceImpl implements IShipmentService {

	private DAOFactory daoFactory;

	public void setDaoFactory(DAOFactory daoFactory) {
		this.daoFactory = daoFactory;
	}

	@Override
	public List<Shipment> findAllShipments() throws DAOException {
		return daoFactory.getShipmentDAO().findAllPackagings();
	}

	@Override
	public Shipment create(Shipment shipment) throws DAOException {

		IShipmentDAO shipmentDAO = daoFactory.getShipmentDAO();

		Shipment newlyShipment = shipmentDAO.create(shipment);
		for (Iterator<ShipmentItem> it = shipment.getShipmentItems().iterator(); it
				.hasNext();) {
			ShipmentItem element = it.next();

			IBookingDAO bookingDAO = daoFactory.getBookingDAO();
			Booking booking = bookingDAO.findBookingById(element.getBooking()
					.getDocNo());
			booking.setStatus(String.valueOf(BookingStatus.PACKED.getValue()));
			bookingDAO.edit(booking);
		}

		return newlyShipment;
	}

	@Override
	public Shipment edit(Shipment shipment) throws DAOException {
		IShipmentDAO shipmentDAO = daoFactory.getShipmentDAO();
		shipment = shipmentDAO.edit(shipment);
		return shipment;
	}

	@Override
	public Shipment findShipmentById(String shipmentNo) throws DAOException {
		return daoFactory.getShipmentDAO().findPackagingById(shipmentNo);
	}

	@Override
	public List<Shipment> findShipmentByCriteria(Shipment shipment)
			throws DAOException {
		return daoFactory.getShipmentDAO().findPackagingByCriteria(shipment);
	}

}
