package com.f2ms.bean;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import net.sf.jasperreports.engine.JRException;

import org.apache.log4j.Logger;
import org.springframework.orm.hibernate3.HibernateOptimisticLockingFailureException;

import com.f2ms.exception.DAOException;
import com.f2ms.model.Booking;
import com.f2ms.model.ReceiveOrder;
import com.f2ms.service.receiveorder.IReceiveOrderService;
import com.f2ms.util.F2MSUtil;
import com.f2ms.util.IF2MSConstants;
import com.f2ms.util.JasperReportUtil;

public class ReceiveOrderBean extends BaseBean {
	private static final String EDIT_RECEIVE_ORDER_HEADER = "editReceiveOrderHeader";

	private static final String NEW_RECEIVE_ORDER_HEADER = "newReceiveOrderHeader";

	private static final String FWD_SEARCH_RECEIVE_ORDER = "searchReceiveOrder";

	private static final String FWD_CREATE_RECEIVE_ORDER = "createReceiveOrder";

	private static Logger logger = Logger.getLogger(ReceiveOrderBean.class);

	private ReceiveOrder receiveOrder;
	private IReceiveOrderService receiveOrderService;

	private Boolean editMode;

	// id to edit ReceiveOrder
	private Long id;

	// search criteria
	private String docNo;
	private Date docDate;
	private String bookingNo;
	private Integer status;

	public ReceiveOrder getReceiveOrder() {
		return receiveOrder;
	}

	public void setReceiveOrder(ReceiveOrder receiveOrder) {
		this.receiveOrder = receiveOrder;
	}

	public IReceiveOrderService getReceiveOrderService() {
		return receiveOrderService;
	}

	public void setReceiveOrderService(IReceiveOrderService receiveOrderService) {
		this.receiveOrderService = receiveOrderService;
	}

	public Boolean getEditMode() {
		return editMode;
	}

	public void setEditMode(Boolean editMode) {
		this.editMode = editMode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDocNo() {
		return docNo;
	}

	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}

	public Date getDocDate() {
		return docDate;
	}

	public void setDocDate(Date docDate) {
		this.docDate = docDate;
	}

	public String getBookingNo() {
		return bookingNo;
	}

	public void setBookingNo(String bookingNo) {
		this.bookingNo = bookingNo;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@PostConstruct
	public void init() {
		receiveOrder = new ReceiveOrder();
	}

	public String create() {
		editMode = false;
		return FWD_CREATE_RECEIVE_ORDER;
	}

	public String save() {

		String returnPage = FWD_SEARCH_RECEIVE_ORDER;
		String userName = (String) F2MSUtil.getSession().getAttribute(
				IF2MSConstants.USER_NAME);

		try {
			receiveOrder.setChangedBy(userName);

			if (editMode) {
				receiveOrderService.edit(receiveOrder);
			} else {
				receiveOrder.setCreatedBy(userName);
				receiveOrderService.create(receiveOrder);
			}
		} catch (HibernateOptimisticLockingFailureException fe) {
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage errMsg = new FacesMessage(getMessage("errOptLock",
					null));
			context.addMessage("ReceiveOrder Exception", errMsg);
			returnPage = FWD_CREATE_RECEIVE_ORDER; 
		} catch (DAOException daoEx) {
			daoEx.printStackTrace();
			logger.error(daoEx.getMessage());
			returnPage = FWD_GLOBAL_ERROR;
		}

		return returnPage;
	}

	public List<ReceiveOrder> getAllReceiveOrders() {
		try {
			return receiveOrderService.findAllReceiveOrders();
		} catch (DAOException daoEx) {
			daoEx.printStackTrace();
			logger.error(daoEx.getMessage());
		}

		return null;
	}

	public String search() {
		return FWD_SEARCH_RECEIVE_ORDER;
	}

	public List<ReceiveOrder> getListResult() {
		try {

			Booking booking = new Booking();
			booking.setDocNo(bookingNo);

			ReceiveOrder searchCriteria = new ReceiveOrder();
			searchCriteria.setDocNo(docNo);
			searchCriteria.setDocDate(docDate);
			searchCriteria.setBooking(booking);
			searchCriteria.setStatus(status);

			return receiveOrderService
					.findReceiveOrderByCriteria(searchCriteria);
		} catch (DAOException daoEx) {
			daoEx.printStackTrace();
			logger.error(daoEx.getMessage());
		}

		return null;
	}

	@SuppressWarnings("rawtypes")
	public String edit() {
		editMode = true;

		FacesContext context = FacesContext.getCurrentInstance();
		Map requestMap = context.getExternalContext().getRequestParameterMap();
		String prmIdNo = (String) requestMap.get("id");

		try {
			receiveOrder = receiveOrderService.findReceiveOrderById(Long
					.parseLong(prmIdNo));
		} catch (NumberFormatException nfx) {
			nfx.printStackTrace();
			logger.error(nfx.getMessage());
			return FWD_GLOBAL_ERROR;
		} catch (DAOException daoEx) {
			daoEx.printStackTrace();
			logger.error(daoEx.getMessage());
			return FWD_GLOBAL_ERROR;
		}

		return FWD_CREATE_RECEIVE_ORDER;
	}

	public String getReceiveOrderHeader() {
		String header = getMessage(NEW_RECEIVE_ORDER_HEADER, null);

		if (editMode) {
			header = getMessage(EDIT_RECEIVE_ORDER_HEADER, null);
		}

		return header;
	}

	@SuppressWarnings("rawtypes")
	public void print() {
		FacesContext context = FacesContext.getCurrentInstance();
		Map requestMap = context.getExternalContext().getRequestParameterMap();
		String prmIdNo = (String) requestMap.get("id");

		Long receiveOrderid = Long.parseLong(prmIdNo);
		try {
			JasperReportUtil.printPickupOrder(receiveOrderid);
		} catch (JRException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (SQLException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}
}
