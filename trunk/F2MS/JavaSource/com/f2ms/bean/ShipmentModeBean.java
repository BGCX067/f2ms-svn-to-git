package com.f2ms.bean;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;
import org.springframework.orm.hibernate3.HibernateOptimisticLockingFailureException;

import com.f2ms.exception.DAOException;
import com.f2ms.model.ShipmentMode;
import com.f2ms.service.shipmentmode.IShipmentModeService;
import com.f2ms.util.F2MSUtil;
import com.f2ms.util.IF2MSConstants;

public class ShipmentModeBean extends BaseBean {
	private static final String FWD_SEARCH_SHIPMENT_MODE = "searchShipmentMode";

	private static final String FWD_CREATE_SHIPMENT_MODE = "createShipmentMode";

	private static Logger logger = Logger.getLogger(ShipmentModeBean.class);

	private ShipmentMode shipmentMode;
	private IShipmentModeService shipmentModeService;
	private Boolean editMode;
	private Long id;
	private String code;
	private String description;
	private Integer status;
	private Integer mode;

	public ShipmentMode getShipmentMode() {
		return shipmentMode;
	}

	public void setShipmentMode(ShipmentMode shipmentMode) {
		this.shipmentMode = shipmentMode;
	}

	public IShipmentModeService getShipmentModeService() {
		return shipmentModeService;
	}

	public void setShipmentModeService(IShipmentModeService shipmentModeService) {
		this.shipmentModeService = shipmentModeService;
	}

	public Boolean getEditMode() {
		return editMode;
	}

	public Long getId() {
		return id;
	}

	public void setEditMode(Boolean editMode) {
		this.editMode = editMode;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getStatus() {
		return status;
	}

	public Integer getMode() {
		return mode;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public void setMode(Integer mode) {
		this.mode = mode;
	}

	@PostConstruct
	public void init() {
		shipmentMode = new ShipmentMode();
	}

	public String create() {
		editMode = false;
		return FWD_CREATE_SHIPMENT_MODE;
	}

	public String save() {

		String returnPage = FWD_SEARCH_SHIPMENT_MODE;
		String userName = (String) F2MSUtil.getSession().getAttribute(
				IF2MSConstants.USER_NAME);

		try {
			shipmentMode.setChangedBy(userName);

			if (editMode) {
				shipmentModeService.edit(shipmentMode);
			} else {
				shipmentMode.setCreatedBy(userName);
				shipmentModeService.create(shipmentMode);
			}
		} catch (HibernateOptimisticLockingFailureException fe) {
			fe.printStackTrace();

			FacesContext context = FacesContext.getCurrentInstance();

			FacesMessage errMsg = new FacesMessage(getMessage("errOptLock",
					null));
			context.addMessage("ShipmentMode Exception", errMsg);

			returnPage = FWD_CREATE_SHIPMENT_MODE;
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			returnPage = FWD_GLOBAL_ERROR;
		}

		return returnPage;
	}

	public List<ShipmentMode> getAllShipmentModes() {
		try {
			return shipmentModeService.findAllShipmentModes();
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		return null;
	}

	public String search() {
		return FWD_SEARCH_SHIPMENT_MODE;
	}

	public List<ShipmentMode> getListResult() {
		try {
			ShipmentMode searchCriteria = new ShipmentMode();
			searchCriteria.setCode(code);
			searchCriteria.setDescription(description);
			searchCriteria.setStatus(status);

			return shipmentModeService
					.findShipmentModeByCriteria(searchCriteria);
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		return null;
	}

	@SuppressWarnings("rawtypes")
	public String edit() {
		editMode = true;

		FacesContext context = FacesContext.getCurrentInstance();
		Map requestMap = context.getExternalContext().getRequestParameterMap();
		String prmIdNo = (String) requestMap.get("id");
		try {
			shipmentMode = shipmentModeService.findShipmentModeById(Long
					.parseLong(prmIdNo));
		} catch (NumberFormatException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			return FWD_GLOBAL_ERROR;
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			return FWD_GLOBAL_ERROR;
		}

		return FWD_CREATE_SHIPMENT_MODE;
	}

	public String getShipmentModeHeader() {
		String header = getMessage("newShipmentModeHeader", null);

		if (editMode) {
			header = getMessage("editShipmentModeHeader", null);
		}

		return header;
	}
}
