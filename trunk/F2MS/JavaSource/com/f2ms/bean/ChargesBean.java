package com.f2ms.bean;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;
import org.springframework.orm.hibernate3.HibernateOptimisticLockingFailureException;

import com.f2ms.enumeration.ChargeStatus;
import com.f2ms.exception.DAOException;
import com.f2ms.model.Charges;
import com.f2ms.service.charges.IChargesService;
import com.f2ms.util.F2MSUtil;
import com.f2ms.util.IF2MSConstants;

public class ChargesBean extends BaseBean {
	private static final String FWD_SEARCH_CHARGES = "searchCharges";

	private static final String FWD_CREATE_CHARGES = "createCharges";

	private static Logger logger = Logger.getLogger(ChargesBean.class);
	
	private Charges charges;
	private IChargesService chargesService;
	
	private String id;
	private boolean editMode;
	
	// Search criteria
	private String chargesDesc;
	private String chargesType;
	private String chargesStatus;
	public Charges getCharges() {
		return charges;
	}
	public void setCharges(Charges charges) {
		this.charges = charges;
	}
	public IChargesService getChargesService() {
		return chargesService;
	}
	public void setChargesService(IChargesService chargesService) {
		this.chargesService = chargesService;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public boolean isEditMode() {
		return editMode;
	}
	public void setEditMode(boolean editMode) {
		this.editMode = editMode;
	}
	public String getChargesDesc() {
		return chargesDesc;
	}
	public void setChargesDesc(String chargesDesc) {
		this.chargesDesc = chargesDesc;
	}
	public String getChargesType() {
		return chargesType;
	}
	public void setChargesType(String chargesType) {
		this.chargesType = chargesType;
	}
	public String getChargesStatus() {
		return chargesStatus;
	}
	public void setChargesStatus(String chargesStatus) {
		this.chargesStatus = chargesStatus;
	}
	
	@PostConstruct
	public void init() {
		charges = new Charges();
	}
	
	public String create() {
		editMode = false;
		
		charges.setStatus(String.valueOf(ChargeStatus.ACTIVE.getValue()));
		
		return FWD_CREATE_CHARGES;
	}
	
	public String save() {
		String returnPage = FWD_SEARCH_CHARGES;
		String userName = (String) F2MSUtil.getSession().getAttribute(IF2MSConstants.USER_NAME);
		
		try {
			charges.setChangedBy(userName);
		
			if(editMode) {
				chargesService.editCharge(charges);
			} else {
				charges.setCreatedBy(userName);
				chargesService.createCharge(charges);
			}
		} catch (HibernateOptimisticLockingFailureException fe){			
			fe.printStackTrace();
			
			FacesContext context = FacesContext.getCurrentInstance();
			
			FacesMessage errMsg = new FacesMessage(getMessage("errOptLock", null));								
			context.addMessage("Charges Exception", errMsg);						
			
			returnPage = FWD_CREATE_CHARGES;
		} catch (DAOException e) {		
			e.printStackTrace();
			logger.error(e.getMessage());
			returnPage = FWD_GLOBAL_ERROR;
		}
		
		return returnPage;
	}
	
	public String search() {		
		return FWD_SEARCH_CHARGES;
	}
	
	public List<Charges> getListResult() {
		try {
			Charges searchCriteria = new Charges();
			searchCriteria.setChargeType(chargesType);
			searchCriteria.setDescription(chargesDesc);
			searchCriteria.setStatus(chargesStatus);
			
			return chargesService.findChargesByCriteria(searchCriteria);
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		
		return null;
	}
	
	public String edit() {
		editMode = true;		
		
		FacesContext context = FacesContext.getCurrentInstance();  
		@SuppressWarnings("rawtypes")
		Map requestMap = context.getExternalContext().getRequestParameterMap();  
		String prmId = (String) requestMap.get("id");
		
		try {
			charges = chargesService.findChargeById(Long.parseLong(prmId));
		} catch (NumberFormatException e) {			
			e.printStackTrace();
			logger.error(e.getMessage());
			return FWD_GLOBAL_ERROR;
		} catch (DAOException e) {			
			e.printStackTrace();
			logger.error(e.getMessage());
			return FWD_GLOBAL_ERROR;
		}
		
		return FWD_CREATE_CHARGES;
	}
	
	public String getChargesHeader() {
		String header = getMessage("newChargesHeader", null);
		
		if(editMode) {
			header = getMessage("editChargesHeader", null);
		}
		
		return header;
	}
}
