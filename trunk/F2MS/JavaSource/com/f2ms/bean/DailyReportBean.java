package com.f2ms.bean;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import net.sf.jasperreports.engine.JRException;

import com.f2ms.enumeration.Status;
import com.f2ms.exception.DAOException;
import com.f2ms.model.ShipmentMode;
import com.f2ms.service.shipmentmode.IShipmentModeService;
import com.f2ms.util.JasperReportUtil;

public class DailyReportBean extends BaseBean {
	private static Logger logger = Logger.getLogger(DailyReportBean.class);
	private static final String FWD_DAILY_SHIPMENT = "dailyShipment";
	private Long polId;
	private Long podId;
	private String shipmentType;
	private String polDesc;
	private String podDesc;	
	
	private IShipmentModeService shipmentModeService;		
	
	public IShipmentModeService getShipmentModeService() {
		return shipmentModeService;
	}
	public void setShipmentModeService(IShipmentModeService shipmentModeService) {
		this.shipmentModeService = shipmentModeService;
	}
	public String getPolDesc() {
		return polDesc;
	}
	public void setPolDesc(String polDesc) {
		this.polDesc = polDesc;
	}
	public String getPodDesc() {
		return podDesc;
	}
	public void setPodDesc(String podDesc) {
		this.podDesc = podDesc;
	}
	public Long getPolId() {
		return polId;
	}
	public void setPolId(Long polId) {
		this.polId = polId;
	}
	public Long getPodId() {
		return podId;
	}
	public void setPodId(Long podId) {
		this.podId = podId;
	}
	public String getShipmentType() {
		return shipmentType;
	}
	public void setShipmentType(String shipmentType) {
		this.shipmentType = shipmentType;
	}
	
	public String init() {
		return FWD_DAILY_SHIPMENT;
	}
	
	public String generateReport() {
		try {						
			JasperReportUtil.printDailyShipment(new Integer((int) polId.longValue()), 
												new Integer((int)podId.longValue()), 
												shipmentType);
		} catch (JRException e) { 			
			e.printStackTrace();
			logger.error(e.getMessage());
			return FWD_GLOBAL_ERROR;
		} catch (IOException e) {			
			e.printStackTrace();
			logger.error(e.getMessage());
			return FWD_GLOBAL_ERROR;
		} catch (SQLException e) {			
			e.printStackTrace();
			logger.error(e.getMessage());
			return FWD_GLOBAL_ERROR;
		}
		
		return FWD_DAILY_SHIPMENT;
	}
	
	public List<SelectItem> getShipmentModeList() {
		List<SelectItem> items = new ArrayList<SelectItem>();

	    ShipmentMode shipmentMode = new ShipmentMode();
	    shipmentMode.setStatus(Status.ACTIVE.getValue());
	
	    try {
	         List<ShipmentMode> listShipmentModes = shipmentModeService.findShipmentModeByCriteria(shipmentMode);
	
	         for (ShipmentMode temp : listShipmentModes) {
	            items.add(new SelectItem(temp.getId(), temp.getDescription()));
	         }	
	    } catch (DAOException e) {    	  
	         e.printStackTrace();
	         logger.error(e.getMessage());
	    }
	
	    return items;
	}
}
