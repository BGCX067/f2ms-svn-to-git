package com.f2ms.bean;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import net.sf.jasperreports.engine.JRException;

import org.apache.log4j.Logger;
import org.springframework.orm.hibernate3.HibernateOptimisticLockingFailureException;

import com.f2ms.enumeration.BookingStatus;
import com.f2ms.exception.DAOException;
import com.f2ms.model.Booking;
import com.f2ms.model.PickupOrder;
import com.f2ms.service.booking.IBookingService;
import com.f2ms.service.pickuporder.IPickupOrderService;
import com.f2ms.util.F2MSUtil;
import com.f2ms.util.IF2MSConstants;
import com.f2ms.util.JasperReportUtil;

/**
 * PickupOrderBean.java
 * 
 * @author Isak Rabin
 * 
 */
public class PickupOrderBean extends BaseBean {
	private static Logger logger = Logger.getLogger(PickupOrderBean.class);

	private static final String CREATE_PICKUP_ORDER_HEADER = "newPickupOrderHeader";
	private static final String EDIT_PICKUP_ORDER_HEADER = "editPickupOrderHeader";

	private PickupOrder pickupOrder;
	private IPickupOrderService pickupOrderService;
	private IBookingService bookingService;

	private boolean editMode;

	// Search Criteria
	private Long id;
	private String docNo;
	private Date pickupDate;
	private String bookingNo;
	private Long truckerId;
	private Integer mode;
	private Integer status;
	private String statusReason;
	private Long warehouseId;
	private String remarks;

	public PickupOrder getPickupOrder() {
		return pickupOrder;
	}

	public void setPickupOrder(PickupOrder pickupOrder) {
		this.pickupOrder = pickupOrder;
	}

	public IPickupOrderService getPickupOrderService() {
		return pickupOrderService;
	}

	public void setPickupOrderService(IPickupOrderService pickupOrderService) {
		this.pickupOrderService = pickupOrderService;
	}

	public IBookingService getBookingService() {
		return bookingService;
	}

	public void setBookingService(IBookingService bookingService) {
		this.bookingService = bookingService;
	}

	public boolean isEditMode() {
		return editMode;
	}

	public void setEditMode(boolean editMode) {
		this.editMode = editMode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDocNo() {
		return docNo;
	}

	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}

	public Date getPickupDate() {
		return pickupDate;
	}

	public void setPickupDate(Date pickupDate) {
		this.pickupDate = pickupDate;
	}

	public String getBookingNo() {
		return bookingNo;
	}

	public void setBookingNo(String bookingNo) {
		this.bookingNo = bookingNo;
	}

	public Long getTruckerId() {
		return truckerId;
	}

	public void setTruckerId(Long truckerId) {
		this.truckerId = truckerId;
	}

	public Integer getMode() {
		return mode;
	}

	public void setMode(Integer mode) {
		this.mode = mode;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getStatusReason() {
		return statusReason;
	}

	public void setStatusReason(String statusReason) {
		this.statusReason = statusReason;
	}

	public Long getWarehouseId() {
		return warehouseId;
	}

	public void setWarehouseId(Long warehouseId) {
		this.warehouseId = warehouseId;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@PostConstruct
	public void init() {
		pickupOrder = new PickupOrder();
	}

	public String create() {
		editMode = false;
		return "createPickupOrder";
	}

	public String save() {

		String returnPage = "searchPickupOrder";
		String userName = (String) F2MSUtil.getSession().getAttribute(
				IF2MSConstants.USER_NAME);

		try {
			pickupOrder.setChangedBy(userName);

			if (editMode) {
				pickupOrderService.edit(pickupOrder);
			} else {
				pickupOrder.setCreatedBy(userName);
				pickupOrderService.create(pickupOrder);
			}
		} catch (HibernateOptimisticLockingFailureException fe) {
			fe.printStackTrace();

			FacesContext context = FacesContext.getCurrentInstance();

			FacesMessage errMsg = new FacesMessage(getMessage("errOptLock",
					null));
			context.addMessage("PickupOrder Exception", errMsg);

			returnPage = CREATE_PICKUP_ORDER_HEADER;
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			
			returnPage = FWD_GLOBAL_ERROR;
		}

		return returnPage;
	}

	public List<PickupOrder> getAllPickupOrders() {
		try {
			return pickupOrderService.findAllPickupOrders();
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		return null;
	}

	public String search() {
		return "searchPickupOrder";
	}

	public List<PickupOrder> getListResult() {
		try {
			PickupOrder searchCriteria = new PickupOrder();
			searchCriteria.setDocNo(docNo);
			searchCriteria.setPickupDate(pickupDate);
			searchCriteria.setBookingNo(bookingNo);
			searchCriteria.setStatus(status);

			return pickupOrderService.findPickupOrderByCriteria(searchCriteria);
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		return null;
	}

	@SuppressWarnings("rawtypes")
	public String edit() {
		editMode = true;

		FacesContext context = FacesContext.getCurrentInstance();
		Map requestMap = context.getExternalContext().getRequestParameterMap();
		String prmIdNo = (String) requestMap.get("id");

		try {
			pickupOrder = pickupOrderService.findPickupOrderById(Long
					.parseLong(prmIdNo));
		} catch (NumberFormatException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			return FWD_GLOBAL_ERROR;
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			return FWD_GLOBAL_ERROR;
		}

		return "createPickupOrder";
	}

	public String getPickupOrderHeader() {
		String header = getMessage(CREATE_PICKUP_ORDER_HEADER, null);

		if (editMode) {
			header = getMessage(EDIT_PICKUP_ORDER_HEADER, null);
		}

		return header;
	}

	public List<Booking> suggestBooking(Object value) {
		String input = (String) value;

		Booking search = new Booking();
		search.setDocNo(input);
		search.setNeedPickup(1);
		search.setStatus(String.valueOf(BookingStatus.NEW.getValue()));

		List<Booking> listResult = null;
		try {
			listResult = bookingService.findBookingByCriteria(search);
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		return listResult;
	}

	@SuppressWarnings("rawtypes")
	public void print() {
		FacesContext context = FacesContext.getCurrentInstance();
		Map requestMap = context.getExternalContext().getRequestParameterMap();
		String prmIdNo = (String) requestMap.get("id");

		Long receiveOrderid = Long.parseLong(prmIdNo);
		try {
			JasperReportUtil.printPickupOrder(receiveOrderid);
		} catch (JRException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (SQLException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}
}
