package com.f2ms.bean;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;
import org.springframework.orm.hibernate3.HibernateOptimisticLockingFailureException;

import com.f2ms.exception.DAOException;
import com.f2ms.model.Customer;
import com.f2ms.service.customer.ICustomerService;
import com.f2ms.util.F2MSUtil;
import com.f2ms.util.IF2MSConstants;

public class CustomerBean extends BaseBean {
	private static final String FWD_SEARCH_CUSTOMER = "searchCustomer";

	private static final String FWD_CREATE_CUSTOMER = "createCustomer";

	private static Logger logger = Logger.getLogger(CustomerBean.class);

	private Customer customer;
	private ICustomerService customerService;
	private String custCode;
	private boolean editMode;
	// Search criteria
	private String idType;
	private String idNo;
	private String name;
	private String address;

	public ICustomerService getCustomerService() {
		return customerService;
	}

	public void setCustomerService(ICustomerService customerService) {
		this.customerService = customerService;
	}

	public Customer getCustomer() {
		return customer;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public boolean isEditMode() {
		return editMode;
	}

	public void setEditMode(boolean editMode) {
		this.editMode = editMode;
	}

	public String getCustCode() {
		return custCode;
	}

	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getIdNo() {
		return idNo;
	}

	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}

	@PostConstruct
	public void init() {
		customer = new Customer();
	}

	public String create() {
		editMode = false;

		return FWD_CREATE_CUSTOMER;
	}

	public String save() {

		String returnPage = FWD_SEARCH_CUSTOMER;
		String userName = (String) F2MSUtil.getSession().getAttribute(
				IF2MSConstants.USER_NAME);

		try {
			customer.setChangedBy(userName);

			if (editMode) {
				customerService.edit(customer);
			} else {
				customer.setCreatedBy(userName);
				customerService.create(customer);
			}
		} catch (HibernateOptimisticLockingFailureException fe) {
			fe.printStackTrace();

			FacesContext context = FacesContext.getCurrentInstance();

			FacesMessage errMsg = new FacesMessage(getMessage("errOptLock",
					null));
			context.addMessage("Customer Exception", errMsg);

			returnPage = FWD_CREATE_CUSTOMER;
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			returnPage = FWD_GLOBAL_ERROR;
		}

		return returnPage;
	}

	public List<Customer> getAllCustomers() {
		try {
			return customerService.findAllCustomers();
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		return null;
	}

	public String search() {
		return FWD_SEARCH_CUSTOMER;
	}

	public List<Customer> getListResult() {
		try {
			Customer searchCriteria = new Customer();
			searchCriteria.setIdType(idType);
			searchCriteria.setIdNo(idNo);
			searchCriteria.setName(name);
			searchCriteria.setAddressLine1(address);

			return customerService.findCustomerByCriteria(searchCriteria);

		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		return null;
	}

	public String edit() {
		editMode = true;

		FacesContext context = FacesContext.getCurrentInstance();  
		@SuppressWarnings("rawtypes")
		Map requestMap = context.getExternalContext().getRequestParameterMap();  
		String prmCustCode = (String) requestMap.get("custCode");
		
		try {
			customer = customerService.findCustomerById(Long
					.parseLong(prmCustCode));
		} catch (NumberFormatException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			return FWD_GLOBAL_ERROR;
		} catch (DAOException e) {
			e.printStackTrace();
			return FWD_GLOBAL_ERROR;
		}

		return FWD_CREATE_CUSTOMER;
	}

	public String getCustomerHeader() {
		String header = getMessage("newCustomerHeader", null);

		if (editMode) {
			header = getMessage("editCustomerHeader", null);
		}

		return header;
	}

	public List<Customer> suggest(Object value) {
		String input = (String) value;

		Customer search = new Customer();
		search.setName(input);

		List<Customer> listResult = null;
		try {
			listResult = customerService.findCustomerByCriteria(search);
		} catch (DAOException e) {
			e.printStackTrace();
		}

		return listResult;
	}
}
