package com.f2ms.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;
import org.springframework.orm.hibernate3.HibernateOptimisticLockingFailureException;

import com.f2ms.exception.DAOException;
import com.f2ms.model.Staff;
import com.f2ms.service.common.ICommonService;
import com.f2ms.util.F2MSUtil;
import com.f2ms.util.IF2MSConstants;

public class StaffBean extends BaseBean {
	
	private static Logger logger = Logger.getLogger(StaffBean.class);
	
	private static final String FWD_SEARCH_STAFF = "searchStaff";
	private static final String FWD_CREATE_STAFF = "createStaff";
	private Staff staff;
	private ICommonService commonService;
	
	private String staffCode;
	private boolean editMode;
	
	// search criteria
	private String name;
	private String designation;
	public Staff getStaff() {
		return staff;
	}
	public void setStaff(Staff staff) {
		this.staff = staff;
	}
	public ICommonService getCommonService() {
		return commonService;
	}
	public void setCommonService(ICommonService commonService) {
		this.commonService = commonService;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public boolean isEditMode() {
		return editMode;
	}
	public void setEditMode(boolean editMode) {
		this.editMode = editMode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	
	@PostConstruct
	public void init() {
		staff = new Staff();
	}
	
	public String create() {
		editMode = false;		
		
		return FWD_CREATE_STAFF;
	}
	
	public String save() {
		String returnPage = FWD_SEARCH_STAFF;
		String userName = (String) F2MSUtil.getSession().getAttribute(IF2MSConstants.USER_NAME);
		
		staff.setChangedBy(userName);
		
		try {
			if(editMode) {
				commonService.editStaff(staff);
			} else {
				staff.setCreatedBy(userName);
				commonService.createStaff(staff);
			}
		} catch (HibernateOptimisticLockingFailureException fe){			
			fe.printStackTrace();
			
			FacesContext context = FacesContext.getCurrentInstance();
			
			FacesMessage errMsg = new FacesMessage(getMessage("errOptLock", null));								
			context.addMessage("Role Exception", errMsg);						
			
			returnPage = "failureSave";
		} catch (DAOException e) {			
			e.printStackTrace();
			logger.error(e.getMessage());
			returnPage = FWD_GLOBAL_ERROR;
		}
						
		return returnPage;
	}
	
	public String search() {
		return FWD_SEARCH_STAFF;
	}
	
	public List<Staff> getListResult() {
		Staff searchCriteria = new Staff();
		searchCriteria.setName(name);
		searchCriteria.setDesignation(designation);
		
		try {
			return commonService.findStaffByCriteria(searchCriteria);
		} catch (DAOException e) {			
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		
		return null;
	}
	
	public String edit() {
		editMode = true;
		
		FacesContext context = FacesContext.getCurrentInstance();  
		@SuppressWarnings("rawtypes")
		Map requestMap = context.getExternalContext().getRequestParameterMap();  
		String prmStaffCode = (String) requestMap.get("staffCode");
		
		try {
			staff = commonService.findStaffById(Long.parseLong(prmStaffCode));
		} catch (NumberFormatException e) {			
			e.printStackTrace();
			logger.error(e.getMessage());
			return FWD_GLOBAL_ERROR;
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			return FWD_GLOBAL_ERROR;
		}
		
		return FWD_CREATE_STAFF;
	}
	
	public String getStaffHeader() {
		String header = getMessage("newStaffHeader", null);
		
		if(editMode) {
			header = getMessage("editStaffHeader", null);
		}
		
		return header;
	}
	
	public ArrayList<Staff> suggest(Object value) {
		String input = (String) value;

		Staff search = new Staff();
		search.setName(input);

		ArrayList<Staff> listResult = null;
		try {
			listResult = (ArrayList<Staff>)commonService.findStaffByCriteria(search); 
					
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());			
		}

		return listResult;
	}
}
