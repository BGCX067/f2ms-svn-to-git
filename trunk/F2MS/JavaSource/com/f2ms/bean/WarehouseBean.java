package com.f2ms.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;
import org.springframework.orm.hibernate3.HibernateOptimisticLockingFailureException;

import com.f2ms.enumeration.Status;
import com.f2ms.exception.DAOException;
import com.f2ms.model.Warehouse;
import com.f2ms.service.warehouse.IWarehouseService;
import com.f2ms.util.F2MSUtil;
import com.f2ms.util.IF2MSConstants;

/**
 * WarehouseBean
 * 
 * @author Isak Rabin
 * 
 */
public class WarehouseBean extends BaseBean {
	private static final String FWD_SEARCH_WAREHOUSE = "searchWarehouse";

	private static final String FWD_CREATE_WAREHOUSE = "createWarehouse";

	private static Logger logger = Logger.getLogger(WarehouseBean.class);

	private Warehouse warehouse;
	private IWarehouseService warehouseService;

	private Boolean editMode;

	// search criteria
	private Long id;
	private String code;
	private String description;
	private String address;
	private Integer status = null;

	public Warehouse getWarehouse() {
		return warehouse;
	}

	public IWarehouseService getWarehouseService() {
		return warehouseService;
	}

	public Boolean getEditMode() {
		return editMode;
	}

	public Long getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public String getAddress() {
		return address;
	}

	public Integer getStatus() {
		return status;
	}

	public void setWarehouse(Warehouse warehouse) {
		this.warehouse = warehouse;
	}

	public void setWarehouseService(IWarehouseService warehouseService) {
		this.warehouseService = warehouseService;
	}

	public void setEditMode(Boolean editMode) {
		this.editMode = editMode;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@PostConstruct
	public void init() {
		warehouse = new Warehouse();
	}

	public String create() {
		editMode = false;
		return FWD_CREATE_WAREHOUSE;
	}

	public String save() {

		String returnPage = FWD_SEARCH_WAREHOUSE;
		String userName = (String) F2MSUtil.getSession().getAttribute(
				IF2MSConstants.USER_NAME);

		try {
			warehouse.setChangedBy(userName);

			if (editMode) {
				warehouseService.edit(warehouse);
			} else {
				warehouse.setCreatedBy(userName);
				warehouseService.create(warehouse);
			}
		} catch (HibernateOptimisticLockingFailureException fe) {
			fe.printStackTrace();

			FacesContext context = FacesContext.getCurrentInstance();

			FacesMessage errMsg = new FacesMessage(getMessage("errOptLock",
					null));
			context.addMessage("Warehouse Exception", errMsg);

			returnPage = FWD_CREATE_WAREHOUSE;
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			returnPage = FWD_GLOBAL_ERROR;
		}

		return returnPage;
	}

	public List<Warehouse> getAllWarehouses() {
		try {
			return warehouseService.findAllWarehouses();
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		return null;
	}

	public String search() {
		return FWD_SEARCH_WAREHOUSE;
	}

	public List<Warehouse> getListResult() {
		try {
			Warehouse searchCriteria = new Warehouse();
			searchCriteria.setCode(code);
			searchCriteria.setAddress(address);
			searchCriteria.setDescription(description);
			searchCriteria.setStatus(status);

			return warehouseService.findWarehouseByCriteria(searchCriteria);
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		return null;
	}

	@SuppressWarnings("rawtypes")
	public String edit() {
		editMode = true;

		FacesContext context = FacesContext.getCurrentInstance();
		Map requestMap = context.getExternalContext().getRequestParameterMap();
		String prmIdNo = (String) requestMap.get("id");

		try {
			warehouse = warehouseService.findWarehouseById(Long
					.parseLong(prmIdNo));
		} catch (NumberFormatException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			return FWD_GLOBAL_ERROR;
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			return FWD_GLOBAL_ERROR;
		}

		return FWD_CREATE_WAREHOUSE;
	}

	public String getWarehouseHeader() {
		String header = getMessage("newWarehouseHeader", null);

		if (editMode) {
			header = getMessage("editWarehouseHeader", null);
		}

		return header;
	}

	public List<Warehouse> suggest(Object value) {
		String input = (String) value;

		Warehouse search = new Warehouse();
		search.setDescription(input);
		search.setStatus(Status.ACTIVE.getValue());

		ArrayList<Warehouse> listResult = null;
		try {
			listResult = (ArrayList<Warehouse>) warehouseService
					.findWarehouseByCriteria(search);
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		return listResult;
	}
}
