package com.f2ms.bean;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import com.f2ms.enumeration.BookingStatus;
import com.f2ms.enumeration.BookingType;
import com.f2ms.enumeration.ChargeStatus;
import com.f2ms.enumeration.ChargeType;
import com.f2ms.enumeration.Currency;
import com.f2ms.enumeration.DocumentStatus;
import com.f2ms.enumeration.IdType;
import com.f2ms.enumeration.PackageKind;
import com.f2ms.enumeration.PickupMode;
import com.f2ms.enumeration.ShipmentType;
import com.f2ms.enumeration.Status;
import com.f2ms.enumeration.TranshipmentVia;
import com.f2ms.enumeration.UserStatus;

public class BaseBean {	
	
   public static final String FWD_GLOBAL_ERROR = "global_error";

   protected static ClassLoader getCurrentClassLoader(Object defaultObject) {
      ClassLoader loader = Thread.currentThread().getContextClassLoader();

      if (loader == null) {
         loader = defaultObject.getClass().getClassLoader();
      }

      return loader;
   }

   public static String getMessage(String key, Object params[]) {
      FacesContext context = FacesContext.getCurrentInstance();
      String bundleName = context.getApplication().getMessageBundle();
      Locale locale = context.getViewRoot().getLocale();

      String message = "";

      ResourceBundle bundle = ResourceBundle.getBundle(bundleName, locale, getCurrentClassLoader(params));

      try {
         message = bundle.getString(key);
      }
      catch (MissingResourceException e) {
         message = "?? key " + key + " not found ??";
      }

      if (params != null) {
         MessageFormat mf = new MessageFormat(message, locale);
         message = mf.format(params, new StringBuffer(), null).toString();
      }

      return message;
   }

   public List<SelectItem> getIdTypeList() {
      List<SelectItem> items = new ArrayList<SelectItem>();
      List<IdType> listTemp = Arrays.asList(IdType.values());

      for (Iterator<IdType> iterator = listTemp.iterator(); iterator.hasNext();) {
         IdType idType = iterator.next();
         items.add(new SelectItem(idType.getValue(), idType.getText()));
      }

      return items;
   }

   public List<SelectItem> getShipmentTypeList() {
      List<SelectItem> items = new ArrayList<SelectItem>();
      List<ShipmentType> listTemp = Arrays.asList(ShipmentType.values());

      for (Iterator<ShipmentType> iterator = listTemp.iterator(); iterator.hasNext();) {
         ShipmentType shipmentType = iterator.next();
         items.add(new SelectItem(shipmentType.getValue(), shipmentType.getText()));
      }

      return items;
   }
   
   

   public List<SelectItem> getTranshipmentTypeList() {
      List<SelectItem> items = new ArrayList<SelectItem>();
      List<TranshipmentVia> listTemp = Arrays.asList(TranshipmentVia.values());

      for (Iterator<TranshipmentVia> iterator = listTemp.iterator(); iterator.hasNext();) {
         TranshipmentVia transhipmentVia = iterator.next();
         items.add(new SelectItem(transhipmentVia.getValue(), transhipmentVia.getText()));
      }

      return items;
   }

   public List<SelectItem> getBookingStatusList() {
      List<SelectItem> items = new ArrayList<SelectItem>();
      List<BookingStatus> listTemp = Arrays.asList(BookingStatus.values());

      for (Iterator<BookingStatus> iterator = listTemp.iterator(); iterator.hasNext();) {
         BookingStatus bookingStatus = iterator.next();
         items.add(new SelectItem(bookingStatus.getValue(), bookingStatus.getText()));
      }

      return items;
   }

   public List<SelectItem> getStatusList() {
      List<SelectItem> items = new ArrayList<SelectItem>();
      List<Status> listTemp = Arrays.asList(Status.values());

      for (Iterator<Status> iterator = listTemp.iterator(); iterator.hasNext();) {
         Status status = iterator.next();
         items.add(new SelectItem(status.getValue(), status.getText()));
      }

      return items;

   }

   public List<SelectItem> getBookingTypeList() {
      List<SelectItem> items = new ArrayList<SelectItem>();
      List<BookingType> listTemp = Arrays.asList(BookingType.values());

      for (Iterator<BookingType> iterator = listTemp.iterator(); iterator.hasNext();) {
         BookingType bookingType = iterator.next();
         items.add(new SelectItem(bookingType.getValue(), bookingType.getText()));
      }

      return items;
   }

   public List<SelectItem> getPackageKindList() {
      List<SelectItem> items = new ArrayList<SelectItem>();
      List<PackageKind> listTemp = Arrays.asList(PackageKind.values());

      for (Iterator<PackageKind> iterator = listTemp.iterator(); iterator.hasNext();) {
         PackageKind packageKind = iterator.next();
         items.add(new SelectItem(packageKind.getValue(), packageKind.getText()));
      }

      return items;
   }

   public List<SelectItem> getCurrencyList() {
      List<SelectItem> items = new ArrayList<SelectItem>();
      List<Currency> listTemp = Arrays.asList(Currency.values());

      for (Iterator<Currency> iterator = listTemp.iterator(); iterator.hasNext();) {
         Currency currency = iterator.next();
         items.add(new SelectItem(currency.getValue(), currency.getText()));
      }

      return items;
   }

   public List<SelectItem> getDocumentStatusList() {
      List<SelectItem> items = new ArrayList<SelectItem>();
      List<DocumentStatus> listTemp = Arrays.asList(DocumentStatus.values());

      for (Iterator<DocumentStatus> iterator = listTemp.iterator(); iterator.hasNext();) {
         DocumentStatus documentStatus = iterator.next();
         items.add(new SelectItem(documentStatus.getValue(), documentStatus.getText()));
      }

      return items;

   }

   public List<SelectItem> getPickupModeList() {
      List<SelectItem> items = new ArrayList<SelectItem>();
      List<PickupMode> listTemp = Arrays.asList(PickupMode.values());

      for (Iterator<PickupMode> iterator = listTemp.iterator(); iterator.hasNext();) {
         PickupMode pickupMode = iterator.next();
         items.add(new SelectItem(pickupMode.getValue(), pickupMode.getText()));
      }

      return items;

   }

   public List<SelectItem> getChargesTypeList() {
      List<SelectItem> items = new ArrayList<SelectItem>();
      List<ChargeType> listTemp = Arrays.asList(ChargeType.values());

      for (Iterator<ChargeType> iterator = listTemp.iterator(); iterator.hasNext();) {
         ChargeType chargeType = iterator.next();
         items.add(new SelectItem(chargeType.getValue(), chargeType.getText()));
      }

      return items;
   }

   public List<SelectItem> getChargesStatusList() {
      List<SelectItem> items = new ArrayList<SelectItem>();
      List<ChargeStatus> listTemp = Arrays.asList(ChargeStatus.values());

      for (Iterator<ChargeStatus> iterator = listTemp.iterator(); iterator.hasNext();) {
         ChargeStatus chargeStatus = iterator.next();
         items.add(new SelectItem(chargeStatus.getValue(), chargeStatus.getText()));
      }

      return items;
   }

   public List<SelectItem> getUserStatusList() {
      List<SelectItem> items = new ArrayList<SelectItem>();
      List<UserStatus> listTemp = Arrays.asList(UserStatus.values());

      for (Iterator<UserStatus> iterator = listTemp.iterator(); iterator.hasNext();) {
         UserStatus userStatus = iterator.next();
         items.add(new SelectItem(userStatus.getValue(), userStatus.getText()));
      }

      return items;
   }
		
}
