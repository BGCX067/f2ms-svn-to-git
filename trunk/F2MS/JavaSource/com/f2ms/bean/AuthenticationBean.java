package com.f2ms.bean;

import java.io.IOException;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.security.core.context.SecurityContextHolder;

import com.f2ms.exception.DAOException;
import com.f2ms.model.User;
import com.f2ms.service.common.ICommonService;
import com.f2ms.util.F2MSUtil;
import com.f2ms.util.IF2MSConstants;

public class AuthenticationBean extends BaseBean {
	
	private static Logger logger = Logger.getLogger(AuthenticationBean.class);

    private static final String FWD_CHANGE_PSWD = "changePswd";
	private static final String FWD_LOGOUT = "logout";
	
	private ICommonService commonService;
	
    private String j_username;
    private String j_password;
    
    private String oldPassword;
    private String newPassword;
    private String confirmNewPassword;
    
    private boolean displayLoginError;        
    
    public ICommonService getCommonService() {
		return commonService;
	}

	public void setCommonService(ICommonService commonService) {
		this.commonService = commonService;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getConfirmNewPassword() {
		return confirmNewPassword;
	}

	public void setConfirmNewPassword(String confirmNewPassword) {
		this.confirmNewPassword = confirmNewPassword;
	}

	public String getJ_username() {
        return j_username;
    }

    public void setJ_username(String j_username) {
        this.j_username = j_username;
    }

    public String getJ_password() {
        return j_password;
    }

    public void setJ_password(String j_password) {
        this.j_password = j_password;
    }
        

    public boolean isDisplayLoginError() {
		return displayLoginError;
	}

	public void setDisplayLoginError(boolean displayLoginError) {
		this.displayLoginError = displayLoginError;
	}

	public String authenticate() {
        try {        	
        	
			ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();

			RequestDispatcher dispatcher = ((ServletRequest) context.getRequest()).getRequestDispatcher("/j_spring_security_check");

			dispatcher.forward((ServletRequest) context.getRequest(),
			        (ServletResponse) context.getResponse());

			FacesContext.getCurrentInstance().responseComplete();
			
			if(SecurityContextHolder.getContext().getAuthentication() == null) {
				displayLoginError = true;				
				throw new Exception("Fail to authenticate");
			}
					
			F2MSUtil.getSession().setAttribute(IF2MSConstants.USER_NAME, F2MSUtil.getCurrentUserName());
			F2MSUtil.getSession().setAttribute(IF2MSConstants.ROLES, F2MSUtil.getCurrentRoles());

			F2MSUtil.getSession().setAttribute("isAdmin", F2MSUtil.isAdmin());
			F2MSUtil.getSession().setAttribute("isStaff", F2MSUtil.isStaff());
			F2MSUtil.getSession().setAttribute("isManager", F2MSUtil.isManager());
			F2MSUtil.getSession().setAttribute("isAgent", F2MSUtil.isAgent());
			
		} catch (ServletException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();		
			logger.error(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();		
			logger.error(e.getMessage());
		}
		
		F2MSUtil.getSession().setAttribute("displayLoginError", displayLoginError);

        return null;
    }

    public String logout() {
    	FacesContext context = FacesContext.getCurrentInstance();
        ((HttpSession) context.getExternalContext().getSession(false)).invalidate();

        return FWD_LOGOUT;
    }
    
    public String initChangePassword() {
    	return FWD_CHANGE_PSWD;
    }
    
    public String changePassword() {
    	FacesContext context = FacesContext.getCurrentInstance();
    	String userName = (String) F2MSUtil.getSession().getAttribute(IF2MSConstants.USER_NAME);
    	    	    	
    	try {
			User user = commonService.findUserByUserName(userName);
			
			if(user != null) {
				if(!StringUtils.equals(oldPassword.trim(), user.getPassword().trim())) {					
					FacesMessage message = new FacesMessage(getMessage("msgOldPassword", null));					
					context.addMessage("Change Password Exception", message);
					
					return FWD_CHANGE_PSWD;
				}
				
				if(!StringUtils.equals(newPassword.trim(), confirmNewPassword.trim())) {					
					FacesMessage message = new FacesMessage(getMessage("msgNewPswdNotMatch", null));					
					context.addMessage("Change Password Exception", message);
					
					return FWD_CHANGE_PSWD;
				}					
				
				user.setPassword(newPassword);
				user.setChangedBy(userName);
				
				commonService.editUserOnly(user);
			}
			
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			return FWD_GLOBAL_ERROR;
		}
				
		FacesMessage message = new FacesMessage(getMessage("msgPswdSuccess", null));
		context.addMessage("Change Password Exception", message);
    	    	
    	return FWD_CHANGE_PSWD;
    }
}
