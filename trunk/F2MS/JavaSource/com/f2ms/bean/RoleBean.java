package com.f2ms.bean;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;
import org.springframework.orm.hibernate3.HibernateOptimisticLockingFailureException;

import com.f2ms.exception.DAOException;
import com.f2ms.model.Role;
import com.f2ms.service.common.ICommonService;
import com.f2ms.util.F2MSUtil;
import com.f2ms.util.IF2MSConstants;

public class RoleBean extends BaseBean {
	private static Logger logger = Logger.getLogger(RoleBean.class);
	
	private static final String FWD_SEARCH_ROLE = "searchRole";
	private static final String FWD_CREATE_ROLE = "createRole";
	private Role role;
	private ICommonService commonService;
	
	private String roleCode;
	private boolean editMode;
	
	// search criteria
	private String roleDesc;

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public ICommonService getCommonService() {
		return commonService;
	}

	public void setCommonService(ICommonService commonService) {
		this.commonService = commonService;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public boolean isEditMode() {
		return editMode;
	}

	public void setEditMode(boolean editMode) {
		this.editMode = editMode;
	}

	public String getRoleDesc() {
		return roleDesc;
	}

	public void setRoleDesc(String roleDesc) {
		this.roleDesc = roleDesc;
	}
	
	@PostConstruct
	public void init() {
		role = new Role();
	}
	
	public String create() {
		editMode = false;
		
		return FWD_CREATE_ROLE;
	}
	
	public String save() {
		String returnPage = FWD_SEARCH_ROLE;
		String userName = (String) F2MSUtil.getSession().getAttribute(IF2MSConstants.USER_NAME);
		
		try {
			role.setChangedBy(userName);
			
			if(editMode) {
				commonService.editRole(role);
			} else {
				role.setCreatedBy(userName);
				commonService.createRole(role);
			}
		} catch (HibernateOptimisticLockingFailureException fe){			
			fe.printStackTrace();
			
			FacesContext context = FacesContext.getCurrentInstance();
			
			FacesMessage errMsg = new FacesMessage(getMessage("errOptLock", null));								
			context.addMessage("Role Exception", errMsg);						
			
			returnPage = FWD_CREATE_ROLE;
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			returnPage = FWD_GLOBAL_ERROR;
		}
	
		return returnPage;
	}
	
	public String search() {
		return FWD_SEARCH_ROLE;
	}
	
	public List<Role> getListResult() {
		try {
			Role searchCriteria = new Role();
			searchCriteria.setRoleCode(roleCode);
			searchCriteria.setRoleDesc(roleDesc);
			
			return commonService.findRoleByCriteria(searchCriteria);
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		
		return null;
	}
	
	public String edit() {
		editMode = true;
		
		FacesContext context = FacesContext.getCurrentInstance();  
		@SuppressWarnings("rawtypes")
		Map requestMap = context.getExternalContext().getRequestParameterMap();  
		String prmRoleCode = (String) requestMap.get("roleCode");
		
		try {
			role = commonService.findRoleById(prmRoleCode);
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			return FWD_GLOBAL_ERROR;
		}
		
		return FWD_CREATE_ROLE;
	}
	
	public String getRoleHeader() {
		String header = getMessage("newRoleHeader", null);
		
		if(editMode) {
			header = getMessage("editRoleHeader", null);
		}
		
		return header;
	}
	
	
}
