package com.f2ms.bean;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import net.sf.jasperreports.engine.JRException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.orm.hibernate3.HibernateOptimisticLockingFailureException;

import com.f2ms.enumeration.ContainerType;
import com.f2ms.enumeration.ShipmentStatus;
import com.f2ms.enumeration.Status;
import com.f2ms.exception.DAOException;
import com.f2ms.model.Booking;
import com.f2ms.model.CargoType;
import com.f2ms.model.Customer;
import com.f2ms.model.Location;
import com.f2ms.model.Shipment;
import com.f2ms.model.ShipmentItem;
import com.f2ms.model.ShipmentMode;
import com.f2ms.service.booking.IBookingService;
import com.f2ms.service.cargotype.ICargoTypeService;
import com.f2ms.service.customer.ICustomerService;
import com.f2ms.service.location.ILocationService;
import com.f2ms.service.shipment.IShipmentService;
import com.f2ms.service.shipmentmode.IShipmentModeService;
import com.f2ms.util.F2MSUtil;
import com.f2ms.util.IF2MSConstants;
import com.f2ms.util.JasperReportUtil;

/**
 * ShipmentBean
 * 
 * @author Isak Rabin
 * 
 */
public class ShipmentBean extends BaseBean {
	private static Logger logger = Logger.getLogger(ShipmentBean.class);

	private static final String EDIT_SHIPMENT_HEADER = "editShipmentHeader";
	private static final String NEW_SHIPMENT_HEADER = "newShipmentHeader";
	// navigation constant
	private static final String CREATE_SHIPMENT_STEP1 = "createShipment1";
	private static final String CREATE_SHIPMENT_STEP2 = "createShipment2";
	private static final String SEARCH_SHIPMENT = "searchShipment";

	private Shipment shipment;
	private IBookingService bookingService;
	private IShipmentService shipmentService;
	private IShipmentModeService shipmentModeService;
	private ILocationService locationService;
	private ICustomerService customerService;
	private ICargoTypeService cargoTypeService;

	// flag edit Mode
	private boolean editMode;

	// Shipment/Booking Search criteria
	private Date startDateBookingPeriod;
	private Date endDateBookingPeriod;

	// Booking Search Criteria
	private Long pol;
	private Long pod;
	private Long customer;

	// Output booking search
	private Date docDate;
	private String docNo;
	private String shipmentType;
	private String goodDescription;
	private Double grossWeight;
	private Double measurement;
	private String status;

	// Shipment Search criteria
	private String bookingNo;
	private String consignee;
	private String shipper;

	// Output Shipment Search
	private String packagingNo;

	// selected booking for packaging
	private Booking booking;
	private final Map<String, Boolean> selectedBookingIds = new HashMap<String, Boolean>();
	private List<Booking> bookingData = new ArrayList<Booking>();
	private List<ShipmentItem> selectedBookingList = new ArrayList<ShipmentItem>();

	public Booking getBooking() {
		return booking;
	}

	public void setBooking(Booking booking) {
		this.booking = booking;
	}

	public Shipment getShipment() {
		return shipment;
	}

	public void setShipment(Shipment shipment) {
		this.shipment = shipment;
	}

	public IBookingService getBookingService() {
		return bookingService;
	}

	public void setBookingService(IBookingService bookingService) {
		this.bookingService = bookingService;
	}

	public IShipmentService getShipmentService() {
		return shipmentService;
	}

	public void setShipmentService(IShipmentService shipmentService) {
		this.shipmentService = shipmentService;
	}

	public IShipmentModeService getShipmentModeService() {
		return shipmentModeService;
	}

	public void setShipmentModeService(IShipmentModeService shipmentModeService) {
		this.shipmentModeService = shipmentModeService;
	}

	public ILocationService getLocationService() {
		return locationService;
	}

	public void setLocationService(ILocationService locationService) {
		this.locationService = locationService;
	}

	public ICustomerService getCustomerService() {
		return customerService;
	}

	public void setCustomerService(ICustomerService customerService) {
		this.customerService = customerService;
	}

	public ICargoTypeService getCargoTypeService() {
		return cargoTypeService;
	}

	public void setCargoTypeService(ICargoTypeService cargoTypeService) {
		this.cargoTypeService = cargoTypeService;
	}

	public boolean isEditMode() {
		return editMode;
	}

	public void setEditMode(boolean editMode) {
		this.editMode = editMode;
	}

	public Date getStartDateBookingPeriod() {
		return startDateBookingPeriod;
	}

	public void setStartDateBookingPeriod(Date startDateBookingPeriod) {
		this.startDateBookingPeriod = startDateBookingPeriod;
	}

	public Date getEndDateBookingPeriod() {
		return endDateBookingPeriod;
	}

	public void setEndDateBookingPeriod(Date endDateBookingPeriod) {
		this.endDateBookingPeriod = endDateBookingPeriod;
	}

	public Long getPol() {
		return pol;
	}

	public void setPol(Long pol) {
		this.pol = pol;
	}

	public Long getPod() {
		return pod;
	}

	public void setPod(Long pod) {
		this.pod = pod;
	}

	public Long getCustomer() {
		return customer;
	}

	public void setCustomer(Long customer) {
		this.customer = customer;
	}

	public Date getDocDate() {
		return docDate;
	}

	public void setDocDate(Date docDate) {
		this.docDate = docDate;
	}

	public String getDocNo() {
		return docNo;
	}

	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}

	public String getShipmentType() {
		return shipmentType;
	}

	public void setShipmentType(String shipmentType) {
		this.shipmentType = shipmentType;
	}

	public String getGoodDescription() {
		return goodDescription;
	}

	public void setGoodDescription(String goodDescription) {
		this.goodDescription = goodDescription;
	}

	public Double getGrossWeight() {
		return grossWeight;
	}

	public void setGrossWeight(Double grossWeight) {
		this.grossWeight = grossWeight;
	}

	public Double getMeasurement() {
		return measurement;
	}

	public void setMeasurement(Double measurement) {
		this.measurement = measurement;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getBookingNo() {
		return bookingNo;
	}

	public void setBookingNo(String bookingNo) {
		this.bookingNo = bookingNo;
	}

	public String getConsignee() {
		return consignee;
	}

	public void setConsignee(String consignee) {
		this.consignee = consignee;
	}

	public String getShipper() {
		return shipper;
	}

	public void setShipper(String shipper) {
		this.shipper = shipper;
	}

	public String getPackagingNo() {
		return packagingNo;
	}

	public void setPackagingNo(String packagingNo) {
		this.packagingNo = packagingNo;
	}

	public List<ShipmentItem> getSelectedBookingList() {
		if (bookingData != null) {
			for (Booking dataItem : bookingData) {
				Boolean selectedBooking = selectedBookingIds.get(dataItem
						.getDocNo());
				if (selectedBooking != null && selectedBooking.booleanValue()) {

					ShipmentItem shipmentItem = new ShipmentItem();
					shipmentItem.setBooking(dataItem);
					shipmentItem.setShipment(shipment);

					selectedBookingList.add(shipmentItem);
					selectedBookingIds.remove(dataItem.getDocNo()); // Reset.
				}
			}
		}

		return selectedBookingList;
	}

	public void setSelectedBookingList(List<ShipmentItem> selectedBookingList) {
		this.selectedBookingList = selectedBookingList;
	}

	public Map<String, Boolean> getSelectedBookingIds() {
		return selectedBookingIds;
	}

	@PostConstruct
	public void init() {

	}

	public String create() {
		editMode = false;
		shipment = new Shipment();
		selectedBookingList = new ArrayList<ShipmentItem>();
		return CREATE_SHIPMENT_STEP1;
	}

	public String createShipmentHeader() {
		selectedBookingList = new ArrayList<ShipmentItem>();
		return CREATE_SHIPMENT_STEP2;
	}

	public String save() {
		String userName = (String) F2MSUtil.getSession().getAttribute(
				IF2MSConstants.USER_NAME);

		List<FacesMessage> errMessages = validate();
		if (!errMessages.isEmpty()) {

			FacesContext context = FacesContext.getCurrentInstance();
			for (FacesMessage errMsg : errMessages) {
				context.addMessage("Shipment Validation", errMsg);
			}

			return CREATE_SHIPMENT_STEP2;
		}

		String returnPage = SEARCH_SHIPMENT;

		try {
			shipment.setChangedBy(userName);

			if (editMode) {
				shipmentService.edit(shipment);
			} else {
				Set<ShipmentItem> items = new HashSet<ShipmentItem>();
				for (ShipmentItem item : selectedBookingList) {
					item.setCreatedBy(shipment.getCreatedBy());
					item.setCreatedOn(shipment.getCreatedOn());
					item.setChangedBy(shipment.getChangedBy());
					item.setChangedOn(shipment.getChangedOn());
					item.setShipment(shipment);
					items.add(item);
				}
				shipment.setShipmentItems(items);
				shipment.setCreatedBy(userName);
				shipmentService.create(shipment);
			}
		} catch (HibernateOptimisticLockingFailureException fe) {
			fe.printStackTrace();

			FacesContext context = FacesContext.getCurrentInstance();

			FacesMessage errMsg = new FacesMessage(getMessage("errOptLock",
					null));
			context.addMessage("Shipment Exception", errMsg);

			returnPage = CREATE_SHIPMENT_STEP1;
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			returnPage = FWD_GLOBAL_ERROR;
		}

		return returnPage;
	}

	@SuppressWarnings("rawtypes")
	public String edit() {
		editMode = true;

		FacesContext context = FacesContext.getCurrentInstance();
		Map requestMap = context.getExternalContext().getRequestParameterMap();
		String prmIdNo = (String) requestMap.get("id");

		try {
			shipment = shipmentService.findShipmentById(prmIdNo);
			selectedBookingList.clear();
			Iterator<ShipmentItem> it = shipment.getShipmentItems().iterator();
			while (it.hasNext()) {
				// Get element
				ShipmentItem element = it.next();
				selectedBookingList.add(element);
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			return FWD_GLOBAL_ERROR;
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			return FWD_GLOBAL_ERROR;
		}

		return CREATE_SHIPMENT_STEP2;
	}

	public String search() {
		return SEARCH_SHIPMENT;
	}

	public String searchBooking() {
		return CREATE_SHIPMENT_STEP1;
	}

	@SuppressWarnings("rawtypes")
	public void printExportCert() {
		FacesContext context = FacesContext.getCurrentInstance();
		Map requestMap = context.getExternalContext().getRequestParameterMap();
		String prmIdNo = (String) requestMap.get("id");

		try {
			JasperReportUtil.printExportCertificate(prmIdNo);
		} catch (JRException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (SQLException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	@SuppressWarnings("rawtypes")
	public void printShipmentCoversheet() {
		FacesContext context = FacesContext.getCurrentInstance();
		Map requestMap = context.getExternalContext().getRequestParameterMap();
		String prmIdNo = (String) requestMap.get("id");

		try {
			JasperReportUtil.printShipmentCoversheet(prmIdNo);
		} catch (JRException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (SQLException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	public List<Booking> getAvailableBooking() {
		try {
			bookingData = bookingService.findAvailableBooking(
					startDateBookingPeriod, endDateBookingPeriod, pol, pod,
					customer);

			return bookingData;
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		return null;
	}

	public String getShipmentHeader() {
		String header = getMessage(NEW_SHIPMENT_HEADER, null);

		if (editMode) {
			header = getMessage(EDIT_SHIPMENT_HEADER, null);
		}

		return header;
	}

	public List<Shipment> getAllShipments() {
		try {
			return shipmentService.findAllShipments();
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		return null;
	}

	public List<Shipment> getListResult() {
		try {

			Shipment searchCriteria = new Shipment();
			searchCriteria.setPackagingNo(docNo);

			return shipmentService.findShipmentByCriteria(searchCriteria);

		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		return null;
	}

	public List<SelectItem> getShipmentModeList() {

		List<SelectItem> items = new ArrayList<SelectItem>();

		try {

			ShipmentMode search = new ShipmentMode();
			search.setStatus(Integer.valueOf(Status.ACTIVE.getValue()));
			List<ShipmentMode> listTemp = shipmentModeService
					.findAllShipmentModes();

			for (Iterator<ShipmentMode> iterator = listTemp.iterator(); iterator
					.hasNext();) {
				ShipmentMode shipmentMode = iterator.next();
				items.add(new SelectItem(shipmentMode.getId(), shipmentMode
						.getDescription()));
			}

		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		return items;
	}

	public List<SelectItem> getContainerTypeList() {

		List<SelectItem> items = new ArrayList<SelectItem>();
		List<ContainerType> listTemp = Arrays.asList(ContainerType.values());

		for (Iterator<ContainerType> iterator = listTemp.iterator(); iterator
				.hasNext();) {
			ContainerType containerType = iterator.next();
			items.add(new SelectItem(containerType.getValue(), containerType
					.getText()));
		}

		return items;
	}

	public List<SelectItem> getCargoTypeList() {

		List<SelectItem> items = new ArrayList<SelectItem>();

		try {

			CargoType search = new CargoType();
			search.setStatus(Integer.valueOf(Status.ACTIVE.getValue()));
			List<CargoType> listTemp = cargoTypeService
					.findCargoTypeByCriteria(search);

			for (Iterator<CargoType> iterator = listTemp.iterator(); iterator
					.hasNext();) {
				CargoType cargoType = iterator.next();
				items.add(new SelectItem(cargoType.getId(), cargoType
						.getDescription()));
			}

		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		return items;
	}

	public List<Booking> suggest(Object value) {
		String input = (String) value;

		Booking search = new Booking();
		search.setDocNo(input);

		List<Booking> listResult = null;
		try {
			listResult = bookingService.findBookingByCriteria(search);
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		return listResult;
	}

	private List<FacesMessage> validate() {
		List<FacesMessage> listMessage = new ArrayList<FacesMessage>();
		FacesMessage message = null;

		if (shipment.getPackagingDate() == null) {
			message = new FacesMessage(
					getMessage("requiredPackagingDate", null));
			listMessage.add(message);
		}

		if (shipment.getShipmentType() == null) {
			message = new FacesMessage(getMessage("requiredShipmentType", null));
			listMessage.add(message);
		}

		validateShipmentMode(listMessage);

		if (shipment.getShipmentDate() == null) {
			message = new FacesMessage(getMessage("requiredShipmentDate", null));
			listMessage.add(message);
		}

		validateConsignee(listMessage);

		validateCarrier(listMessage);

		if (shipment.getVesselName() == null) {
			message = new FacesMessage(getMessage("requiredShipmentVesselName",
					null));
			listMessage.add(message);
		}

		if (shipment.getVoyageNo() == null) {
			message = new FacesMessage(getMessage("requiredShipmentVoyageNo",
					null));
			listMessage.add(message);
		}

		validatePOL(listMessage);

		validatePOD(listMessage);

		if (StringUtils.isBlank(shipment.getContainerNo())) {
			message = new FacesMessage(getMessage(
					"requiredShipmentContainerNo", null));
			listMessage.add(message);
		}

		if (shipment.getCargoType() == null
				|| shipment.getCargoType().getId() == null
				|| shipment.getCargoType().getId().intValue() == 0) {
			message = new FacesMessage(getMessage(
					"requiredShipmentContainerType", null));
			listMessage.add(message);
		} else {
			boolean validCargoType = true;
			try {
				CargoType cargoType = cargoTypeService
						.findCargoTypeById(shipment.getCargoType().getId());
				if (cargoType == null) {
					validCargoType = false;
				}
			} catch (DAOException e) {
				validCargoType = false;
				e.printStackTrace();
				logger.error(e.getMessage());
			}

			if (!validCargoType) {
				message = new FacesMessage(getMessage("invalidCargoType", null));
				listMessage.add(message);
			}
		}

		if (shipment.getMaximumLoad() == null) {
			message = new FacesMessage(getMessage(
					"requiredShipmentMaximumLoad", null));
			listMessage.add(message);
		}

		if (shipment.getMaximumMeasurement() == null) {
			message = new FacesMessage(getMessage(
					"requiredShipmentMaximumMeasurement", null));
			listMessage.add(message);
		}

		if (shipment.getTotalVolume() == null) {
			message = new FacesMessage(getMessage(
					"requiredShipmentTotalVolume", null));
			listMessage.add(message);
		} else {
			if (shipment.getTotalVolume().doubleValue() > shipment
					.getMaximumMeasurement().doubleValue()) {
				message = new FacesMessage(getMessage(
						"invalidShipmentTotalVolume", null));
				listMessage.add(message);
			}
		}

		if (shipment.getTotalWeight() == null) {
			message = new FacesMessage(getMessage(
					"requiredShipmentTotalWeight", null));
			listMessage.add(message);
		} else {
			if (shipment.getTotalWeight().doubleValue() > shipment
					.getMaximumLoad().doubleValue()) {
				message = new FacesMessage(getMessage(
						"invalidShipmentTotalWeight", null));
				listMessage.add(message);
			}
		}

		validateStatus(listMessage);

		validateShipmentItem(listMessage);

		return listMessage;
	}

	private void validateShipmentItem(List<FacesMessage> listMessage) {
		FacesMessage message;
		if (selectedBookingList == null || selectedBookingList.size() == 0) {
			message = new FacesMessage(getMessage("requiredShipmentItem", null));
			listMessage.add(message);
		} else {
			if (selectedBookingList.size() > 1) {
				boolean sameDestination = true;
				ShipmentItem firstItem = selectedBookingList.get(0);
				for (ShipmentItem selected : selectedBookingList) {
					if (selected.getBooking().getPodId().intValue() != firstItem
							.getBooking().getPolId().intValue()
							|| selected.getBooking().getPodId().intValue() != firstItem
									.getBooking().getPolId().intValue()) {
						sameDestination = false;
						break;
					}
				}

				if (!sameDestination) {
					message = new FacesMessage(getMessage(
							"requiredShipmentItemSameDestination", null));
					listMessage.add(message);
				}
			}
		}
	}

	private void validateCarrier(List<FacesMessage> listMessage) {
		FacesMessage message;
		if (shipment.getCarrier() == null
				|| shipment.getCarrier().getCustCode() == null
				|| shipment.getCarrier().getCustCode().intValue() == 0) {
			message = new FacesMessage(getMessage("requiredShipmentShipper",
					null));
			listMessage.add(message);
		} else {
			boolean validShipper = true;
			Long custCode = shipment.getConsignee().getCustCode();
			try {
				Customer customer = customerService.findCustomerById(custCode);
				if (customer == null) {
					validShipper = false;
				}
			} catch (DAOException e) {
				validShipper = false;
				e.printStackTrace();
				logger.error(e.getMessage());
			}

			if (!validShipper) {
				message = new FacesMessage(getMessage("invalidShipmentShipper",
						null));
				listMessage.add(message);
			}
		}
	}

	private void validateConsignee(List<FacesMessage> listMessage) {
		FacesMessage message;
		if (shipment.getConsignee() == null
				|| shipment.getConsignee().getCustCode() == null
				|| shipment.getConsignee().getCustCode().intValue() == 0) {
			message = new FacesMessage(getMessage("requiredShipmentConsignee",
					null));
			listMessage.add(message);
		} else {
			boolean validConsignee = true;
			Long custCode = shipment.getConsignee().getCustCode();
			try {
				Customer customer = customerService.findCustomerById(custCode);
				if (customer == null) {
					validConsignee = false;
				}
			} catch (DAOException e) {
				validConsignee = false;
				e.printStackTrace();
				logger.error(e.getMessage());
			}

			if (!validConsignee) {
				message = new FacesMessage(getMessage(
						"invalidShipmentConsignee", null));
				listMessage.add(message);
			}
		}
	}

	private void validateStatus(List<FacesMessage> listMessage) {
		FacesMessage message;
		if (shipment.getStatus() == null) {
			message = new FacesMessage(getMessage("requiredShipmentStatus",
					null));
			listMessage.add(message);
		} else {
			if (editMode) {
				if (shipment.getStatus().intValue() == ShipmentStatus.CANCELLED
						.getValue()) {
					if (shipment.getStatusReason() == null) {
						message = new FacesMessage(getMessage(
								"requiredShipmentStatusReason", null));
						listMessage.add(message);
					}
				}
			}
		}
	}

	private void validatePOD(List<FacesMessage> listMessage) {
		FacesMessage message;
		if (shipment.getPod() == null || shipment.getPod().getId() == null
				|| shipment.getPod().getId() == 0) {
			message = new FacesMessage(getMessage("requiredShipmentPOD", null));
			listMessage.add(message);
		} else {
			boolean validPOD = true;
			Long locationId = shipment.getPol().getId();
			try {
				Location location = locationService
						.findLocationById(locationId);
				if (location == null) {
					validPOD = false;
				}
			} catch (DAOException e) {
				validPOD = false;
				e.printStackTrace();
				logger.error(e.getMessage());
			}

			if (!validPOD) {
				message = new FacesMessage(getMessage("invalidShipmentPOD",
						null));
				listMessage.add(message);
			}
		}
	}

	private void validatePOL(List<FacesMessage> listMessage) {
		FacesMessage message;
		if (shipment.getPol() == null || shipment.getPol().getId() == null
				|| shipment.getPol().getId() == 0) {
			message = new FacesMessage(getMessage("requiredShipmentPOL", null));
			listMessage.add(message);
		} else {
			boolean validPOL = true;
			Long locationId = shipment.getPol().getId();
			try {
				Location location = locationService
						.findLocationById(locationId);
				if (location == null) {
					validPOL = false;
				}
			} catch (DAOException e) {
				validPOL = false;
				e.printStackTrace();
				logger.error(e.getMessage());
			}

			if (!validPOL) {
				message = new FacesMessage(getMessage("invalidShipmentPOL",
						null));
				listMessage.add(message);

			}
		}
	}

	private void validateShipmentMode(List<FacesMessage> listMessage) {
		FacesMessage message;
		if (shipment.getShipmentMode() == null
				|| shipment.getShipmentMode().getId() == null
				|| shipment.getShipmentMode().getId().intValue() == 0) {
			message = new FacesMessage(getMessage("requiredShipmentMode", null));
			listMessage.add(message);
		} else {
			boolean validShipmentMode = true;
			try {
				ShipmentMode shipmentMode = shipmentModeService
						.findShipmentModeById(shipment.getShipmentMode()
								.getId());
				if (shipmentMode == null) {
					validShipmentMode = false;
				}
			} catch (DAOException e) {
				validShipmentMode = false;
				e.printStackTrace();
				logger.error(e.getMessage());
			}

			if (!validShipmentMode) {
				message = new FacesMessage(getMessage("invalidShipmentMode",
						null));
				listMessage.add(message);
			}

		}
	}
}
