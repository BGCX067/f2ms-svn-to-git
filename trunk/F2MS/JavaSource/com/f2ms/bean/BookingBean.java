package com.f2ms.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.richfaces.event.UploadEvent;
import org.richfaces.model.UploadItem;
import org.springframework.orm.hibernate3.HibernateOptimisticLockingFailureException;

import com.f2ms.enumeration.BookingStatus;
import com.f2ms.enumeration.ShipmentType;
import com.f2ms.enumeration.Status;
import com.f2ms.exception.DAOException;
import com.f2ms.model.Booking;
import com.f2ms.model.BookingFiles;
import com.f2ms.model.ShipmentMode;
import com.f2ms.service.booking.IBookingService;
import com.f2ms.service.shipmentmode.IShipmentModeService;
import com.f2ms.util.F2MSUtil;
import com.f2ms.util.IF2MSConstants;

public class BookingBean extends BaseBean {
	
	private static Logger logger = Logger.getLogger(BookingBean.class);

   private static final String FWD_SEARCH_BOOKING = "searchBooking";
   private static final String FWD_CREATE_BOOKING = "createBooking";	
   private Booking booking;
   private IBookingService bookingService;
   private IShipmentModeService shipmentModeService;
   private boolean editMode;
   // Search criteria
   private String docNo;
   private String status;
   private String shipmentType;
   private String transhipmentType;
   private String hsCode;
   private String permitNo;
   private int needPickup;
   private int transhipment = ShipmentType.TRANSHIPMENT.getValue();
   private int local = ShipmentType.LOCAL.getValue();

   // upload files
   private List<UploadItem> uploadedFiles;
   private boolean autoUpload;
   private boolean useFlash;
   
   // delete files
   private Map<Long, Boolean> checkDeleted;   
   
   public Map<Long, Boolean> getCheckDeleted() {
	return checkDeleted;
   }

	public void setCheckDeleted(Map<Long, Boolean> checkDeleted) {
		this.checkDeleted = checkDeleted;
	}
	
	public List<UploadItem> getUploadedFiles() {
      return uploadedFiles;
   }

   public void setUploadedFiles(List<UploadItem> uploadedFiles) {
      this.uploadedFiles = uploadedFiles;
   }

   public boolean isAutoUpload() {
      return autoUpload;
   }

   public void setAutoUpload(boolean autoUpload) {
      this.autoUpload = autoUpload;
   }

   public boolean isUseFlash() {
      return useFlash;
   }

   public void setUseFlash(boolean useFlash) {
      this.useFlash = useFlash;
   }

   public IShipmentModeService getShipmentModeService() {
      return shipmentModeService;
   }

   public void setShipmentModeService(IShipmentModeService shipmentModeService) {
      this.shipmentModeService = shipmentModeService;
   }

   public void setBookingService(IBookingService bookingService) {
      this.bookingService = bookingService;
   }

   public Booking getBooking() {
      return booking;
   }

   public void setBooking(Booking booking) {
      this.booking = booking;
   }

   public boolean isEditMode() {
      return editMode;
   }

   public void setEditMode(boolean editMode) {
      this.editMode = editMode;
   }

   public String getDocNo() {
      return docNo;
   }

   public void setDocNo(String docNo) {
      this.docNo = docNo;
   }

   public String getStatus() {
      return status;
   }

   public void setStatus(String status) {
      this.status = status;
   }

   public String getShipmentType() {
      return shipmentType;
   }

   public void setShipmentType(String shipmentType) {
      this.shipmentType = shipmentType;
   }

   public String getTranshipmentType() {
      return transhipmentType;
   }

   public void setTranshipmentType(String transhipmentType) {
      this.transhipmentType = transhipmentType;
   }

   public String getHsCode() {
      return hsCode;
   }

   public void setHsCode(String hsCode) {
      this.hsCode = hsCode;
   }

   public String getPermitNo() {
      return permitNo;
   }

   public void setPermitNo(String permitNo) {
      this.permitNo = permitNo;
   }

   public int getTranshipment() {
      return transhipment;
   }

   public void setTranshipment(int transhipment) {
      this.transhipment = transhipment;
   }

   public int getLocal() {
      return local;
   }

   public void setLocal(int local) {
      this.local = local;
   }

   public int getNeedPickup() {
      return needPickup;
   }

   public void setNeedPickup(int needPickup) {
      this.needPickup = needPickup;
   }

   public IBookingService getBookingService() {
      return bookingService;
   }

   public String create() {
      editMode = false;
      booking = new Booking();
      uploadedFiles = new ArrayList<UploadItem>();

      return FWD_CREATE_BOOKING;
   }

   private List<FacesMessage> validate() {
      List<FacesMessage> listMessage = new ArrayList<FacesMessage>();
      FacesMessage message = null;

      if (StringUtils.isBlank(booking.getBookingType())) {
         message = new FacesMessage(getMessage("requiredBookingType", null));
         listMessage.add(message);
      }

      if (StringUtils.isBlank(booking.getShipmentType())) {
         message = new FacesMessage(getMessage("requiredShipment", null));
         listMessage.add(message);
      }
      
      if(StringUtils.equals(String.valueOf(ShipmentType.TRANSHIPMENT.getValue()), booking.getShipmentType())) {
    	  if(StringUtils.isBlank(booking.getTranshipmentVia())) {
    		  message = new FacesMessage(getMessage("requiredTranshipment", null));
    	      listMessage.add(message);
    	  }
      }

      if (booking.getShipperId() == null) {
         message = new FacesMessage(getMessage("requiredShipper", null));
         listMessage.add(message);
      }

      if (booking.getConsigneeId() == null) {
         message = new FacesMessage(getMessage("requiredConsignee", null));
         listMessage.add(message);
      }

      if (booking.getPlaceOfReceiptId() == null) {
         message = new FacesMessage(getMessage("requiredPlaceOfReceipt", null));
         listMessage.add(message);
      }

      if (booking.getPolId() == null) {
         message = new FacesMessage(getMessage("requiredPOL", null));
         listMessage.add(message);
      }

      if (booking.getPodId() == null) {
         message = new FacesMessage(getMessage("requiredPOD", null));
         listMessage.add(message);
      }

      if (booking.getFinalDestinationId() == null) {
         message = new FacesMessage(getMessage("requiredFinalDestination", null));
         listMessage.add(message);
      }

      if (booking.getGrossWeight() == null) {
         message = new FacesMessage(getMessage("requiredGrossWeight", null));
         listMessage.add(message);
      }

      if (booking.getMeasurement() == null) {
         message = new FacesMessage(getMessage("requiredMeasurement", null));
         listMessage.add(message);
      }

      if (StringUtils.isBlank(booking.getHsCode())) {
         message = new FacesMessage(getMessage("requiredHSCode", null));
         listMessage.add(message);
      }

      if (StringUtils.isBlank(booking.getPermitNo())) {
         message = new FacesMessage(getMessage("requiredPermitNo", null));
         listMessage.add(message);
      }

      return listMessage;
   }

   public String save() {

      String returnPage = FWD_SEARCH_BOOKING;
      String userName = (String) F2MSUtil.getSession().getAttribute(IF2MSConstants.USER_NAME);

      if (!validate().isEmpty()) {
         FacesContext context = FacesContext.getCurrentInstance();
         for (FacesMessage errMsg : validate()) {
            context.addMessage("Booking Validation", errMsg);
         }

         return FWD_CREATE_BOOKING;
      }

      try {
         booking.setChangedBy(userName);
         
         List<BookingFiles> listFiles = new ArrayList<BookingFiles>();

        // Populate attachment
        for (UploadItem uploadItem : uploadedFiles) {
           File savedFile = uploadFile(uploadItem);

           if (savedFile != null && savedFile.exists()) {
              BookingFiles bookingFiles = new BookingFiles();
              bookingFiles.setFilePath(savedFile.getAbsolutePath());
              bookingFiles.setContentType(uploadItem.getContentType());
              bookingFiles.setFileName(uploadItem.getFileName());
              bookingFiles.setFileSize(String.valueOf(uploadItem.getFileSize()));

              listFiles.add(bookingFiles);
           }
        }            
                  
        booking.setListFiles(listFiles);

         if (editMode) {
            bookingService.editBooking(booking);
            
	         // delete files
	            Set<Long> ids = checkDeleted.keySet();
	            for (Long id : ids) {        	         	 
	           	 if(checkDeleted.get(id)) {
	           		 BookingFiles deleted = bookingService.findBookingFileById(id);
	           		 
	           		 if(deleted != null) {
	           			 bookingService.deleteBookingFiles(deleted);
	           		 }        		         		
	           	 }
	   		 }
         } else {
            booking.setCreatedBy(userName);        
            bookingService.createBooking(booking);
         }                 
         
      }
      catch (HibernateOptimisticLockingFailureException fe) {
         fe.printStackTrace();

         FacesContext context = FacesContext.getCurrentInstance();
         FacesMessage errMsg = new FacesMessage(getMessage("errOptLock", null));
         context.addMessage("Booking Exception", errMsg);

         returnPage = FWD_CREATE_BOOKING;
      }
      catch (DAOException e) {
         e.printStackTrace();
      }
      catch (FileNotFoundException e) {
         e.printStackTrace();
      }
      catch (IOException e) {
         e.printStackTrace();
      }

      return returnPage;
   }

   public String edit() {
	  booking = new Booking();
	  checkDeleted = new HashMap<Long, Boolean>();
	  uploadedFiles = new ArrayList<UploadItem>();
      editMode = true;
      
      FacesContext context = FacesContext.getCurrentInstance();  
		@SuppressWarnings("rawtypes")
		Map requestMap = context.getExternalContext().getRequestParameterMap();  
		String prmDocNo = (String) requestMap.get("docNo");

      try {
         booking = bookingService.findBookingById(prmDocNo);
      }
      catch (NumberFormatException e) {
         e.printStackTrace();
      }
      catch (DAOException e) {
         e.printStackTrace();
      }

      return FWD_CREATE_BOOKING;
   }

   public String search() {
      return FWD_SEARCH_BOOKING;
   }

   public List<Booking> getAllBookings() {
      try {
         return bookingService.findAllBooking();
      }
      catch (DAOException e) {
         e.printStackTrace();
      }

      return null;
   }

   public List<Booking> getListResult() {
      try {
         Booking searchCriteria = new Booking();
         searchCriteria.setDocNo(docNo);
         searchCriteria.setStatus(status);
         searchCriteria.setShipmentType(shipmentType);
         searchCriteria.setTranshipmentVia(transhipmentType);
         searchCriteria.setHsCode(hsCode);
         searchCriteria.setPermitNo(permitNo);
         searchCriteria.setNeedPickup(needPickup);

         return bookingService.findBookingByCriteria(searchCriteria);

      }
      catch (DAOException e) {
         e.printStackTrace();
      }

      return null;
   }

   public String getBookingHeader() {
      String header = getMessage("newBookingHeader", null);

      if (editMode) {
         header = getMessage("editBookingHeader", null);
      }

      return header;
   }

   public List<Booking> suggest(Object value) {
      String input = (String) value;

      Booking search = new Booking();
      search.setDocNo(input);

      List<Booking> listResult = null;
      try {
         listResult = bookingService.findBookingByCriteria(search);
      }
      catch (DAOException e) {
         e.printStackTrace();
      }

      return listResult;
   }

   public List<SelectItem> getShipmentModeList() {
      List<SelectItem> items = new ArrayList<SelectItem>();

      ShipmentMode shipmentMode = new ShipmentMode();
      shipmentMode.setStatus(Status.ACTIVE.getValue());

      try {
         List<ShipmentMode> listShipmentModes = shipmentModeService.findShipmentModeByCriteria(shipmentMode);

         for (ShipmentMode temp : listShipmentModes) {
            items.add(new SelectItem(temp.getId(), temp.getDescription()));
         }

      }
      catch (DAOException e) {    	  
         e.printStackTrace();
         logger.error(e.getMessage());
      }

      return items;
   }

   public void uploadListener(UploadEvent event) throws Exception {
      UploadItem item = event.getUploadItem();

      uploadFile(item);
   }

   private File uploadFile(UploadItem item) throws FileNotFoundException, IOException {
      File uploadedFile = item.getFile();
      File savedFile = null;

      if (uploadedFile.exists()) {
         boolean success = false;

         File dir = new File(getDirectory());
         if (!dir.exists()) {
            success = dir.mkdir();
         }
         else {
            success = true;
         }

         if (success) {
            savedFile = new File(getDirectory() + item.getFileName());

            InputStream in = new FileInputStream(uploadedFile);
            OutputStream out = new FileOutputStream(savedFile);

            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
               out.write(buf, 0, len);
            }
            in.close();
            out.close();
         }
      }

      return savedFile;
   }

   private String getDirectory() {
      String directory = IF2MSConstants.DIR_ATTACHMENTS;
      
      File root = new File(directory);
      
      if(!root.exists()) {
    	  root.mkdir();
      }

      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmssms");

      String folder = sdf.format(new Timestamp(new Date().getTime())) + "/";
      directory += folder;

      return directory;
   }

   public String clearUploadData() {

      File dir = new File(getDirectory());
      String[] files = dir.list();

      if (files != null) {
         for (int i = 0; i < files.length; i++) {
            File file = new File(getDirectory() + files[i]);

            if (file.exists()) {
               file.delete();
            }
         }
      }

      uploadedFiles.clear();

      return null;
   }

   public String downloadFile() {
	  FacesContext context = FacesContext.getCurrentInstance();  
	  @SuppressWarnings("rawtypes")
	  Map requestMap = context.getExternalContext().getRequestParameterMap();  
	  String prmIdNo = (String) requestMap.get("fileId");
	  
	  try {
		BookingFiles file = bookingService.findBookingFileById(new Long(prmIdNo));
		
		
		if(file != null) {
			ExternalContext extContext = context.getExternalContext();
			HttpServletResponse response = (HttpServletResponse) extContext.getResponse();
			response.setContentType(file.getContentType());
			response.addHeader("Content-disposition", "attachment; filename=\"" + file.getFileName() +"\"");
			
			byte[] data = getBytesFromFile(new File(file.getFilePath()));
			
			ServletOutputStream os = response.getOutputStream();
			os.write(data);
			os.flush();
			os.close();
			context.responseComplete();
		}		
	} catch (DAOException e) { 		
		e.printStackTrace();
	} catch (IOException e) { 		
		e.printStackTrace();
	} 

      return null;
   }
   
   private byte[] getBytesFromFile(File file) throws IOException {
       InputStream is = new FileInputStream(file);

       // Get the size of the file
       long length = file.length();
       
       byte[] bytes = new byte[(int)length];

       // Read in the bytes
       int offset = 0;
       int numRead = 0;
       while (offset < bytes.length
              && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
           offset += numRead;
       }
       
       if (offset < bytes.length) {
           throw new IOException("Could not completely read file "+file.getName());
       }

       is.close();
       
       return bytes;
   }
   
   public String cancel() {
	   String userName = (String) F2MSUtil.getSession().getAttribute(IF2MSConstants.USER_NAME);
	   
	   try {
		   Booking found = bookingService.findBookingById(booking.getDocNo());
			   
		   if(found != null) {
			   found.setStatus(String.valueOf(BookingStatus.CANCELLED.getValue()));
			   found.setChangedBy(userName);
			   
			   bookingService.editBooking(found);
			   
		   }
		} catch (DAOException e) {		
			e.printStackTrace();
			logger.error(e.getMessage());
			return FWD_GLOBAL_ERROR;
		}
	   
	   return FWD_SEARCH_BOOKING;
   }
   
   public int getCancelledStatus() {
	   return BookingStatus.CANCELLED.getValue();
   }
}
