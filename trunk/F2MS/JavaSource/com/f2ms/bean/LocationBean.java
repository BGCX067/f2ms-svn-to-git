package com.f2ms.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;
import org.springframework.orm.hibernate3.HibernateOptimisticLockingFailureException;

import com.f2ms.enumeration.Status;
import com.f2ms.exception.DAOException;
import com.f2ms.model.Location;
import com.f2ms.service.location.ILocationService;
import com.f2ms.util.F2MSUtil;
import com.f2ms.util.IF2MSConstants;

/**
 * 
 * LocationBean
 * 
 * @author Isak Rabin
 * 
 */
public class LocationBean extends BaseBean {
	
	private static Logger logger = Logger.getLogger(LocationBean.class);

	// Constants
	private static final String SEARCH_LOCATION_PAGE = "searchLocation";
	private static final String CREATE_LOCATION_PAGE = "createLocation";
	private static final String EDIT_LOCATION_HEADER = "editLocationHeader";
	private static final String CREATE_LOCATION_HEADER = "newLocationHeader";

	private Location location;

	private ILocationService locationService;

	private Boolean editMode = false;

	// search criteria
	private Long id;
	private String code;
	private String description;
	private Integer status = null;

	public ILocationService getLocationService() {
		return locationService;
	}

	public void setLocationService(ILocationService locationService) {
		this.locationService = locationService;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Boolean getEditMode() {
		return editMode;
	}

	public void setEditMode(Boolean editMode) {
		this.editMode = editMode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@PostConstruct
	public void init() {
		location = new Location();
	}

	public String create() {
		editMode = false;
		return CREATE_LOCATION_PAGE;
	}

	public String save() {

		String returnPage = SEARCH_LOCATION_PAGE;
		String userName = (String) F2MSUtil.getSession().getAttribute(
				IF2MSConstants.USER_NAME);

		try {
			location.setChangedBy(userName);

			if (editMode) {
				locationService.edit(location);
			} else {
				location.setCreatedBy(userName);
				locationService.create(location);
			}
		} catch (HibernateOptimisticLockingFailureException fe) {
			fe.printStackTrace();

			FacesContext context = FacesContext.getCurrentInstance();

			FacesMessage errMsg = new FacesMessage(getMessage("errOptLock",
					null));
			context.addMessage("Location Exception", errMsg);

			returnPage = CREATE_LOCATION_PAGE;
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			returnPage = FWD_GLOBAL_ERROR;
		}

		return returnPage;
	}

	public List<Location> getAllLocations() {
		try {
			return locationService.findAllLocations();
		} catch (DAOException e) {
			e.printStackTrace();
		}

		return null;
	}

	public String search() {
		return SEARCH_LOCATION_PAGE;
	}

	public List<Location> getListResult() {
		try {
			Location searchCriteria = new Location();
			searchCriteria.setCode(code);
			searchCriteria.setDescription(description);
			searchCriteria.setStatus(status);

			return locationService.findLocationByCriteria(searchCriteria);
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		return null;
	}

	@SuppressWarnings("rawtypes")
	public String edit() {
		editMode = true;

		FacesContext context = FacesContext.getCurrentInstance();
		Map requestMap = context.getExternalContext().getRequestParameterMap();
		String prmIdNo = (String) requestMap.get("id");

		try {
			location = locationService
					.findLocationById(Long.parseLong(prmIdNo));
		} catch (NumberFormatException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			return FWD_GLOBAL_ERROR;
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			return FWD_GLOBAL_ERROR;
		}

		return CREATE_LOCATION_PAGE;
	}

	public List<Location> suggest(Object value) {
		String input = (String) value;

		Location search = new Location();
		search.setDescription(input);
		search.setStatus(Status.ACTIVE.getValue());

		ArrayList<Location> listResult = null;
		try {
			listResult = (ArrayList<Location>) locationService
					.findLocationByCriteria(search);
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		return listResult;
	}

	public String getLocationHeader() {
		String header = getMessage(CREATE_LOCATION_HEADER, null);

		if (editMode) {
			header = getMessage(EDIT_LOCATION_HEADER, null);
		}

		return header;
	}
}
