package com.f2ms.bean;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import net.sf.jasperreports.engine.JRException;

import org.apache.log4j.Logger;
import org.springframework.orm.hibernate3.HibernateOptimisticLockingFailureException;

import com.f2ms.enumeration.DOStatus;
import com.f2ms.exception.DAOException;
import com.f2ms.model.DeliveryOrder;
import com.f2ms.service.deliveryorder.IDeliveryOrderService;
import com.f2ms.util.F2MSUtil;
import com.f2ms.util.IF2MSConstants;
import com.f2ms.util.JasperReportUtil;

public class DeliveryOrderBean extends BaseBean {
	private static Logger logger = Logger.getLogger(DeliveryOrderBean.class);
	
	private static final String FWD_SEARCH_DO = "searchDeliveryOrder";
	private static final String FWD_CREATE_DO = "createDeliveryOrder";
	
	private IDeliveryOrderService deliveryOrderService;
	private DeliveryOrder deliveryOrder;
	
	private String id;
	private boolean editMode;
	
	// Search criteria
	private String docNo;
	private String bookingNo;
	public IDeliveryOrderService getDeliveryOrderService() {
		return deliveryOrderService;
	}
	public void setDeliveryOrderService(IDeliveryOrderService deliveryOrderService) {
		this.deliveryOrderService = deliveryOrderService;
	}
	public DeliveryOrder getDeliveryOrder() {
		return deliveryOrder;
	}
	public void setDeliveryOrder(DeliveryOrder deliveryOrder) {
		this.deliveryOrder = deliveryOrder;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDocNo() {
		return docNo;
	}
	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}
	public String getBookingNo() {
		return bookingNo;
	}
	public void setBookingNo(String bookingNo) {
		this.bookingNo = bookingNo;
	}
	
	
	public boolean isEditMode() {
		return editMode;
	}
	public void setEditMode(boolean editMode) {
		this.editMode = editMode;
	}
	@PostConstruct
	public void init() {
		deliveryOrder = new DeliveryOrder();
	}

	public String create() {
		editMode = false;
		
		return FWD_CREATE_DO; 
	}
	
	public String save() {
		String returnPage = FWD_SEARCH_DO;
		String userName = (String) F2MSUtil.getSession().getAttribute(IF2MSConstants.USER_NAME);
		
		try {
			deliveryOrder.setChangedBy(userName);
			
			if(editMode) {
				deliveryOrderService.editDeliveryOrder(deliveryOrder);
			} else {
				deliveryOrder.setCreatedBy(userName);
				deliveryOrderService.createDeliveryOrder(deliveryOrder);
			}
			
		} catch (HibernateOptimisticLockingFailureException fe){			
			fe.printStackTrace();
			
			FacesContext context = FacesContext.getCurrentInstance();
			
			FacesMessage errMsg = new FacesMessage(getMessage("errOptLock", null));								
			context.addMessage("Delivery Order Exception", errMsg);						
			
			returnPage = FWD_CREATE_DO;
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			returnPage = FWD_GLOBAL_ERROR;
		}
		
		return returnPage;
	}
	
	public String search() {		
		return FWD_SEARCH_DO;
	}
	
	public List<DeliveryOrder> getListResult() {
		try {
			DeliveryOrder searchCriteria = new DeliveryOrder();
			searchCriteria.setBookingNo(bookingNo);
			searchCriteria.setDocNo(docNo);
			
			
			return deliveryOrderService.findDeliveryOrderByCriteria(searchCriteria);
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			
		}
		
		return null;
	}
	
	public String edit() {
		editMode = true;		
		
		FacesContext context = FacesContext.getCurrentInstance();  
		@SuppressWarnings("rawtypes")
		Map requestMap = context.getExternalContext().getRequestParameterMap();  
		String prmId = (String) requestMap.get("id");
		
		try {
			deliveryOrder = deliveryOrderService.findDeliveryOrderById(new Long(prmId));
		} catch (NumberFormatException e) {			
			e.printStackTrace();
			logger.error(e.getMessage());
			return FWD_GLOBAL_ERROR;
		} catch (DAOException e) {			
			e.printStackTrace();
			logger.error(e.getMessage());
			return FWD_GLOBAL_ERROR;
		}
		
		return FWD_CREATE_DO;
	}
	
	public String getDeliveryOrderHeader() {
		String header = getMessage("newDOHeader", null);
		
		if(editMode) {
			header = getMessage("editDOHeader", null);
		}
		
		return header; 
	}
	
	public String print() {	
		FacesContext context = FacesContext.getCurrentInstance();  
		@SuppressWarnings("rawtypes")
		Map requestMap = context.getExternalContext().getRequestParameterMap();  
		String prmIdNo = (String) requestMap.get("idNo");
		
		try {
			DeliveryOrder found = deliveryOrderService.findDeliveryOrderById(new Long(prmIdNo));
			
			if(found != null) {
				found.setStatus(DOStatus.PRINTED.getValue());
				deliveryOrderService.editDeliveryOrder(found);
			}
		} catch (NumberFormatException e1) { 			
			e1.printStackTrace();
			logger.error(e1.getMessage());
			return FWD_GLOBAL_ERROR;
		} catch (DAOException e1) {			
			e1.printStackTrace();
			logger.error(e1.getMessage());
			return FWD_GLOBAL_ERROR;
		}
		
		try {
			JasperReportUtil.printDeliveryOrder(new Long(prmIdNo));
			
		} catch (JRException e) { 			
			e.printStackTrace();
			logger.error(e.getMessage());
			return FWD_GLOBAL_ERROR;
		} catch (IOException e) {			
			e.printStackTrace();
			logger.error(e.getMessage());
			return FWD_GLOBAL_ERROR;
		} catch (SQLException e) {			
			e.printStackTrace();
			logger.error(e.getMessage());
			return FWD_GLOBAL_ERROR;
		}
		
		return FWD_SEARCH_DO;
	}
	
	public String cancel() {
		String userName = (String) F2MSUtil.getSession().getAttribute(IF2MSConstants.USER_NAME);
		
		try {
			DeliveryOrder found = deliveryOrderService.findDeliveryOrderById(deliveryOrder.getId());
			
			if(found != null) {
				found.setStatus(DOStatus.CANCEL.getValue());
				found.setChangedBy(userName);
				
				deliveryOrderService.editDeliveryOrder(found);
			}
			
		} catch (NumberFormatException e) {			
			e.printStackTrace();
			logger.error(e.getMessage());
			return FWD_GLOBAL_ERROR;
		} catch (DAOException e) {			
			e.printStackTrace();
			logger.error(e.getMessage());
			return FWD_GLOBAL_ERROR;
		}
		
		return FWD_SEARCH_DO;
	}
	
	public int getCancelledStatus() {
		return DOStatus.CANCEL.getValue();
	}
	
}
