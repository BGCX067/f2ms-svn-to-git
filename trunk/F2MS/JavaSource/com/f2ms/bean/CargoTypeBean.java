package com.f2ms.bean;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;
import org.springframework.orm.hibernate3.HibernateOptimisticLockingFailureException;

import com.f2ms.exception.DAOException;
import com.f2ms.model.CargoType;
import com.f2ms.service.cargotype.ICargoTypeService;
import com.f2ms.util.F2MSUtil;
import com.f2ms.util.IF2MSConstants;

/**
 * 
 * CargoTypeBean
 * 
 * @author Isak Rabin
 * 
 */
public class CargoTypeBean extends BaseBean {
	private static Logger logger = Logger.getLogger(CargoTypeBean.class);

	private CargoType cargoType;
	private ICargoTypeService cargoTypeService;

	// flag is page editmode
	private Boolean editMode;

	// search criteria
	private Long id;
	private String code;
	private String description;
	private Integer status;

	public CargoType getCargoType() {
		return cargoType;
	}

	public ICargoTypeService getCargoTypeService() {
		return cargoTypeService;
	}

	public Boolean getEditMode() {
		return editMode;
	}

	public Long getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public Integer getStatus() {
		return status;
	}

	public void setCargoType(CargoType cargoType) {
		this.cargoType = cargoType;
	}

	public void setCargoTypeService(ICargoTypeService cargoTypeService) {
		this.cargoTypeService = cargoTypeService;
	}

	public void setEditMode(Boolean editMode) {
		this.editMode = editMode;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@PostConstruct
	public void init() {
		cargoType = new CargoType();
	}

	public String create() {
		editMode = false;
		return "createCargoType";
	}

	public String save() {

		String returnPage = "searchCargoType";
		String userName = (String) F2MSUtil.getSession().getAttribute(
				IF2MSConstants.USER_NAME);

		try {
			cargoType.setChangedBy(userName);

			if (editMode) {
				cargoTypeService.edit(cargoType);
			} else {
				cargoType.setCreatedBy(userName);
				cargoTypeService.create(cargoType);
			}
		} catch (HibernateOptimisticLockingFailureException fe) {
			fe.printStackTrace();

			FacesContext context = FacesContext.getCurrentInstance();

			FacesMessage errMsg = new FacesMessage(getMessage("errOptLock",
					null));
			context.addMessage("CargoType Exception", errMsg);

			returnPage = "failureSave";
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			returnPage = FWD_GLOBAL_ERROR;
		}

		return returnPage;
	}

	public List<CargoType> getAllCargoTypes() {
		try {
			return cargoTypeService.findAllCargoTypes();
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		return null;
	}

	public String search() {
		return "searchCargoType";
	}

	public List<CargoType> getListResult() {
		try {
			CargoType searchCriteria = new CargoType();
			searchCriteria.setCode(code);
			searchCriteria.setDescription(description);
			searchCriteria.setStatus(status);

			return cargoTypeService.findCargoTypeByCriteria(searchCriteria);
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		return null;
	}

	@SuppressWarnings("rawtypes")
	public String edit() {
		editMode = true;

		FacesContext context = FacesContext.getCurrentInstance();
		Map requestMap = context.getExternalContext().getRequestParameterMap();
		String prmIdNo = (String) requestMap.get("id");

		try {
			cargoType = cargoTypeService.findCargoTypeById(Long
					.parseLong(prmIdNo));
		} catch (NumberFormatException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			return FWD_GLOBAL_ERROR;
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			return FWD_GLOBAL_ERROR;
		}

		return "createCargoType";
	}

	public String getCargoTypeHeader() {
		String header = getMessage("newCargoTypeHeader", null);

		if (editMode) {
			header = getMessage("editCargoTypeHeader", null);
		}

		return header;
	}

	public List<CargoType> suggest(Object value) {
		String input = (String) value;

		CargoType search = new CargoType();
		search.setCode(input);

		List<CargoType> listResult = null;
		try {
			listResult = cargoTypeService.findCargoTypeByCriteria(search);
		} catch (DAOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		return listResult;
	}
}
