package com.f2ms.dao.charges;

import java.util.List;

import com.f2ms.exception.DAOException;
import com.f2ms.model.Charges;

public interface IChargesDAO {
	public Charges create(Charges charges) throws DAOException;
	public Charges edit(Charges charges) throws DAOException;
	public Charges findChargesById(Long id) throws DAOException;
	public List<Charges> findChargesByCriteria(Charges charges) throws DAOException;
}
