package com.f2ms.dao.charges;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

import com.f2ms.dao.AbstractDAOHib;
import com.f2ms.exception.DAOException;
import com.f2ms.model.Charges;

public class ChargesDAOHibImpl extends AbstractDAOHib<Charges> implements IChargesDAO {

	@Override
	public Charges create(Charges charges) throws DAOException {
		charges.setCreatedOn(new Date());
		charges.setChangedOn(new Date());
		
		Long id = (Long) super.create(charges);
		
		Charges created = charges;
		created.setId(id);
		
		return created;
	}

	@Override
	public Charges edit(Charges charges) throws DAOException {
		charges.setChangedOn(new Date());
		
		super.edit(charges);
		
		return charges;
	}

	@Override
	public Charges findChargesById(Long id) throws DAOException {
		return (Charges) super.find(Charges.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Charges> findChargesByCriteria(Charges charges)
			throws DAOException {
		
		DetachedCriteria criteria = DetachedCriteria.forClass(Charges.class);
		
		if(StringUtils.isNotBlank(charges.getChargeType())) {
			criteria.add(Restrictions.eq("chargeType", charges.getChargeType()));
		}
		
		if(StringUtils.isNotBlank(charges.getDescription())) {
			criteria.add(Restrictions.ilike("description", charges.getDescription() + "%"));
		}
		
		if(StringUtils.isNotBlank(charges.getStatus())) {
			criteria.add(Restrictions.eq("status", charges.getStatus()));
		}
		
//		criteria.add(Restrictions.gt("effectiveDate", new Date()));
//		criteria.add(Restrictions.lt("expiryDate", new Date()));
		
		criteria.addOrder(Property.forName("effectiveDate").asc());
		criteria.addOrder(Property.forName("expiryDate").asc());
		
		return getHibernateTemplate().findByCriteria(criteria);
	}
	
}

