package com.f2ms.dao.deliveryorder;

import java.util.List;

import com.f2ms.exception.DAOException;
import com.f2ms.model.DeliveryOrder;

public interface IDeliveryOrderDAO {
	public DeliveryOrder create(DeliveryOrder deliveryOrder) throws DAOException;
	public DeliveryOrder edit(DeliveryOrder deliveryOrder) throws DAOException;
	public DeliveryOrder findById(Long id) throws DAOException;
	public List<DeliveryOrder> findByCriteria(DeliveryOrder deliveryOrder) throws DAOException; 
	public int getMaxSeqNo(String bookingNo) throws DAOException;
}
