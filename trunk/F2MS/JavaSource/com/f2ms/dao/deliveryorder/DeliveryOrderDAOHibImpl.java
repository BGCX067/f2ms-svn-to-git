package com.f2ms.dao.deliveryorder;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.f2ms.dao.AbstractDAOHib;
import com.f2ms.exception.DAOException;
import com.f2ms.model.DeliveryOrder;

public class DeliveryOrderDAOHibImpl extends AbstractDAOHib<DeliveryOrder>
		implements IDeliveryOrderDAO {

	@Override
	public DeliveryOrder create(DeliveryOrder deliveryOrder)
			throws DAOException {
		deliveryOrder.setCreatedOn(new Date());
		deliveryOrder.setChangedOn(new Date());
		
		Long id = (Long) super.create(deliveryOrder);
		
		DeliveryOrder created = deliveryOrder;
		created.setId(id);
		
		return created;
	}

	@Override
	public DeliveryOrder edit(DeliveryOrder deliveryOrder) throws DAOException {
		deliveryOrder.setChangedOn(new Date());
		
		super.edit(deliveryOrder);
		
		return deliveryOrder;
	}

	@Override
	public DeliveryOrder findById(Long id)
			throws DAOException {
		return (DeliveryOrder) super.find(DeliveryOrder.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DeliveryOrder> findByCriteria(DeliveryOrder deliveryOrder)
			throws DAOException {
		DetachedCriteria criteria = DetachedCriteria.forClass(DeliveryOrder.class);
		
		if(StringUtils.isNotBlank(deliveryOrder.getBookingNo())) {
			criteria.add(Restrictions.ilike("bookingNo", deliveryOrder.getBookingNo() + "%"));
		}
		
		if(StringUtils.isNotBlank(deliveryOrder.getDocNo())) {
			criteria.add(Restrictions.ilike("docNo", deliveryOrder.getDocNo()));
		}
		
		return getHibernateTemplate().findByCriteria(criteria); 
	}

	@Override
	public int getMaxSeqNo(String bookingNo) throws DAOException {
		int seqNo = 1;
		
		String hql = "from DeliveryOrder do where do.bookingNo = ?";
		
		@SuppressWarnings("unchecked")
		List<DeliveryOrder> listTemp = getHibernateTemplate().find(hql, bookingNo);
		
		if(listTemp != null && !listTemp.isEmpty()) {
			seqNo = listTemp.size() + 1;
		}
		
		return seqNo;
	}

}
