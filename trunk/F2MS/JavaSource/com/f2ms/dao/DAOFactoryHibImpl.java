package com.f2ms.dao;

import com.f2ms.dao.booking.IBookingDAO;
import com.f2ms.dao.booking.IBookingFilesDAO;
import com.f2ms.dao.cargotype.ICargoTypeDAO;
import com.f2ms.dao.charges.IChargesDAO;
import com.f2ms.dao.common.IRoleDAO;
import com.f2ms.dao.common.IStaffDAO;
import com.f2ms.dao.common.IUserDAO;
import com.f2ms.dao.common.IUserRoleDAO;
import com.f2ms.dao.customer.ICustomerDAO;
import com.f2ms.dao.deliveryorder.IDeliveryOrderDAO;
import com.f2ms.dao.location.ILocationDAO;
import com.f2ms.dao.pickuporder.IPickupOrderDAO;
import com.f2ms.dao.receiveorder.IReceiveOrderDAO;
import com.f2ms.dao.shipment.IShipmentDAO;
import com.f2ms.dao.shipmentitem.IShipmentItemDAO;
import com.f2ms.dao.shipmentmode.IShipmentModeDAO;
import com.f2ms.dao.warehouse.IWarehouseDAO;

public class DAOFactoryHibImpl extends DAOFactory {
	private IBookingDAO bookingDAO;
	private ICargoTypeDAO cargoTypeDAO;
	private IChargesDAO chargesDAO;
	private IRoleDAO roleDAO;
	private IStaffDAO staffDAO;
	private IUserDAO userDAO;
	private IUserRoleDAO userRoleDAO;
	private ICustomerDAO customerDAO;
	private IDeliveryOrderDAO deliveryOrderDAO;
	private ILocationDAO locationDAO;
	private IPickupOrderDAO pickupOrderDAO;
	private IReceiveOrderDAO receiveOrderDAO;
	private IShipmentDAO shipmentDAO;
	private IShipmentItemDAO shipmentItemDAO;
	private IShipmentModeDAO shipmentModeDAO;
	private IWarehouseDAO warehouseDAO;
	private IBookingFilesDAO bookingFilesDAO;

	public int initialize(IChargesDAO charges, IBookingDAO booking,
			ICargoTypeDAO cargo, IRoleDAO role, IStaffDAO staff, IUserDAO user,
			IUserRoleDAO userRole, ICustomerDAO customer,
			IDeliveryOrderDAO deliveryOrder, ILocationDAO location,
			IPickupOrderDAO pickupOrder, IReceiveOrderDAO receiveOrder,
			IShipmentDAO shipment, IShipmentItemDAO shipmentItem,
			IShipmentModeDAO shipmentMode, IWarehouseDAO warehouse,
			IBookingFilesDAO bookingFiles) {
		this.chargesDAO = charges;
		this.bookingDAO = booking;
		this.cargoTypeDAO = cargo;
		this.roleDAO = role;
		this.staffDAO = staff;
		this.userDAO = user;
		this.userRoleDAO = userRole;
		this.customerDAO = customer;
		this.deliveryOrderDAO = deliveryOrder;
		this.locationDAO = location;
		this.pickupOrderDAO = pickupOrder;
		this.receiveOrderDAO = receiveOrder;
		this.shipmentDAO = shipment;
		this.shipmentItemDAO = shipmentItem;
		this.shipmentModeDAO = shipmentMode;
		this.warehouseDAO = warehouse;
		this.bookingFilesDAO = bookingFiles;

		return HIBERNATE_IMPL;
	}

	@Override
	public IBookingDAO getBookingDAO() {
		return bookingDAO;
	}

	@Override
	public ICargoTypeDAO getCargoTypeDAO() {
		return cargoTypeDAO;
	}

	@Override
	public IChargesDAO getChargesDAO() {
		return chargesDAO;
	}

	@Override
	public IRoleDAO getRoleDAO() {
		return roleDAO;
	}

	@Override
	public IStaffDAO getStaffDAO() {
		return staffDAO;
	}

	@Override
	public IUserDAO getUserDAO() {
		return userDAO;
	}

	@Override
	public IUserRoleDAO getUserRoleDAO() {
		return userRoleDAO;
	}

	@Override
	public ICustomerDAO getCustomerDAO() {
		return customerDAO;
	}

	@Override
	public IDeliveryOrderDAO getDeliveryOrderDAO() {
		return deliveryOrderDAO;
	}

	@Override
	public ILocationDAO getLocationDAO() {
		return locationDAO;
	}

	@Override
	public IPickupOrderDAO getPickupOrderDAO() {
		return pickupOrderDAO;
	}

	@Override
	public IReceiveOrderDAO getReceiveOrderDAO() {
		return receiveOrderDAO;
	}

	@Override
	public IShipmentDAO getShipmentDAO() {
		return shipmentDAO;
	}

	@Override
	public IShipmentItemDAO getShipmentItemDAO() {
		return shipmentItemDAO;
	}

	@Override
	public IShipmentModeDAO getShipmentModeDAO() {
		return shipmentModeDAO;
	}

	@Override
	public IWarehouseDAO getWarehouseDAO() {
		return warehouseDAO;
	}

	@Override
	public IBookingFilesDAO getBookingFilesDAO() {
		return bookingFilesDAO;
	}

}
