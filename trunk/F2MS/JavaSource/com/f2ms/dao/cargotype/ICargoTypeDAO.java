package com.f2ms.dao.cargotype;

import java.util.List;

import com.f2ms.exception.DAOException;
import com.f2ms.model.CargoType;

public interface ICargoTypeDAO {

	public List<CargoType> findAllCargoTypes() throws DAOException;

	public CargoType create(CargoType cargoType) throws DAOException;

	public CargoType edit(CargoType cargoType) throws DAOException;

	public CargoType findCargoTypeById(long cargoTypeId) throws DAOException;

	public List<CargoType> findCargoTypeByCriteria(CargoType cargoType)
			throws DAOException;
}
