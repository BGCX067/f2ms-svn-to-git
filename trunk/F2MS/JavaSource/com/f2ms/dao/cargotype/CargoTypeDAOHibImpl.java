package com.f2ms.dao.cargotype;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

import com.f2ms.dao.AbstractDAOHib;
import com.f2ms.exception.DAOException;
import com.f2ms.model.CargoType;

/**
 * CargoTypeDAOHibImpl.java
 * 
 * @author Isak Rabin
 * 
 */
public class CargoTypeDAOHibImpl extends AbstractDAOHib<CargoType> implements
		ICargoTypeDAO {

	@Override
	public List<CargoType> findAllCargoTypes() throws DAOException {
		return super.findAll(CargoType.class);
	}

	@Override
	public CargoType create(CargoType cargoType) throws DAOException {
		cargoType.setCreatedOn(new Date());
		cargoType.setChangedOn(new Date());

		Long id = (Long) super.create(cargoType);

		CargoType entity = cargoType;
		entity.setId(id);

		return entity;
	}

	@Override
	public CargoType edit(CargoType cargoType) throws DAOException {
		cargoType.setChangedOn(new Date());

		super.edit(cargoType);

		return cargoType;
	}

	@Override
	public CargoType findCargoTypeById(long cargoTypeId) throws DAOException {
		return (CargoType) super.find(CargoType.class, cargoTypeId);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CargoType> findCargoTypeByCriteria(CargoType cargoType)
			throws DAOException {
		DetachedCriteria criteria = DetachedCriteria.forClass(CargoType.class);

		if (StringUtils.isNotBlank(cargoType.getCode())) {
			criteria.add(Restrictions.eq("code", cargoType.getCode() + "%"));
		}

		if (StringUtils.isNotBlank(cargoType.getDescription())) {
			criteria.add(Restrictions.ilike("description",
					cargoType.getDescription() + "%"));
		}

		if (cargoType.getStatus() != null
				&& cargoType.getStatus().intValue() != 0) {
			criteria.add(Restrictions.eq("status", cargoType.getStatus()));
		}

		criteria.addOrder(Property.forName("code").asc());
		List<CargoType> listResult = getHibernateTemplate().findByCriteria(
				criteria);

		return listResult;
	}
}
