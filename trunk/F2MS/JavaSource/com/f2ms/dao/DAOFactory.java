package com.f2ms.dao;

import com.f2ms.dao.booking.IBookingDAO;
import com.f2ms.dao.booking.IBookingFilesDAO;
import com.f2ms.dao.cargotype.ICargoTypeDAO;
import com.f2ms.dao.charges.IChargesDAO;
import com.f2ms.dao.common.IRoleDAO;
import com.f2ms.dao.common.IStaffDAO;
import com.f2ms.dao.common.IUserDAO;
import com.f2ms.dao.common.IUserRoleDAO;
import com.f2ms.dao.customer.ICustomerDAO;
import com.f2ms.dao.deliveryorder.IDeliveryOrderDAO;
import com.f2ms.dao.location.ILocationDAO;
import com.f2ms.dao.pickuporder.IPickupOrderDAO;
import com.f2ms.dao.receiveorder.IReceiveOrderDAO;
import com.f2ms.dao.shipment.IShipmentDAO;
import com.f2ms.dao.shipmentitem.IShipmentItemDAO;
import com.f2ms.dao.shipmentmode.IShipmentModeDAO;
import com.f2ms.dao.warehouse.IWarehouseDAO;

public abstract class DAOFactory {
	private static DAOFactory instance;

	public static final int HIBERNATE_IMPL = 1;

	public static DAOFactory getInstance() {
		instance = loadInstance(HIBERNATE_IMPL);

		return instance;
	}

	public static DAOFactory getInstance(int i) {
		instance = loadInstance(i);

		return instance;
	}

	private static DAOFactory loadInstance(int acc) {
		switch (acc) {
		case (1):
			return new com.f2ms.dao.DAOFactoryHibImpl();
		default:
			return new com.f2ms.dao.DAOFactoryHibImpl();
		}
	}

	public abstract IBookingDAO getBookingDAO();

	public abstract ICargoTypeDAO getCargoTypeDAO();

	public abstract IChargesDAO getChargesDAO();

	public abstract IRoleDAO getRoleDAO();

	public abstract IStaffDAO getStaffDAO();

	public abstract IUserDAO getUserDAO();

	public abstract IUserRoleDAO getUserRoleDAO();

	public abstract ICustomerDAO getCustomerDAO();

	public abstract IDeliveryOrderDAO getDeliveryOrderDAO();

	public abstract ILocationDAO getLocationDAO();

	public abstract IPickupOrderDAO getPickupOrderDAO();

	public abstract IReceiveOrderDAO getReceiveOrderDAO();

	public abstract IShipmentDAO getShipmentDAO();

	public abstract IShipmentItemDAO getShipmentItemDAO();

	public abstract IShipmentModeDAO getShipmentModeDAO();

	public abstract IWarehouseDAO getWarehouseDAO();

	public abstract IBookingFilesDAO getBookingFilesDAO();
}
