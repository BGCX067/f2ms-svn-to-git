package com.f2ms.dao.shipment;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

import com.f2ms.dao.AbstractDAOHib;
import com.f2ms.exception.DAOException;
import com.f2ms.model.Shipment;

/**
 * ShipmentDAOHibImpl.java
 * 
 * @author Isak Rabin
 * 
 */
public class ShipmentDAOHibImpl extends AbstractDAOHib<Shipment> implements
		IShipmentDAO {

	@Override
	public List<Shipment> findAllPackagings() throws DAOException {
		return super.findAll(Shipment.class);
	}

	@Override
	public Shipment create(Shipment shipment) throws DAOException {
		shipment.setCreatedOn(new Date());
		shipment.setChangedOn(new Date());

		String docNo = generateDocNo();
		shipment.setPackagingNo(docNo);

		super.create(shipment);

		return shipment;
	}

	@Override
	public Shipment edit(Shipment shipment) throws DAOException {
		shipment.setChangedOn(new Date());
		super.edit(shipment);
		return shipment;
	}

	@Override
	public Shipment findPackagingById(String shipmentNo) throws DAOException {
		return (Shipment) super.find(Shipment.class, shipmentNo);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Shipment> findPackagingByCriteria(Shipment shipment)
			throws DAOException {
		DetachedCriteria criteria = DetachedCriteria.forClass(Shipment.class);

		if (StringUtils.isNotBlank(shipment.getPackagingNo())) {
			criteria.add(Restrictions.eq("packagingNo",
					shipment.getPackagingNo() + "%"));
		}

		if (shipment.getStatus() != null
				&& shipment.getStatus().intValue() != 0) {
			criteria.add(Restrictions.eq("status", shipment.getStatus()));
		}

		criteria.addOrder(Property.forName("packagingNo").asc());
		List<Shipment> listResult = getHibernateTemplate().findByCriteria(
				criteria);

		return listResult;
	}

	private String generateDocNo() {
		return "DOC-" + Calendar.getInstance().getTimeInMillis();
	}
}
