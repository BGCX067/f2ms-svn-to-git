package com.f2ms.dao.shipment;

import java.util.List;

import com.f2ms.exception.DAOException;
import com.f2ms.model.Shipment;

public interface IShipmentDAO {

	public List<Shipment> findAllPackagings() throws DAOException;

	public Shipment create(Shipment shipment) throws DAOException;

	public Shipment edit(Shipment shipment) throws DAOException;

	public Shipment findPackagingById(String shipmentNo) throws DAOException;

	public List<Shipment> findPackagingByCriteria(Shipment shipment)
			throws DAOException;
}
