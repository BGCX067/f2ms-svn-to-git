package com.f2ms.dao.receiveorder;

import java.util.List;

import com.f2ms.exception.DAOException;
import com.f2ms.model.ReceiveOrder;

public interface IReceiveOrderDAO {

    public List<ReceiveOrder> findAllReceiveOrders() throws DAOException;

    public ReceiveOrder create(ReceiveOrder receiveOrder) throws DAOException;

    public ReceiveOrder edit(ReceiveOrder receiveOrder) throws DAOException;

    public ReceiveOrder findReceiveOrderById(long rowId) throws DAOException;

    public List<ReceiveOrder> findReceiveOrderByCriteria(
            ReceiveOrder receiveOrder) throws DAOException;
}
