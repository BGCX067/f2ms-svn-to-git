package com.f2ms.dao.receiveorder;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

import com.f2ms.dao.AbstractDAOHib;
import com.f2ms.exception.DAOException;
import com.f2ms.model.ReceiveOrder;

/**
 * ReceiveOrderDAOHibImpl.java
 * 
 * @author Isak Rabin
 * 
 */
public class ReceiveOrderDAOHibImpl extends AbstractDAOHib<ReceiveOrder>
		implements IReceiveOrderDAO {

	@Override
	public List<ReceiveOrder> findAllReceiveOrders() throws DAOException {
		return super.findAll(ReceiveOrder.class);
	}

	@Override
	public ReceiveOrder create(ReceiveOrder receiveOrder) throws DAOException {
		receiveOrder.setCreatedOn(new Date());
		receiveOrder.setChangedOn(new Date());

		String docNo = generateDocNo();
		receiveOrder.setDocNo(docNo);
		Long id = (Long) super.create(receiveOrder);

		ReceiveOrder entity = receiveOrder;
		entity.setId(id);

		return entity;
	}

	@Override
	public ReceiveOrder edit(ReceiveOrder receiveOrder) throws DAOException {
		receiveOrder.setChangedOn(new Date());

		super.edit(receiveOrder);

		return receiveOrder;
	}

	@Override
	public ReceiveOrder findReceiveOrderById(long receiveOrderId)
			throws DAOException {
		return (ReceiveOrder) super.find(ReceiveOrder.class, receiveOrderId);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ReceiveOrder> findReceiveOrderByCriteria(
			ReceiveOrder receiveOrder) throws DAOException {
		DetachedCriteria criteria = DetachedCriteria
				.forClass(ReceiveOrder.class);

		if (StringUtils.isNotBlank(receiveOrder.getDocNo())) {
			criteria.add(Restrictions.eq("docNo", receiveOrder.getDocNo() + "%"));
		}

		if (receiveOrder.getDocDate() != null) {
			criteria.add(Restrictions.eq("docDate", receiveOrder.getDocDate()));
		}

		if (StringUtils.isNotBlank(receiveOrder.getBooking().getDocNo())) {
			criteria.add(Restrictions.eq("bookingNo", receiveOrder.getBooking()
					.getDocNo() + "%"));
		}

		if (receiveOrder.getStatus() != null
				&& receiveOrder.getStatus().intValue() != 0) {
			criteria.add(Restrictions.eq("status", receiveOrder.getStatus()));
		}

		criteria.addOrder(Property.forName("docNo").asc());
		List<ReceiveOrder> listResult = getHibernateTemplate().findByCriteria(
				criteria);

		return listResult;
	}

	private String generateDocNo() {
		return "DOC-" + Calendar.getInstance().getTimeInMillis();
	}
}
