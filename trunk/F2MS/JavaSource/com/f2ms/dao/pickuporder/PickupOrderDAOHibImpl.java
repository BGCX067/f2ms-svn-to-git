package com.f2ms.dao.pickuporder;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

import com.f2ms.dao.AbstractDAOHib;
import com.f2ms.exception.DAOException;
import com.f2ms.model.PickupOrder;

public class PickupOrderDAOHibImpl extends AbstractDAOHib<PickupOrder>
		implements IPickupOrderDAO {

	@Override
	public List<PickupOrder> findAllPickupOrders() throws DAOException {
		return super.findAll(PickupOrder.class);
	}

	@Override
	public PickupOrder create(PickupOrder pickupOrder) throws DAOException {
		pickupOrder.setCreatedOn(new Date());
		pickupOrder.setChangedOn(new Date());

		String docNo = generateDocNo();
		pickupOrder.setDocNo(docNo);
		Long id = (Long) super.create(pickupOrder);

		PickupOrder entity = pickupOrder;
		entity.setId(id);

		return entity;
	}

	@Override
	public PickupOrder edit(PickupOrder pickupOrder) throws DAOException {
		pickupOrder.setChangedOn(new Date());

		super.edit(pickupOrder);

		return pickupOrder;
	}

	@Override
	public PickupOrder findPickupOrderById(long pickupOrderId)
			throws DAOException {
		return (PickupOrder) super.find(PickupOrder.class, pickupOrderId);
	}

	@Override
	public List<PickupOrder> findPickupOrderByCriteria(PickupOrder pickupOrder)
			throws DAOException {
		DetachedCriteria criteria = DetachedCriteria
				.forClass(PickupOrder.class);

		if (StringUtils.isNotBlank(pickupOrder.getDocNo())) {
			criteria.add(Restrictions.eq("docNo", pickupOrder.getDocNo() + "%"));
		}

		if (pickupOrder.getPickupDate() != null) {
			criteria.add(Restrictions.eq("pickupDate",
					pickupOrder.getPickupDate()));
		}

		if (StringUtils.isNotBlank(pickupOrder.getBookingNo())) {
			criteria.add(Restrictions.eq("bookingNo",
					pickupOrder.getBookingNo() + "%"));
		}

		if (pickupOrder.getStatus() != null
				&& pickupOrder.getStatus().intValue() != 0) {
			criteria.add(Restrictions.eq("status", pickupOrder.getStatus()));
		}

		if (pickupOrder.getMode() != null
				&& pickupOrder.getMode().intValue() != 0) {
			criteria.add(Restrictions.eq("mode", pickupOrder.getMode()));
		}

		criteria.addOrder(Property.forName("docNo").asc());
		@SuppressWarnings("unchecked")
		List<PickupOrder> listResult = getHibernateTemplate().findByCriteria(
				criteria);

		return listResult;
	}

	private String generateDocNo() {
		return "DOC-" + Calendar.getInstance().getTimeInMillis();
	}
}
