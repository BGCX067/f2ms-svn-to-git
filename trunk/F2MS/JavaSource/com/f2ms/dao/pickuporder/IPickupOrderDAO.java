package com.f2ms.dao.pickuporder;

import java.util.List;

import com.f2ms.exception.DAOException;
import com.f2ms.model.PickupOrder;

public interface IPickupOrderDAO {

    public List<PickupOrder> findAllPickupOrders() throws DAOException;

    public PickupOrder create(PickupOrder pickupOrder) throws DAOException;

    public PickupOrder edit(PickupOrder pickupOrder) throws DAOException;

    public PickupOrder findPickupOrderById(long rowId) throws DAOException;

    public List<PickupOrder> findPickupOrderByCriteria(PickupOrder pickupOrder)
            throws DAOException;
}
