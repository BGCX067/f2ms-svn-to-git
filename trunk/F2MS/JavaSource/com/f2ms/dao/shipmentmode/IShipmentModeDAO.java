package com.f2ms.dao.shipmentmode;

import java.util.List;

import com.f2ms.exception.DAOException;
import com.f2ms.model.ShipmentMode;

public interface IShipmentModeDAO {

    public List<ShipmentMode> findAllShipmentModes() throws DAOException;

    public ShipmentMode create(ShipmentMode shipmentMode) throws DAOException;

    public ShipmentMode edit(ShipmentMode shipmentMode) throws DAOException;

    public ShipmentMode findShipmentModeById(long rowId) throws DAOException;

    public List<ShipmentMode> findShipmentModeByCriteria(
            ShipmentMode shipmentMode) throws DAOException;
}
