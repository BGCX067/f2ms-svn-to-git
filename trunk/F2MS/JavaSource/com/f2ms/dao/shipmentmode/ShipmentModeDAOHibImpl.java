package com.f2ms.dao.shipmentmode;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

import com.f2ms.dao.AbstractDAOHib;
import com.f2ms.exception.DAOException;
import com.f2ms.model.ShipmentMode;

/**
 * ShipmentModeDAOHibImpl.java
 * 
 * @author Isak Rabin
 * 
 */
public class ShipmentModeDAOHibImpl extends AbstractDAOHib<ShipmentMode>
		implements IShipmentModeDAO {

	@Override
	public List<ShipmentMode> findAllShipmentModes() throws DAOException {
		return super.findAll(ShipmentMode.class);
	}

	@Override
	public ShipmentMode create(ShipmentMode shipmentMode) throws DAOException {
		shipmentMode.setCreatedOn(new Date());
		shipmentMode.setChangedOn(new Date());

		Long id = (Long) super.create(shipmentMode);

		ShipmentMode entity = shipmentMode;
		entity.setId(id);

		return entity;
	}

	@Override
	public ShipmentMode edit(ShipmentMode shipmentMode) throws DAOException {
		shipmentMode.setChangedOn(new Date());

		super.edit(shipmentMode);

		return shipmentMode;
	}

	@Override
	public ShipmentMode findShipmentModeById(long shipmentModeId)
			throws DAOException {
		return (ShipmentMode) super.find(ShipmentMode.class, shipmentModeId);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ShipmentMode> findShipmentModeByCriteria(
			ShipmentMode shipmentMode) throws DAOException {
		DetachedCriteria criteria = DetachedCriteria
				.forClass(ShipmentMode.class);

		if (StringUtils.isNotBlank(shipmentMode.getCode())) {
			criteria.add(Restrictions.eq("code", shipmentMode.getCode() + "%"));
		}

		if (StringUtils.isNotBlank(shipmentMode.getDescription())) {
			criteria.add(Restrictions.ilike("description",
					shipmentMode.getDescription() + "%"));
		}

		if (shipmentMode.getStatus() != null
				&& shipmentMode.getStatus().intValue() != 0) {
			criteria.add(Restrictions.eq("status", shipmentMode.getStatus()));
		}

		criteria.addOrder(Property.forName("code").asc());
		List<ShipmentMode> listResult = getHibernateTemplate().findByCriteria(
				criteria);

		return listResult;
	}
}
