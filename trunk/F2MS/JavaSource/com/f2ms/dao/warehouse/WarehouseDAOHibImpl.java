package com.f2ms.dao.warehouse;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

import com.f2ms.dao.AbstractDAOHib;
import com.f2ms.exception.DAOException;
import com.f2ms.model.Warehouse;

/**
 * WarehouseDAOHibImpl
 * 
 * @author Isak Rabin
 * 
 */
public class WarehouseDAOHibImpl extends AbstractDAOHib<Warehouse> implements
		IWarehouseDAO {

	@Override
	public List<Warehouse> findAllWarehouses() throws DAOException {
		return super.findAll(Warehouse.class);
	}

	@Override
	public Warehouse create(Warehouse warehouse) throws DAOException {
		warehouse.setCreatedOn(new Date());
		warehouse.setChangedOn(new Date());

		Long id = (Long) super.create(warehouse);

		Warehouse entity = warehouse;
		entity.setId(id);

		return entity;
	}

	@Override
	public Warehouse edit(Warehouse warehouse) throws DAOException {
		warehouse.setChangedOn(new Date());

		super.edit(warehouse);

		return warehouse;
	}

	@Override
	public Warehouse findWarehouseById(long warehouseId) throws DAOException {
		return (Warehouse) super.find(Warehouse.class, warehouseId);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Warehouse> findWarehouseByCriteria(Warehouse warehouse)
			throws DAOException {
		DetachedCriteria criteria = DetachedCriteria.forClass(Warehouse.class);

		if (StringUtils.isNotBlank(warehouse.getCode())) {
			criteria.add(Restrictions.eq("code", warehouse.getCode()));
		}

		if (StringUtils.isNotBlank(warehouse.getDescription())) {
			criteria.add(Restrictions.ilike("description",
					warehouse.getDescription() + "%"));
		}

		if (StringUtils.isNotBlank(warehouse.getAddress())) {
			criteria.add(Restrictions.ilike("address", warehouse.getAddress()
					+ "%"));
		}

		if (warehouse.getStatus() != null
				&& warehouse.getStatus().intValue() != 0) {
			criteria.add(Restrictions.eq("status", warehouse.getStatus()));
		}

		criteria.addOrder(Property.forName("code").asc());
		List<Warehouse> listResult = getHibernateTemplate().findByCriteria(
				criteria);

		return listResult;
	}
}
