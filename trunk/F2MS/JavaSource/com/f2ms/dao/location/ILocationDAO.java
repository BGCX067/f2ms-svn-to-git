package com.f2ms.dao.location;

import java.util.List;

import com.f2ms.exception.DAOException;
import com.f2ms.model.Location;

public interface ILocationDAO {

    public List<Location> findAllLocations() throws DAOException;

    public Location create(Location location) throws DAOException;

    public Location edit(Location location) throws DAOException;

    public Location findLocationById(long rowId) throws DAOException;

    public List<Location> findLocationByCriteria(Location location)
            throws DAOException;
}
