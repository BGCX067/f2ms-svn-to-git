package com.f2ms.dao.location;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

import com.f2ms.dao.AbstractDAOHib;
import com.f2ms.exception.DAOException;
import com.f2ms.model.Location;

public class LocationDAOHibImpl extends AbstractDAOHib<Location> implements
		ILocationDAO {

	@Override
	public List<Location> findAllLocations() throws DAOException {
		return super.findAll(Location.class);
	}

	@Override
	public Location create(Location location) throws DAOException {
		location.setCreatedOn(new Date());
		location.setChangedOn(new Date());

		Long id = (Long) super.create(location);

		Location entity = location;
		entity.setId(id);

		return entity;
	}

	@Override
	public Location edit(Location location) throws DAOException {
		location.setChangedOn(new Date());

		super.edit(location);

		return location;
	}

	@Override
	public Location findLocationById(long locationId) throws DAOException {
		return (Location) super.find(Location.class, locationId);
	}

	@Override
	public List<Location> findLocationByCriteria(Location location)
			throws DAOException {
		DetachedCriteria criteria = DetachedCriteria.forClass(Location.class);

		if (StringUtils.isNotBlank(location.getCode())) {
			criteria.add(Restrictions.eq("code", location.getCode()));
		}

		if (StringUtils.isNotBlank(location.getDescription())) {
			criteria.add(Restrictions.ilike("description",
					location.getDescription() + "%"));
		}

		if (location.getStatus() != null
				&& location.getStatus().intValue() != 0) {
			criteria.add(Restrictions.eq("status", location.getStatus()));
		}

		criteria.addOrder(Property.forName("code").asc());
		@SuppressWarnings("unchecked")
		List<Location> listResult = getHibernateTemplate().findByCriteria(
				criteria);

		return listResult;
	}
}
