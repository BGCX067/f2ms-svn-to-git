package com.f2ms.dao.booking;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

import com.f2ms.dao.AbstractDAOHib;
import com.f2ms.enumeration.BookingStatus;
import com.f2ms.exception.DAOException;
import com.f2ms.model.Booking;

public class BookingDAOHibImpl extends AbstractDAOHib<Booking> implements
		IBookingDAO {

	@Override
	public Booking create(Booking booking) throws DAOException {
		booking.setStatus(String.valueOf(BookingStatus.NEW.getValue()));
		booking.setCreatedOn(new Date());
		booking.setChangedOn(new Date());

		String docNo = generateDocNo();
		booking.setDocNo(docNo);

		super.create(booking);

		return booking;
	}

	private String generateDocNo() {
		return "DOC-" + Calendar.getInstance().getTimeInMillis();
	}

	@Override
	public Booking edit(Booking booking) throws DAOException {
		booking.setChangedOn(new Date());
		super.edit(booking);

		return booking;
	}

	@Override
	public List<Booking> findAllBooking() throws DAOException {
		return super.findAll(Booking.class);
	}

	@Override
	public Booking findBookingById(String bookingNo) throws DAOException {
		return (Booking) super.find(Booking.class, bookingNo);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Booking> findBookingByCriteria(Booking booking)
			throws DAOException {
		DetachedCriteria criteria = DetachedCriteria.forClass(Booking.class);

		if (StringUtils.isNotBlank(booking.getDocNo())) {
			criteria.add(Restrictions.ilike("docNo", booking.getDocNo() + "%"));
		}

		if (StringUtils.isNotBlank(booking.getStatus())) {
			criteria.add(Restrictions.eq("status", booking.getStatus()));
		}

		if (StringUtils.isNotBlank(booking.getShipmentType())) {
			criteria.add(Restrictions.eq("shipmentType",
					booking.getShipmentType()));
		}

		if (StringUtils.isNotBlank(booking.getTranshipmentVia())) {
			criteria.add(Restrictions.eq("transhipmentVia",
					booking.getTranshipmentVia()));
		}

		if (StringUtils.isNotBlank(booking.getHsCode())) {
			criteria.add(Restrictions.ilike("hsCode", booking.getHsCode() + "%"));
		}

		if (StringUtils.isNotBlank(booking.getPermitNo())) {
			criteria.add(Restrictions.ilike("permitNo", booking.getPermitNo()));
		}

		if (booking.getNeedPickup() != null
				&& booking.getNeedPickup().intValue() == 1) {
			criteria.add(Restrictions.eq("needPickup", booking.getNeedPickup()
					.intValue()));
		}

		criteria.addOrder(Property.forName("docNo").asc());
		List<Booking> listResult = getHibernateTemplate().findByCriteria(
				criteria);

		return listResult;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Booking> findAvailableBooking(Date startPeriod, Date endPeriod,
			Long pol, Long pod, Long customerCode) throws DAOException {
		DetachedCriteria criteria = DetachedCriteria.forClass(Booking.class);

		if (startPeriod != null && endPeriod != null) {
			criteria.add(Restrictions.between("createdOn", startPeriod,
					endPeriod));
		}

		if (pol != null && pol.intValue() != 0) {
			criteria.add(Restrictions.eq("finalDestinationId", pol.longValue()));
		}

		if (customerCode != null && customerCode.intValue() != 0) {
			criteria.add(Restrictions.eq("consigneeId",
					customerCode.longValue()));
		}

		criteria.add(Restrictions.eq("status",
				String.valueOf(BookingStatus.RECEIVED.getValue())));

		criteria.addOrder(Property.forName("createdOn").asc());
		criteria.addOrder(Property.forName("docNo").asc());

		List<Booking> listResult = getHibernateTemplate().findByCriteria(
				criteria);

		return listResult;
	}

}
