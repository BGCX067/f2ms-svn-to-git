package com.f2ms.dao.booking;

import java.util.Date;
import java.util.List;

import com.f2ms.exception.DAOException;
import com.f2ms.model.Booking;

public interface IBookingDAO {

	public Booking create(Booking booking) throws DAOException;

	public Booking edit(Booking booking) throws DAOException;

	public List<Booking> findAllBooking() throws DAOException;

	public Booking findBookingById(String bookingNo) throws DAOException;

	public List<Booking> findBookingByCriteria(Booking booking)
			throws DAOException;

	public List<Booking> findAvailableBooking(Date startPeriod, Date endPeriod,
			Long pol, Long pod, Long customerCode) throws DAOException;	
	
}
