package com.f2ms.dao.booking;

import java.util.List;

import com.f2ms.exception.DAOException;
import com.f2ms.model.BookingFiles;

public interface IBookingFilesDAO {
	public BookingFiles createBookingFile(BookingFiles bookingFiles) throws DAOException;
	
	public List<BookingFiles> findFilesByBookingNo(String bookingNO) throws DAOException;
	
	public void deleteBookingFile(BookingFiles bookingFile) throws DAOException;
	
	public BookingFiles findById(Long id) throws DAOException;
}
