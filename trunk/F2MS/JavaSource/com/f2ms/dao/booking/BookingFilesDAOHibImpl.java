package com.f2ms.dao.booking;

import java.util.List;

import com.f2ms.dao.AbstractDAOHib;
import com.f2ms.exception.DAOException;
import com.f2ms.model.BookingFiles;

public class BookingFilesDAOHibImpl extends AbstractDAOHib<BookingFiles> implements
		IBookingFilesDAO {

	@Override
	public BookingFiles createBookingFile(BookingFiles bookingFiles)
			throws DAOException {
		
		Long id = (Long) super.create(bookingFiles);
		
		BookingFiles created = bookingFiles;
		created.setId(id);
		
		return created;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BookingFiles> findFilesByBookingNo(String bookingNO)
			throws DAOException {
		
		String hql = "from BookingFiles files where files.bookingNo = ?";
		
		return getHibernateTemplate().find(hql, bookingNO);
	}

	@Override
	public void deleteBookingFile(BookingFiles bookingFile) throws DAOException {
		super.delete(bookingFile);
	}

	@Override
	public BookingFiles findById(Long id) throws DAOException {		
		return (BookingFiles) super.find(BookingFiles.class, id);
	}	
}
