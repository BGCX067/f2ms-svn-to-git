package com.f2ms.dao.customer;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

import com.f2ms.dao.AbstractDAOHib;
import com.f2ms.exception.DAOException;
import com.f2ms.model.Customer;

public class CustomerDAOHibImpl extends AbstractDAOHib<Customer> implements
        ICustomerDAO {

    @Override
    public List<Customer> findAllCustomers() throws DAOException {
        return super.findAll(Customer.class);
    }

    @Override
    public Customer create(Customer customer) throws DAOException {
        customer.setCreatedOn(new Date());
        customer.setChangedOn(new Date());

        Long custCode = (Long) super.create(customer);

        Customer created = customer;
        created.setCustCode(custCode);

        return created;
    }

    @Override
    public Customer edit(Customer customer) throws DAOException {
        customer.setChangedOn(new Date());

        super.edit(customer);

        return customer;
    }

    @Override
    public Customer findCustomerById(long custCode) throws DAOException {
        return (Customer) super.find(Customer.class, custCode);
    }

    @Override
    public List<Customer> findCustomerByCriteria(Customer customer)
            throws DAOException {

        DetachedCriteria criteria = DetachedCriteria.forClass(Customer.class);

        if (StringUtils.isNotBlank(customer.getIdType())) {
            criteria.add(Restrictions.eq("idType", customer.getIdType()));
        }

        if (StringUtils.isNotBlank(customer.getIdNo())) {
            criteria.add(Restrictions.eq("idNo", customer.getIdNo()));
        }

        if (StringUtils.isNotBlank(customer.getName())) {
            criteria.add(Restrictions.ilike("name", customer.getName() + "%"));
        }

        if (StringUtils.isNotBlank(customer.getAddressLine1())) {
            criteria.add(Restrictions.ilike("addressLine1", customer.getFullAddress() + "%"));
        }

        criteria.addOrder(Property.forName("name").asc());
        @SuppressWarnings("unchecked")
        List<Customer> listResult = getHibernateTemplate().findByCriteria(criteria);

        return listResult;
    }
}
