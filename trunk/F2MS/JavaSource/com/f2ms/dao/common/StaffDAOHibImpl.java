package com.f2ms.dao.common;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.f2ms.dao.AbstractDAOHib;
import com.f2ms.exception.DAOException;
import com.f2ms.model.Staff;

public class StaffDAOHibImpl extends AbstractDAOHib<Staff> implements IStaffDAO {

    @Override
    public Staff create(Staff staff) throws DAOException {
        Long staffCode = (Long) super.create(staff);

        Staff created = staff;
        created.setStaffCode(staffCode);

        return created;
    }

    @Override
    public Staff edit(Staff staff) throws DAOException {
        super.edit(staff);

        return staff;
    }

    @Override
    public List<Staff> findAllStaff() throws DAOException {
        return super.findAll(Staff.class);
    }

    @Override
    public Staff findStaffById(long rowId) throws DAOException {
        return (Staff) super.find(Staff.class, rowId);
    }

	@SuppressWarnings("unchecked")
	@Override
	public List<Staff> findByCriteria(Staff searchCriteria) throws DAOException {
		DetachedCriteria criteria = DetachedCriteria.forClass(Staff.class);
		
		if(StringUtils.isNotBlank(searchCriteria.getName())) {
			criteria.add(Restrictions.ilike("name", searchCriteria.getName() + "%"));
		}
		
		if(StringUtils.isNotBlank(searchCriteria.getDesignation())) {
			criteria.add(Restrictions.ilike("designation", searchCriteria.getDesignation() + "%"));
		}
		
		return getHibernateTemplate().findByCriteria(criteria); 
	}
 
}
