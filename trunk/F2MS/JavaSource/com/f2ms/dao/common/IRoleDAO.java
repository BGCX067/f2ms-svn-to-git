package com.f2ms.dao.common;

import java.util.List;

import com.f2ms.exception.DAOException;
import com.f2ms.model.Role;

public interface IRoleDAO {

    public List<Role> findAllRoles() throws DAOException;
    public Role create(Role role) throws DAOException;
    public Role edit(Role role) throws DAOException;
    public Role findById(String roleCode) throws DAOException;
    public List<Role> findByCriteria(Role role) throws DAOException;
}
