package com.f2ms.dao.common;

import java.util.Collection;
import java.util.List;

import com.f2ms.exception.DAOException;
import com.f2ms.model.UserRole;

public interface IUserRoleDAO {

    public void create(UserRole userRole) throws DAOException;

    public void edit(UserRole userRole) throws DAOException;
    
    public void delete(UserRole userRole) throws DAOException;
    
    public void deleteAllRoles(Collection<UserRole> roles) throws DAOException;
    
    public List<UserRole> findRolesByUserName(String userName) throws DAOException;
}
