package com.f2ms.dao.common;

import java.util.List;

import com.f2ms.exception.DAOException;
import com.f2ms.model.User;

public interface IUserDAO {

    public User create(User user) throws DAOException;

    public User edit(User user) throws DAOException;

    public List<User> findAllUsers() throws DAOException;

    public User findUserById(long rowId) throws DAOException;

    public User findUserByName(String userName) throws DAOException;
    
    public List<User> findByCriteria(User user) throws DAOException;
}
