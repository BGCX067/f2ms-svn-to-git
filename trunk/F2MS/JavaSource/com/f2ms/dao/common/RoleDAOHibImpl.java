package com.f2ms.dao.common;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

import com.f2ms.dao.AbstractDAOHib;
import com.f2ms.exception.DAOException;
import com.f2ms.model.Role;

public class RoleDAOHibImpl extends AbstractDAOHib<Role> implements IRoleDAO {

    @Override
    public List<Role> findAllRoles() throws DAOException {
        return super.findAll(Role.class);
    }

	@Override
	public Role create(Role role) throws DAOException {
		role.setCreatedOn(new Date());
		role.setChangedOn(new Date());
		
		super.create(role);
		
		return role;
	}

	@Override
	public Role edit(Role role) throws DAOException {
		role.setChangedOn(new Date());
		
		super.edit(role);
		
		return role;
	}

	@Override
	public Role findById(String roleCode) throws DAOException {
		Role role = null;
		
		String hql = "from Role role where role.roleCode = ?";
		
		@SuppressWarnings("unchecked")
		List<Role> listTemp = getHibernateTemplate().find(hql, roleCode);
		
		if(listTemp != null && !listTemp.isEmpty()) {
			role = listTemp.get(0);
		}
		
		return role;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Role> findByCriteria(Role role) throws DAOException {
		DetachedCriteria criteria = DetachedCriteria.forClass(Role.class);
		
		if(StringUtils.isNotBlank(role.getRoleCode())) {
			criteria.add(Restrictions.ilike("roleCode", role.getRoleCode()  + "%"));
		}
		
		if(StringUtils.isNotBlank(role.getRoleDesc())) {
			criteria.add(Restrictions.ilike("roleDesc", role.getRoleDesc()  + "%"));
		}
		
		criteria.addOrder(Property.forName("roleCode").asc());
		
		return getHibernateTemplate().findByCriteria(criteria); 
	}
       
}
