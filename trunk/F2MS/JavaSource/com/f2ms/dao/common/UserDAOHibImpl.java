package com.f2ms.dao.common;

import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

import com.f2ms.dao.AbstractDAOHib;
import com.f2ms.exception.DAOException;
import com.f2ms.model.User;
import com.f2ms.model.UserRole;

public class UserDAOHibImpl extends AbstractDAOHib<User> implements IUserDAO {

    @Override
    public User create(User user) throws DAOException {
    	user.setCreatedOn(new Date());
    	user.setChangedOn(new Date());
    	    	
        Long idNo = (Long) super.create(user);

        User created = user;
        created.setIdNo(idNo);

        return created;
    }

    @Override
    public User edit(User user) throws DAOException {
    	user.setChangedOn(new Date());
    	
        super.edit(user);

        return user;
    }

    @Override
    public List<User> findAllUsers() throws DAOException {
        return super.findAll(User.class);
    }

    @Override
    public User findUserById(long rowId) throws DAOException {
        return (User) super.find(User.class, rowId);
    }

    @Override
    public User findUserByName(String userName) throws DAOException {
        User user = null;

        String hql = "from User user where user.username = ?";
        @SuppressWarnings("unchecked")
        List<User> users = getHibernateTemplate().find(hql, userName);
        if (users != null && !users.isEmpty()) {
            user = users.get(0);

            hql = "from UserRole userRole where userRole.username = ?";
            @SuppressWarnings("unchecked")
            List<UserRole> userRoles = getHibernateTemplate().find(hql, userName);

            user.setRoles(new HashSet<UserRole>(userRoles));
        }

        return user;
    }

	@SuppressWarnings("unchecked")
	@Override
	public List<User> findByCriteria(User user) throws DAOException {
		DetachedCriteria criteria = DetachedCriteria.forClass(User.class);
		
		if(StringUtils.isNotBlank(user.getUsername())) {
			criteria.add(Restrictions.ilike("username", user.getUsername() + "%"));
		}
		
		if(StringUtils.isNotBlank(user.getStatus())) {
			criteria.add(Restrictions.eq("status", user.getStatus()  + "%"));
		}
		
		criteria.addOrder(Property.forName("username").asc());
		
		return getHibernateTemplate().findByCriteria(criteria); 
	}
        
}
