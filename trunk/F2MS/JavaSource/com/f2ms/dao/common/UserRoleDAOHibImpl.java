package com.f2ms.dao.common;

import java.util.Collection;
import java.util.List;

import com.f2ms.dao.AbstractDAOHib;
import com.f2ms.exception.DAOException;
import com.f2ms.model.UserRole;

public class UserRoleDAOHibImpl extends AbstractDAOHib<UserRole> implements IUserRoleDAO {

    @Override
    public void create(UserRole userRole) throws DAOException {
        super.create(userRole);
    }

    @Override
    public void edit(UserRole userRole) throws DAOException {
        super.edit(userRole);
    }

	@Override
	public void delete(UserRole userRole) throws DAOException {		
		super.delete(userRole);
	}

	@Override
	public void deleteAllRoles(Collection<UserRole> roles) throws DAOException {
		super.deleteAll(roles);		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UserRole> findRolesByUserName(String userName)
			throws DAOException {
		String hql = "from UserRole ur where ur.username = ?";
		
		return getHibernateTemplate().find(hql, userName);
	}       
}
