package com.f2ms.dao.common;

import java.util.List;

import com.f2ms.exception.DAOException;
import com.f2ms.model.Staff;

public interface IStaffDAO {

    public Staff create(Staff staff) throws DAOException;

    public Staff edit(Staff staff) throws DAOException;

    public List<Staff> findAllStaff() throws DAOException;

    public Staff findStaffById(long rowId) throws DAOException;
    
    public List<Staff> findByCriteria(Staff searchCriteria) throws DAOException;
}
