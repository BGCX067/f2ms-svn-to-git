package com.f2ms.dao;

import java.util.Collection;
import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.f2ms.exception.DAOException;

public abstract class AbstractDAOHib<T> extends HibernateDaoSupport {

    protected Object create(Object obj) throws DAOException {
        return getHibernateTemplate().save(obj);
    }

    protected Object edit(Object obj) throws DAOException {
        getHibernateTemplate().update(obj);
        return obj;
    }

    protected Object find(Class<T> clazz, Long id) throws DAOException {
        return getHibernateTemplate().load(clazz, id);
    }

    protected Object find(Class<T> clazz, String id) throws DAOException {
        return getHibernateTemplate().load(clazz, id);
    }
    
    protected void delete(Object obj) throws DAOException {
    	getHibernateTemplate().delete(obj);
    }
    
    protected void deleteAll(Collection<T> entities) throws DAOException {
    	getHibernateTemplate().deleteAll(entities); 
    }

    @SuppressWarnings("unchecked")
    protected List<T> findAll(Class<T> clazz) throws DAOException {
        return getHibernateTemplate().find("from " + clazz.getName());
    }
}
