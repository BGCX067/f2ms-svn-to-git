package com.f2ms.dao.shipmentitem;

import java.util.List;

import com.f2ms.exception.DAOException;
import com.f2ms.model.ShipmentItem;

public interface IShipmentItemDAO {

	public List<ShipmentItem> findAllPackagingItems() throws DAOException;

	public ShipmentItem create(ShipmentItem shipmentItem) throws DAOException;

	public ShipmentItem edit(ShipmentItem shipmentItem) throws DAOException;

	public ShipmentItem findPackagingById(Long rowId) throws DAOException;

	public List<ShipmentItem> findPackagingByCriteria(ShipmentItem shipmentItem)
			throws DAOException;

}
