package com.f2ms.enumeration;

public enum Currency implements IBaseEnum {

    SGD(1, "SGD"), USD(2, "USD");
    private int value;
    private String text;

    private Currency(int value, String text) {
        this.value = value;
        this.text = text;
    }

    @Override
    public int getValue() {
        return value;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public String getText(final int value) {
        switch (value) {
            case 1:
                return SGD.getText();
            case 2:
                return USD.getText();
            default:
                return null;
        }
    }
}
