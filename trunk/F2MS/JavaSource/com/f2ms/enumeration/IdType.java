package com.f2ms.enumeration;

public enum IdType implements IBaseEnum {

    NRIC(1, "NRIC"), FIN(2, "FIN"), PASSPORT(3, "Passport");
    private int value;
    private String text;

    IdType(int value, String text) {
        this.value = value;
        this.text = text;
    }

    @Override
    public int getValue() {
        return value;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public String getText(final int value) {
        switch (value) {
            case 1:
                return NRIC.getText();
            case 2:
                return FIN.getText();
            case 3:
                return PASSPORT.getText();
            default:
                return null;
        }
    }
}
