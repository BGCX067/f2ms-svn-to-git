package com.f2ms.enumeration;

public enum PickupStatus implements IBaseEnum {

    NEW(1, "New"), RECEIVED(2, "Received"), PRINTED(3, "Printed"), CANCELLED(4,
    "Cancelled");
    int value;
    String text;

    PickupStatus(int value, String text) {
        this.value = value;
        this.text = text;
    }

    @Override
    public int getValue() {
        return this.value;
    }

    @Override
    public String getText() {
        return this.text;
    }

    @Override
    public String getText(int value) {
        switch (value) {
            case 1:
                return NEW.getText();
            case 2:
                return RECEIVED.getText();
            case 3:
                return PRINTED.getText();
            case 4:
                return CANCELLED.getText();
            default:
                return null;
        }
    }

    public static PickupStatus get(int value) {
        switch (value) {
            case 1:
                return NEW;
            case 2:
                return RECEIVED;
            case 3:
                return PRINTED;
            case 4:
                return CANCELLED;
            default:
                return null;
        }
    }
}
