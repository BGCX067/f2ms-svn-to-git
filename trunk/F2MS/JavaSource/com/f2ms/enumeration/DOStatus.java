package com.f2ms.enumeration;

public enum DOStatus implements IBaseEnum {
	NEW(1, "New"), PRINTED(2, "Printed"), CANCEL(3, "Cancelled");
	
	private int value;
	private String text;
	private DOStatus(int value, String text) {
		this.value = value;
		this.text = text;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	@Override
	public String getText(int value) {
		switch(value) {
			case 1:
				return NEW.getText();
			case 2:
				return PRINTED.getText();
			case 3:
				return CANCEL.getText();
			default:
				return null;
		}
	}
}
