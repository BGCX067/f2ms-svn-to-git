package com.f2ms.enumeration;

public enum DocumentStatus implements IBaseEnum {

	NEW(1, "New"), PRINTED(2, "Printed"), CANCELLED(3, "Cancelled");

	private int value;
	private String text;

	DocumentStatus(int value, String text) {
		this.value = value;
		this.text = text;
	}

	@Override
	public int getValue() {
		return this.value;
	}

	@Override
	public String getText() {
		return this.text;
	}

	@Override
	public String getText(int value) {
		switch (value) {
		case 1:
			return NEW.getText();
		case 2:
			return PRINTED.getText();
		case 3:
			return CANCELLED.getText();
		default:
			return null;
		}
	}

	public static DocumentStatus get(int value) {
		switch (value) {
		case 1:
			return NEW;
		case 2:
			return PRINTED;
		case 3:
			return CANCELLED;
		default:
			return null;
		}
	}
}
