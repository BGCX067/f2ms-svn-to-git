package com.f2ms.enumeration;

public enum ContainerType implements IBaseEnum {
	BC20(1, "20' bulk container"), DC20(2, "20' dry cargo"), FR20(3,
			"20' flat rack fixed ends"), FF20(4, "20' flat rack folding ends"), HC20(
			5, "20' high cube"), OT20(6, "20' open top"), PF20(7,
			"20' platform"), RF20(8, "20' reefer"), TC20(9,
			"20' tank container"), BC40(10, "40' bulk container"), DC40(11,
			"40' dry cargo"), FR40(12, "0' flat rack fixed ends"), FF40(13,
			"40' flat rack folding ends"), HC40(14, "40' high cube"), OT40(15,
			"40' open top"), PF40(16, "40' platform"), RF40(17, "40' reefer"), TC40(
			18, "40' tank container");

	private int value;
	private String text;

	ContainerType(int value, String text) {
		this.text = text;
		this.text = text;
	}

	@Override
	public int getValue() {
		return value;
	}

	@Override
	public String getText() {
		return text;
	}

	@Override
	public String getText(int value) {
		switch (value) {
		case 1:
			return BC20.getText();
		case 2:
			return DC20.getText();
		case 3:
			return FR20.getText();
		case 4:
			return FF20.getText();
		case 5:
			return HC20.getText();
		case 6:
			return OT20.getText();
		case 7:
			return PF20.getText();
		case 8:
			return RF20.getText();
		case 9:
			return TC20.getText();
		case 10:
			return BC40.getText();
		case 11:
			return DC40.getText();
		case 12:
			return FR40.getText();
		case 13:
			return FF40.getText();
		case 14:
			return HC40.getText();
		case 15:
			return OT40.getText();
		case 16:
			return PF40.getText();
		case 17:
			return RF40.getText();
		case 18:
			return TC40.getText();
		default:
			return null;
		}
	}

}
