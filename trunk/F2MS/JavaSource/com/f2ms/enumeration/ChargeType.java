package com.f2ms.enumeration;

public enum ChargeType implements IBaseEnum {
	TYPE_1(1, "Type 1");
	
	private int value;
	private String text;
	
	private ChargeType(int value, String text) {
		this.value = value;
		this.text = text;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String getText(final int value) {
		switch(value) {
			case 1:
				return TYPE_1.getText();
			default:
				return null;
		}
	}
	
	
}
