package com.f2ms.enumeration;

public enum UserStatus implements IBaseEnum {
	ACTIVE(1, "Active"), NOT_ACTIVE(0, "Not Active");
	
	private int value;
	private String text;
	private UserStatus(int value, String text) {
		this.value = value;
		this.text = text;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
	@Override
	public String getText(final int value) {
		switch(value) {
			case 0:
				return NOT_ACTIVE.getText();
			case 1:
				return ACTIVE.getText();
			default:
				return null;
		}
	}
}
