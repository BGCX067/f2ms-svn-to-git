package com.f2ms.enumeration;

public enum TranshipmentVia implements IBaseEnum {

    VIA1(1, "VIA 1");
    private int value;
    private String text;

    private TranshipmentVia(int value, String text) {
        this.value = value;
        this.text = text;
    }

    @Override
    public int getValue() {
        return value;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public String getText(int value) {
        switch (value) {
            case 1:
                return VIA1.getText();
            default:
                return null;
        }

    }
}
