package com.f2ms.enumeration;

public enum BookingStatus implements IBaseEnum {

    NEW(1, "New"), RECEIVED(2, "Received"), RELEASED(3, "Released"), PACKED(4, "Packed"), DELIVERED(5, "Delivered"), CANCELLED(6, "Cancelled");
    private int value;
    private String text;

    private BookingStatus(int value, String text) {
        this.value = value;
        this.text = text;
    }

    @Override
    public int getValue() {
        return value;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public String getText(final int value) {
        switch (value) {
            case 1:
                return NEW.getText();
            case 2:
            	return RECEIVED.getText();
            case 3:
            	return RELEASED.getText();
            case 4:
            	return PACKED.getText();
            case 5:
            	return DELIVERED.getText();
            case 6:
            	return CANCELLED.getText();
            default:
                return null;
        }
    }
}
