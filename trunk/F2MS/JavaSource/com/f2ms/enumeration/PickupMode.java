package com.f2ms.enumeration;

public enum PickupMode implements IBaseEnum {

    CGS(1, "CGS Trucker"), OTHERS(2, "Others Trucker");
    int value;
    String text;

    PickupMode(int value, String text) {
        this.value = value;
        this.text = text;
    }

    @Override
    public int getValue() {
        return value;
    }

    @Override
    public String getText() {
        return this.text;
    }

    @Override
    public String getText(int value) {
        switch (value) {
            case 1:
                return CGS.getText();
            case 2:
                return OTHERS.getText();
            default:
                return null;
        }
    }
}
