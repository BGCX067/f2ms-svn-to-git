package com.f2ms.enumeration;

public enum ShipmentStatus implements IBaseEnum {

   NEW(1, "New"), SHIPPED(2, "Shipped"), PRINTED(3, "Printed"), CANCELLED(4, "Cancelled");

   int value;
   String text;

   ShipmentStatus(int value, String text) {
      this.value = value;
      this.text = text;
   }

   @Override
   public int getValue() {
      return this.value;
   }

   @Override
   public String getText() {
      return this.text;
   }

   @Override
   public String getText(int value) {
      switch (value) {
         case 1:
            return NEW.getText();
         case 2:
            return SHIPPED.getText();
         case 3:
            return PRINTED.getText();
         case 4:
            return CANCELLED.getText();
         default:
            return null;
      }
   }

   public static String get(int value) {
      switch (value) {
         case 1:
            return NEW.getText();
         case 2:
            return SHIPPED.getText();
         case 3:
            return PRINTED.getText();
         case 4:
            return CANCELLED.getText();
         default:
            return null;
      }
   }
}
