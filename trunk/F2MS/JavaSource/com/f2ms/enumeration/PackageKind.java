package com.f2ms.enumeration;


public enum PackageKind implements IBaseEnum {

	AEROSOL(1, "Aerosol"),
	ATOMIZER(2, "Atomizer"),
	BAG(3, "Bag"),
	BAR(4, "Bar"),
	BARREL(5, "Barrel"),
	BASKET(6, "Basket"),
	BIN(7, "Bin"),
	BOARD(8, "Board"),
	BOLT(9, "Bolt"),
	BOX(10, "Box"),
	BUCKET(11, "Bucket");        
    
    private int value;
    private String text;

    private PackageKind(int value, String text) {
        this.value = value;
        this.text = text;
    }

    @Override
    public int getValue() {
        return value;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public String getText(final int value) {
        switch (value) {
            case 1:
                return AEROSOL.getText();
            case 2:
            	return ATOMIZER.getText();
            case 3:
            	return BAG.getText();
            case 4:
            	return BARREL.getText();
            case 5:
            	return BASKET.getText();
            case 6:
            	return BIN.getText();
            case 7:
            	return BOARD.getText();
            case 8:
            	return BOLT.getText();
            case 9:
            	return BOX.getText();
            case 10:
            	return BUCKET.getText();
            default:
                return null;
        }
    }
}
