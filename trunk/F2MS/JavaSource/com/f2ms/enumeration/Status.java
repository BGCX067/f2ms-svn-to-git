package com.f2ms.enumeration;

public enum Status implements IBaseEnum {

    ACTIVE(1, "Active"), INACTIVE(2, "InActive");
    private int value;
    private String text;

    Status(int value, String text) {
        this.value = value;
        this.text = text;
    }

    @Override
    public int getValue() {
        return this.value;
    }

    @Override
    public String getText() {
        return this.text;
    }

    @Override
    public String getText(int value) {
        switch (value) {
            case 1:
                return ACTIVE.getText();
            case 2:
                return INACTIVE.getText();
            default:
                return null;
        }
    }

    public static Status get(int value) {
        switch (value) {
            case 1:
                return ACTIVE;
            case 2:
                return INACTIVE;
            default:
                return null;
        }
    }
}
