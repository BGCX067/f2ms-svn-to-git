package com.f2ms.enumeration;

public interface IBaseEnum {

    int getValue();

    String getText();

    String getText(int value);
}
