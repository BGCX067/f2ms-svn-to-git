package com.f2ms.enumeration;

public enum BookingType implements IBaseEnum {

    INBOUND(1, "Inbound"), OUTBOUND(2, "Outbound");
    private int value;
    private String text;

    private BookingType(int value, String text) {
        this.value = value;
        this.text = text;
    }

    @Override
    public int getValue() {
        return value;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public String getText(final int value) {
        switch (value) {
            case 1:
                return INBOUND.getText();
            case 2:
                return OUTBOUND.getText();
            default:
                return null;
        }

    }
}
