package com.f2ms.enumeration;

public enum ShipmentType implements IBaseEnum {

    LOCAL(1, "Local"), TRANSHIPMENT(2, "Transhipment");
    int value;
    String text;

    ShipmentType(int value, String text) {
        this.value = value;
        this.text = text;
    }

    @Override
    public int getValue() {
        return value;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public String getText(int value) {
        switch (value) {
            case 1:
                return LOCAL.getText();
            case 2:
                return TRANSHIPMENT.getText();
            default:
                return null;
        }
    }
}
