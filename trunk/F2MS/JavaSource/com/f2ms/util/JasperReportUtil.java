package com.f2ms.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;

public class JasperReportUtil {

	private static final String DB_PROPERTIES = "/WEB-INF/database.properties";
	private final static String REPORT_FOLDER = "/WEB-INF/jasper/";

	private static final String JDBC_DRIVER_CLASS_NAME = "jdbc.driverClassName";
	private static final String JDBC_URL = "jdbc.url";
	private static final String JDBC_USERNAME = "jdbc.username";
	private static final String JDBC_PASSWORD = "jdbc.password";

	private static final String REPORT_PICKUP_ORDER = "PickupOrder.jasper";
	private static final String REPORT_RECEIVE_ORDER = "ReceiveOrder.jasper";
	private static final String REPORT_SHIPMENT_COVERSHEET = "ShipmentCoverSheet.jasper";
	private static final String REPORT_EXPORT_CERT = "ExportCertificate.jasper";
	private static final String REPORT_DAILY_SHIPMENT = "DailyShipment.jasper";
	private static final String REPORT_DELIVERY_ORDER = "DeliveryOrder.jasper";
	private static final String REPORT_RELEASE_ORDER = "ReleaseOrder.jasper";

	private static Connection getDBConnection(Properties prop)
			throws SQLException {

		Connection dbConn = null;
		try {
			String driverClass = prop.getProperty(JDBC_DRIVER_CLASS_NAME);
			String url = prop.getProperty(JDBC_URL);
			String user = prop.getProperty(JDBC_USERNAME);
			String password = prop.getProperty(JDBC_PASSWORD);

			Class.forName(driverClass);
			dbConn = DriverManager.getConnection(url, user, password);

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		return dbConn;
	}

	public static void printPickupOrder(Long rowId) throws JRException,
			IOException, SQLException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("ROW_ID", rowId.toString());
		prepareReport(REPORT_PICKUP_ORDER, params);
	}

	public static void printReceiveOrder(Long rowId) throws JRException,
			IOException, SQLException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("ROW_ID", rowId.toString());
		prepareReport(REPORT_RECEIVE_ORDER, params);
	}

	public static void printDeliveryOrder(Long rowId) throws JRException,
			IOException, SQLException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("ROW_ID", rowId);
		prepareReport(REPORT_DELIVERY_ORDER, params);
	}

	public static void printExportCertificate(String rowId) throws JRException,
			IOException, SQLException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("PackagingNo", rowId);
		prepareReport(REPORT_EXPORT_CERT, params);
	}

	public static void printShipmentCoversheet(String rowId)
			throws JRException, IOException, SQLException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("PackagingNo", rowId);
		prepareReport(REPORT_SHIPMENT_COVERSHEET, params);
	}

	public static void printDailyShipment(Integer polId, Integer podId,
			String shipmentType) throws JRException, IOException, SQLException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("POL", polId);
		params.put("POD", podId);
		params.put("SHIPMENTTYPE", shipmentType);

		prepareReport(REPORT_DAILY_SHIPMENT, params);
	}

	public static void prepareReport(String reportname,
			Map<String, Object> params) throws JRException, IOException,
			SQLException {

		Connection dbConn = null;
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ExternalContext externalContext = facesContext.getExternalContext();
		ServletContext servletContext = (ServletContext) externalContext
				.getContext();
		HttpServletResponse response = (HttpServletResponse) externalContext
				.getResponse();

		Properties prop = new Properties();
		FileInputStream fis = new FileInputStream(
				servletContext.getRealPath(DB_PROPERTIES));
		prop.load(fis);
		dbConn = getDBConnection(prop);

		File reportFile = new File(servletContext.getRealPath(REPORT_FOLDER
				+ reportname));
		JasperPrint jasperPrint = JasperFillManager.fillReport(
				reportFile.getPath(), params, dbConn);

		response.setContentType("application/pdf");
		JRExporter exporter = new JRPdfExporter();
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,
				response.getOutputStream());
		exporter.exportReport();

		facesContext.responseComplete();

		if (dbConn != null) {
			dbConn.close();
		}
	}
}
