package com.f2ms.util.validator;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.richfaces.component.html.HtmlCalendar;

/**
 * DateValidator.java
 * <p>
 * This class use to compare 2 date.<br/>
 * Here operator:<br/>
 * <ul>
 * <li>gt for Greater Than</li>
 * <li>lt for Less Than</li>
 * <li>eq for EqualTo</li>
 * <li>gq for Greater and Equal To</li>
 * <li>lq for Less and Equal To</li>
 * <li>nt for Not Equal To</li>
 * </ul>
 * </p>
 * 
 * 
 * @author Isak Rabin
 * 
 */
public class DateValidator implements Validator {
	Logger logger = Logger.getLogger(this.getClass().getName());

	private String operator = "";
	private Date compareWith = new Date();
	private String compareFieldName = "";
	private String comparatorFieldName = "";

	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {

		boolean isValid = true;
		initProps(component, context);
		FacesMessage message = new FacesMessage();
		Date thisDate = null;
		try {
			thisDate = (Date) value;
		} catch (Exception e) {
			logger.log(Level.WARNING,
					"Error in validate method of DateCompareValidator : " + e);
		}

		if (thisDate == null) {
			thisDate = new Date();
		}

		int result = thisDate.compareTo(compareWith);
		String compareData = "";
		String comparerData = "";

		if (compareFieldName != null && compareFieldName.trim().length() != 0) {
			compareData = compareFieldName;
		} else {
			compareData = new SimpleDateFormat("dd/MM/yyyy")
					.format(compareWith);
		}

		if (comparatorFieldName.trim().length() != 0) {
			comparerData = comparatorFieldName;
		} else {
			comparerData = new SimpleDateFormat("dd/MM/yyyy").format(thisDate);
		}

		if (result < 0 && operator.equalsIgnoreCase("ge")) {
			/*
			 * required >= and we found the result less than
			 */
			message.setDetail(comparerData + " should >= " + compareData);
			message.setSummary(comparerData + " should  >= " + compareData);
			isValid = false;
		} else if (result > 0 && operator.equalsIgnoreCase("le")) {
			/*
			 * required <= 0 and we found the result greater than
			 */
			message.setDetail(comparerData + " should  <= " + compareData);
			message.setSummary(comparerData + " should  <= " + compareData);
			isValid = false;
		} else if (result != 0 && operator.equalsIgnoreCase("eq")) {
			/*
			 * 
			 * required == and we found the result not equal
			 */
			message.setDetail(comparerData + " should  == " + compareData);
			message.setSummary(comparerData + " should  == " + compareData);
			isValid = false;

		} else if (result <= 0 && operator.equalsIgnoreCase("gt")) {
			/*
			 * 
			 * required > and we found the result less than equal too
			 */
			message.setDetail(comparerData + " should  > " + compareData);
			message.setSummary(comparerData + " should  > " + compareData);
			isValid = false;
		} else if (result >= 0 && operator.equalsIgnoreCase("lt")) {
			/*
			 * 
			 * required <>0
			 */
			message.setDetail(comparerData + " should  < " + compareData);
			message.setSummary(comparerData + " should  < " + compareData);
			isValid = false;
		} else if (result == 0 && operator.equalsIgnoreCase("nt")) {
			/*
			 * 
			 * required != and we found the result equal too
			 */
			message.setDetail(comparerData + " should  not = " + compareData);
			message.setSummary(comparerData + " should  not = " + compareData);
			isValid = false;
		}

		if (!isValid) {
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(message);
		}

	}

	/*
	 * This method will set the value of both operator and date to compare.
	 */
	private void initProps(UIComponent component, FacesContext context) {
		Object compareWith = component.getAttributes().get("compareWith");

		HtmlCalendar htmlCalendar = (HtmlCalendar) context.getViewRoot()
				.findComponent(compareWith.toString());
		if (htmlCalendar != null) {
			compareWith = htmlCalendar.getValue();
		}

		operator = (String) component.getAttributes().get("operator");
		if (operator == null) {
			operator = ">";
		}

		compareFieldName = (String) component.getAttributes().get(
				"compareFieldName");
		comparatorFieldName = (String) component.getAttributes().get(
				"comparatorFieldName");

		if (compareWith != null) {
			try {
				this.compareWith = (Date) compareWith;
			} catch (Exception e) {
				logger.log(Level.WARNING, "Error in parsing the Date : " + e);
			}
		}
	}
}
