package com.f2ms.util.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import com.f2ms.bean.BaseBean;

public class EmailValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent ui, Object obj)
            throws ValidatorException {
        String enteredEmail = (String) obj;
        //Set the email pattern string
        Pattern p = Pattern.compile(".+@.+\\.[a-z]+");

        //Match the given string with the pattern
        Matcher m = p.matcher(enteredEmail);

        //Check whether match is found
        boolean matchFound = m.matches();

        if (!matchFound) {
            FacesMessage errMsg = new FacesMessage(BaseBean.getMessage("errInvalidEmail", null));
            throw new ValidatorException(errMsg);
        }
    }
}
