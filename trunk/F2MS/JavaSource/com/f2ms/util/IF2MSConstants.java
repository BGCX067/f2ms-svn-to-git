package com.f2ms.util;

public interface IF2MSConstants {

	public static final String ACTIVE_STATUS = "A";
	public static final String INACTIVE_STATUS = "I";
	// User
	public static final String USER_NAME = "username";
	public static final String ROLES = "roles";
	// Roles
	public static final String ROLE_ADMIN = "ROLE_ADMIN";
	public static final String ROLE_STAFF = "ROLE_STAFF";
	public static final String ROLE_MANAGER = "ROLE_MANAGER";
	public static final String ROLE_AGENT = "ROLE_AGENT";
	
	// attachments directory
	public static final String DIR_ATTACHMENTS = "C:/F2MS-UploadedFiles/";
}
