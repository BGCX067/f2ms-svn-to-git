package com.f2ms.util;

import java.io.Serializable;

public class Skin implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6828247225157991160L;
    private String skin;
    private String theme;

    public String getSkin() {
        return skin;
    }

    public void setSkin(String skin) {
        this.skin = skin;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }
}
