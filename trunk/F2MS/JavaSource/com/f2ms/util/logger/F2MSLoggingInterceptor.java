package com.f2ms.util.logger;

import java.util.logging.Logger;

import org.aspectj.lang.JoinPoint;

import com.f2ms.util.F2MSUtil;
import com.f2ms.util.IF2MSConstants;

public class F2MSLoggingInterceptor {

   private static final Logger log = Logger.getLogger(F2MSLoggingInterceptor.class.getName());

   public F2MSLoggingInterceptor() {
      log.info("LoggingInterceptor Initialize");
   }

   public void log(final JoinPoint joinPoint) {

      final String className = joinPoint.getTarget().getClass().getName();
      final String methodName = joinPoint.getSignature().getName();

      String userName = (String) F2MSUtil.getSession().getAttribute(IF2MSConstants.USER_NAME);

      log.severe(userName + " executing method: " + className + "." + methodName + "()");
   }

}