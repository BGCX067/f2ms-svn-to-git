package com.f2ms.util.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.springframework.beans.factory.annotation.Autowired;

import com.f2ms.exception.DAOException;
import com.f2ms.model.ShipmentMode;
import com.f2ms.service.shipmentmode.IShipmentModeService;

public class ShipmentModeConverter implements Converter {

	@Autowired
	private static IShipmentModeService shipmentModeService;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		try {
			return shipmentModeService
					.findShipmentModeById(Long.valueOf(value));
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (DAOException e) {
			e.printStackTrace();
		}

		return value;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		return ((ShipmentMode) value).getId().toString();
	}
}
