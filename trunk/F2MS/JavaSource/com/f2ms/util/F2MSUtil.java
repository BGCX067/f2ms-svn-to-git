package com.f2ms.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

public class F2MSUtil {
	private static final String charset = "!0123456789abcdefghijklmnopqrstuvwxyz";

    public static List<String> getCurrentRoles() {
        List<String> roles = new ArrayList<String>();

        Collection<GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();

        for (Iterator<GrantedAuthority> iterator = authorities.iterator(); iterator.hasNext();) {
            GrantedAuthority grantedAuthority = (GrantedAuthority) iterator.next();
            String role = grantedAuthority.getAuthority();
            roles.add(role);
        }

        return roles;
    }

    public static String getCurrentUserName() {
        String userName = SecurityContextHolder.getContext().getAuthentication().getName();

        return userName;
    }

    public static HttpSession getSession() {
        return (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
    }
    
    public static HttpServletRequest getRequest() {
    	return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getResponse();
    }

    public static Boolean isAdmin() {
        Boolean isAdmin = false;
        if (getCurrentRoles().contains(IF2MSConstants.ROLE_ADMIN)) {
            isAdmin = true;
        }

        return isAdmin;
    }

    public static Boolean isStaff() {
        Boolean isStaff = false;
        if (getCurrentRoles().contains(IF2MSConstants.ROLE_STAFF)) {
            isStaff = true;
        }

        return isStaff;
    }

    public static Boolean isManager() {
        Boolean isManager = false;
        if (getCurrentRoles().contains(IF2MSConstants.ROLE_MANAGER)) {
            isManager = true;
        }

        return isManager;
    }
    
    public static Boolean isAgent() {
    	Boolean isAgent = false;
    	if(getCurrentRoles().contains(IF2MSConstants.ROLE_AGENT)) {
    		isAgent = true;
    	}
    	
    	return isAgent;
    }
    
    public static String getRandomString(int length) {
        Random rand = new Random(System.currentTimeMillis());
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int pos = rand.nextInt(charset.length());
            sb.append(charset.charAt(pos));
        }
        return sb.toString();
    }
}
