package com.f2ms.util;

import java.io.Serializable;

public class Language implements Serializable {

    private static final long serialVersionUID = 2492342947568087861L;
    private String locale;

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public void changeEnglish() {
        locale = "en";
    }

    public void changeBahasa() {
        locale = "in";
    }
}
