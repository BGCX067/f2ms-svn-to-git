package com.f2ms.util;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailUtil {
	public static void sendEmail(String[] recipients, String subject, String msg)
			throws AddressException, MessagingException {
		String host = "smtp.gmail.com";
		int port = 465;
		String userName = "mtechteam07@gmail.com";
		String password = "singapore123";

		Properties props = new Properties();

		props.put("mail.transport.protocol", "smtps");
		props.put("mail.smtps.host", host);
		props.put("mail.smtps.auth", "true");

		if (recipients != null && recipients.length > 0) {
			Session session = Session.getDefaultInstance(props);

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("admin@f2ms.com"));

			InternetAddress[] addressTo = new InternetAddress[recipients.length];
			for (int i = 0; i < recipients.length; i++) {
				addressTo[i] = new InternetAddress(recipients[i]);
			}
			message.setRecipients(Message.RecipientType.TO, addressTo);

			message.setSubject(subject);
			message.setContent(msg, "text/html; charset=ISO-8859-1");

			Transport transport = session.getTransport();
			transport.connect(host, port, userName, password);

			transport.sendMessage(message,
					message.getRecipients(Message.RecipientType.TO));

			transport.close();

			System.out.println("Done");

		}
	}
}
