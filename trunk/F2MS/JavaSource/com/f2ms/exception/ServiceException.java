package com.f2ms.exception;

public class ServiceException extends Exception {
	private static final long serialVersionUID = -67372038729448419L;

	public ServiceException() {
		
	}
	
	public ServiceException(String s) {
		super(s);
	}
	
	public ServiceException(Exception e) {
		super(e);
	}
}
