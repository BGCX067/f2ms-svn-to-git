package com.f2ms.model;


public class Role extends BaseModel {

    private static final long serialVersionUID = -4379767068806594455L;
    private String roleCode;
    private String roleDesc;

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String code) {
        this.roleCode = code;
    }

    public String getRoleDesc() {
        return roleDesc;
    }

    public void setRoleDesc(String desc) {
        this.roleDesc = desc;
    }
}
