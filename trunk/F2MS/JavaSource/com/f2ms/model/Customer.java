/**
 * 
 */
package com.f2ms.model;

import org.apache.commons.lang.StringUtils;

import com.f2ms.enumeration.IdType;

/**
 * @author teguh
 *
 */
public class Customer extends BaseModel {

    private static final long serialVersionUID = -6585964078071563174L;
    private Long custCode;
    private String idType;
    private String idNo;
    private String name;
    private String addressLine1;
    private String addressLine2;
    private String hpTel;
    private String workTel;
    private String email;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getHpTel() {
        return hpTel;
    }

    public void setHpTel(String addressLine3) {
        this.hpTel = addressLine3;
    }

    public String getWorkTel() {
        return workTel;
    }

    public void setWorkTel(String addressLine4) {
        this.workTel = addressLine4;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String addressLine5) {
        this.email = addressLine5;
    }

    public Long getCustCode() {
        return custCode;
    }

    public void setCustCode(Long custCode) {
        this.custCode = custCode;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getFullAddress() {
        String fullAddress = "";

        if (StringUtils.isNotBlank(addressLine1)) {
            fullAddress += addressLine1;
        }

        if (StringUtils.isNotBlank(addressLine2)) {
            fullAddress += " " + addressLine2;
        }

        return fullAddress;
    }

    public String getIdTypeDesc() {
        return IdType.NRIC.getText(Integer.parseInt(idType));
    }
}
