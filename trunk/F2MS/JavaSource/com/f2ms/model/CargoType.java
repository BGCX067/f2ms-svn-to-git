package com.f2ms.model;

import com.f2ms.enumeration.Status;

/**
 * CargoType Model
 * 
 * @author Isak Rabin
 * 
 */
public class CargoType extends BaseModel {

	private static final long serialVersionUID = -750224100015467139L;

	private Long Id;
	private String code;
	private String description;
	private Integer status;
	private String remarks;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getStatusDescription() {
		return Status.get(this.status).getText();
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}
