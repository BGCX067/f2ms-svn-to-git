package com.f2ms.model;

import java.io.Serializable;

public class UserRole implements Serializable {

    private static final long serialVersionUID = 6727071289041271378L;
    private String username;
    private String roleCode;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }
}
