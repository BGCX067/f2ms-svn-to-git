package com.f2ms.model;

import com.f2ms.enumeration.ShipmentType;
import com.f2ms.enumeration.Status;

/**
 * ShipmentMode model
 * 
 * @author Isak Rabin
 * 
 */
public class ShipmentMode extends BaseModel {

	private static final long serialVersionUID = 8996156769077670586L;

	private Long id;
	private String code;
	private String description;
	private Integer status = Status.ACTIVE.getValue();
	private Integer type = ShipmentType.LOCAL.getValue();
	private String remarks;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getStatusDescription() {
		return Status.get(status.intValue()).getText();
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}
