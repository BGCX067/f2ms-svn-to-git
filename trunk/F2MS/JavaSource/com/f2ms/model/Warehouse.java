package com.f2ms.model;

import com.f2ms.enumeration.Status;

/**
 * Warehouse Model
 * 
 * @author Isak
 * 
 */
public class Warehouse extends BaseModel {

	private static final long serialVersionUID = -5383977287473667039L;

	private Long id;
	private String code;
	private String description;
	private String address;
	private Integer status = Integer.valueOf(Status.ACTIVE.getValue());
	private String remarks;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getStatusDescription() {
		return Status.get(status.intValue()).getText();
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}
