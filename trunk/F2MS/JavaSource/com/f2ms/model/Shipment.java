package com.f2ms.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.f2ms.enumeration.ShipmentStatus;

/**
 * Shipment Model
 * 
 * @author Isak Rabin
 * 
 */
public class Shipment extends BaseModel {

	private static final long serialVersionUID = -8090202460993694133L;

	private String packagingNo;
	private Date packagingDate;
	private Integer shipmentType;
	private ShipmentMode shipmentMode = new ShipmentMode();
	private Customer consignee = new Customer();
	private Customer carrier = new Customer();
	private String vesselName;
	private String voyageNo;
	private Location pol = new Location();
	private Location pod = new Location();
	private Date shipmentDate;
	private String containerNo;
	private CargoType cargoType = new CargoType();
	private Double maximumLoad;
	private Double maximumMeasurement;
	private Double totalVolume;
	private Double totalWeight;
	private Integer status = ShipmentStatus.NEW.getValue();
	private String statusReason;
	private String remarks;
	private Set<ShipmentItem> shipmentItems = new HashSet<ShipmentItem>(0);

	public String getPackagingNo() {
		return packagingNo;
	}

	public void setPackagingNo(String packagingNo) {
		this.packagingNo = packagingNo;
	}

	public Date getPackagingDate() {
		return packagingDate;
	}

	public void setPackagingDate(Date packagingDate) {
		this.packagingDate = packagingDate;
	}

	public Integer getShipmentType() {
		return shipmentType;
	}

	public void setShipmentType(Integer shipmentType) {
		this.shipmentType = shipmentType;
	}

	public ShipmentMode getShipmentMode() {
		return shipmentMode;
	}

	public void setShipmentMode(ShipmentMode shipmentMode) {
		this.shipmentMode = shipmentMode;
	}

	public Customer getConsignee() {
		return consignee;
	}

	public void setConsignee(Customer consignee) {
		this.consignee = consignee;
	}

	public Customer getCarrier() {
		return carrier;
	}

	public void setCarrier(Customer carrier) {
		this.carrier = carrier;
	}

	public String getVesselName() {
		return vesselName;
	}

	public void setVesselName(String vesselName) {
		this.vesselName = vesselName;
	}

	public String getVoyageNo() {
		return voyageNo;
	}

	public void setVoyageNo(String voyageNo) {
		this.voyageNo = voyageNo;
	}

	public Location getPol() {
		return pol;
	}

	public Location getPod() {
		return pod;
	}

	public void setPol(Location pol) {
		this.pol = pol;
	}

	public void setPod(Location pod) {
		this.pod = pod;
	}

	public Date getShipmentDate() {
		return shipmentDate;
	}

	public void setShipmentDate(Date shipmentDate) {
		this.shipmentDate = shipmentDate;
	}

	public String getContainerNo() {
		return containerNo;
	}

	public void setContainerNo(String containerNo) {
		this.containerNo = containerNo;
	}

	public CargoType getCargoType() {
		return cargoType;
	}

	public void setCargoType(CargoType cargoType) {
		this.cargoType = cargoType;
	}

	public Double getMaximumLoad() {
		return maximumLoad;
	}

	public void setMaximumLoad(Double maximumLoad) {
		this.maximumLoad = maximumLoad;
	}

	public Double getMaximumMeasurement() {
		return maximumMeasurement;
	}

	public void setMaximumMeasurement(Double maximumMeasurement) {
		this.maximumMeasurement = maximumMeasurement;
	}

	public Double getTotalVolume() {
		return totalVolume;
	}

	public void setTotalVolume(Double totalVolume) {
		this.totalVolume = totalVolume;
	}

	public Double getTotalWeight() {
		return totalWeight;
	}

	public void setTotalWeight(Double totalWeight) {
		this.totalWeight = totalWeight;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getStatusDescription() {
		return ShipmentStatus.get(status);
	}

	public String getStatusReason() {
		return statusReason;
	}

	public void setStatusReason(String statusReason) {
		this.statusReason = statusReason;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Set<ShipmentItem> getShipmentItems() {
		return shipmentItems;
	}

	public void setShipmentItems(Set<ShipmentItem> shipmentItems) {
		this.shipmentItems = shipmentItems;
	}

}
