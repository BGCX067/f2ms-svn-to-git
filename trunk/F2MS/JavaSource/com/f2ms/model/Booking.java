package com.f2ms.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.f2ms.enumeration.BookingStatus;
import com.f2ms.enumeration.ShipmentType;
import com.f2ms.enumeration.TranshipmentVia;

public class Booking extends BaseModel {

	private static final long serialVersionUID = 2594122046627041657L;
	private String docNo;
	private String shipmentType;
	private String transhipmentVia;
	private Long shipperId;
	private Long consigneeId;
	private Long placeOfReceiptId;
	private Long polId;
	private Long podId;
	private Long finalDestinationId;
	private String goodsDesc;
	private String markAndNo;
	private Integer innerPkgNo;
	private Integer innerPkgKind;
	private Integer outerPkgNo;
	private Integer outerPkgKind;
	private Double grossWeight;
	private Double measurement;
	private String hsCode;
	private String permitNo;
	private String remark;
	private String status;
	private String currencyId;
	private String bookingType;
	private Integer needPickup;
	private String shipperName;
	private String consigneeName;
	private String placeOfReceiptDesc;
	private String polDesc;
	private String podDesc;
	private String finalDestinationDesc;
	private Long shipmentMode;
	private Double goodsValues;
	private Integer goodsValuesCurr;
	private Long warehouse;	
	
	private List<BookingFiles> listFiles = new ArrayList<BookingFiles>();
	
	private Customer shipperObj;
	private Customer consigneeObj;
	private Location placeOfReceiptObj;
	private Location polObj;
	private Location podObj;
	private Location finalDestinationObj;
	private ShipmentMode shipmentModeObj;
	private Warehouse warehouseObj;

	public String getShipmentType() {
		return shipmentType;
	}

	public void setShipmentType(String shipmentType) {
		this.shipmentType = shipmentType;
	}

	public String getTranshipmentVia() {
		return transhipmentVia;
	}

	public void setTranshipmentVia(String transhipmentVia) {
		this.transhipmentVia = transhipmentVia;
	}

	public String getGoodsDesc() {
		return goodsDesc;
	}

	public void setGoodsDesc(String goodsDesc) {
		this.goodsDesc = goodsDesc;
	}

	public String getMarkAndNo() {
		return markAndNo;
	}

	public void setMarkAndNo(String markAndNo) {
		this.markAndNo = markAndNo;
	}

	public String getHsCode() {
		return hsCode;
	}

	public void setHsCode(String hsCode) {
		this.hsCode = hsCode;
	}

	public String getPermitNo() {
		return permitNo;
	}

	public void setPermitNo(String permitNo) {
		this.permitNo = permitNo;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getStatus() {
		return status;
	}
	
	public String getStatusDesc() {
		String statusDesc = "";
		
		if(StringUtils.isNotBlank(status)) { 
			statusDesc = BookingStatus.NEW.getText(Integer.parseInt(status));
		}
		
		return statusDesc;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(String currencyId) {
		this.currencyId = currencyId;
	}

	public Long getShipperId() {
		return shipperId;
	}

	public void setShipperId(Long shipperId) {
		this.shipperId = shipperId;
	}

	public Long getConsigneeId() {
		return consigneeId;
	}

	public void setConsigneeId(Long consigneeId) {
		this.consigneeId = consigneeId;
	}

	public Long getPlaceOfReceiptId() {
		return placeOfReceiptId;
	}

	public void setPlaceOfReceiptId(Long placeOfReceiptId) {
		this.placeOfReceiptId = placeOfReceiptId;
	}

	public Long getPolId() {
		return polId;
	}

	public void setPolId(Long polId) {
		this.polId = polId;
	}

	public Long getPodId() {
		return podId;
	}

	public void setPodId(Long podId) {
		this.podId = podId;
	}

	public Long getFinalDestinationId() {
		return finalDestinationId;
	}

	public void setFinalDestinationId(Long finalDestinationId) {
		this.finalDestinationId = finalDestinationId;
	}

	public Integer getInnerPkgNo() {
		return innerPkgNo;
	}

	public void setInnerPkgNo(Integer innerPkgNo) {
		this.innerPkgNo = innerPkgNo;
	}

	public Integer getInnerPkgKind() {
		return innerPkgKind;
	}

	public void setInnerPkgKind(Integer innerPkgKind) {
		this.innerPkgKind = innerPkgKind;
	}

	public Integer getOuterPkgNo() {
		return outerPkgNo;
	}

	public void setOuterPkgNo(Integer outerPkgNo) {
		this.outerPkgNo = outerPkgNo;
	}

	public Integer getOuterPkgKind() {
		return outerPkgKind;
	}

	public void setOuterPkgKind(Integer outerPkgKind) {
		this.outerPkgKind = outerPkgKind;
	}

	public Double getGrossWeight() {
		return grossWeight;
	}

	public void setGrossWeight(Double grossWeight) {
		this.grossWeight = grossWeight;
	}

	public Double getMeasurement() {
		return measurement;
	}

	public void setMeasurement(Double measurement) {
		this.measurement = measurement;
	}

	public Integer getNeedPickup() {
		return needPickup;
	}

	public void setNeedPickup(Integer needPickup) {
		this.needPickup = needPickup;
	}

	public String getShipmentTypeDesc() {
		String shipmentTypeDesc = "";

		if (StringUtils.isNotBlank(shipmentType)) {
			shipmentTypeDesc = ShipmentType.LOCAL.getText(Integer
					.parseInt(shipmentType));
		}

		return shipmentTypeDesc;
	}

	public String getTranshipmentViaDesc() {
		String transhipmentViaDesc = "";

		if (StringUtils.isNotBlank(transhipmentVia)) {
			transhipmentViaDesc = TranshipmentVia.VIA1.getText(Integer
					.parseInt(transhipmentVia));
		}

		return transhipmentViaDesc;
	}

	public String getBookingStatusDesc() {
		String bookingStatusDesc = "";

		if (StringUtils.isNotBlank(status)) {
			bookingStatusDesc = BookingStatus.NEW.getText(Integer
					.parseInt(status));
		}

		return bookingStatusDesc;
	}

	public String getDocNo() {
		return docNo;
	}

	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}

	public String getShipperName() {
		return shipperName;
	}

	public void setShipperName(String shipperName) {
		this.shipperName = shipperName;
	}

	public String getConsigneeName() {
		return consigneeName;
	}

	public void setConsigneeName(String consigneeName) {
		this.consigneeName = consigneeName;
	}

	public String getBookingType() {
		return bookingType;
	}

	public void setBookingType(String bookingType) {
		this.bookingType = bookingType;
	}

	public String getPlaceOfReceiptDesc() {
		return placeOfReceiptDesc;
	}

	public void setPlaceOfReceiptDesc(String placeOfReceiptDesc) {
		this.placeOfReceiptDesc = placeOfReceiptDesc;
	}

	public String getPolDesc() {
		return polDesc;
	}

	public void setPolDesc(String polDesc) {
		this.polDesc = polDesc;
	}

	public String getPodDesc() {
		return podDesc;
	}

	public void setPodDesc(String podDesc) {
		this.podDesc = podDesc;
	}

	public String getFinalDestinationDesc() {
		return finalDestinationDesc;
	}

	public void setFinalDestinationDesc(String finalDestinationDesc) {
		this.finalDestinationDesc = finalDestinationDesc;
	}	

	public Long getShipmentMode() {
		return shipmentMode;
	}

	public void setShipmentMode(Long shipmentMode) {
		this.shipmentMode = shipmentMode;
	}

	public Double getGoodsValues() {
		return goodsValues;
	}

	public void setGoodsValues(Double goodsValues) {
		this.goodsValues = goodsValues;
	}

	public Integer getGoodsValuesCurr() {
		return goodsValuesCurr;
	}

	public void setGoodsValuesCurr(Integer goodsValuesCurr) {
		this.goodsValuesCurr = goodsValuesCurr;
	}

	public Long getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(Long warehouse) {
		this.warehouse = warehouse;
	}

	public Customer getShipperObj() {
		return shipperObj;
	}

	public void setShipperObj(Customer shipperObj) {
		this.shipperObj = shipperObj;
	}

	public Customer getConsigneeObj() {
		return consigneeObj;
	}

	public void setConsigneeObj(Customer consigneeObj) {
		this.consigneeObj = consigneeObj;
	}

	public Location getPlaceOfReceiptObj() {
		return placeOfReceiptObj;
	}

	public void setPlaceOfReceiptObj(Location placeOfReceiptObj) {
		this.placeOfReceiptObj = placeOfReceiptObj;
	}

	public Location getPolObj() {
		return polObj;
	}

	public void setPolObj(Location polObj) {
		this.polObj = polObj;
	}

	public Location getPodObj() {
		return podObj;
	}

	public void setPodObj(Location podObj) {
		this.podObj = podObj;
	}

	public Location getFinalDestinationObj() {
		return finalDestinationObj;
	}

	public void setFinalDestinationObj(Location finalDestinationObj) {
		this.finalDestinationObj = finalDestinationObj;
	}

	public ShipmentMode getShipmentModeObj() {
		return shipmentModeObj;
	}

	public void setShipmentModeObj(ShipmentMode shipmentModeObj) {
		this.shipmentModeObj = shipmentModeObj;
	}

	public Warehouse getWarehouseObj() {
		return warehouseObj;
	}

	public void setWarehouseObj(Warehouse warehouseObj) {
		this.warehouseObj = warehouseObj;
	}

	public List<BookingFiles> getListFiles() {
		return listFiles;
	}

	public void setListFiles(List<BookingFiles> listFiles) {
		this.listFiles = listFiles;
	}	
		
}
