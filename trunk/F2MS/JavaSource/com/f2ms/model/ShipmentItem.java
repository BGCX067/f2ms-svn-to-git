package com.f2ms.model;

import java.util.Date;

public class ShipmentItem extends BaseModel {

	private static final long serialVersionUID = -1110797960948734247L;

	private Long id;
	private Shipment shipment;
	private Booking booking;
	private Date exportCertDate;
	private String shipmentPermitNo;
	private String exportCertNo;
	private String marking;
	private String description;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Shipment getShipment() {
		return shipment;
	}

	public void setShipment(Shipment shipment) {
		this.shipment = shipment;
	}

	public Booking getBooking() {
		return booking;
	}

	public void setBooking(Booking booking) {
		this.booking = booking;
	}

	public Date getExportCertDate() {
		return exportCertDate;
	}

	public void setExportCertDate(Date exportCertDate) {
		this.exportCertDate = exportCertDate;
	}

	public String getShipmentPermitNo() {
		return shipmentPermitNo;
	}

	public void setShipmentPermitNo(String shipmentPermitNo) {
		this.shipmentPermitNo = shipmentPermitNo;
	}

	public String getExportCertNo() {
		return exportCertNo;
	}

	public void setExportCertNo(String exportCertNo) {
		this.exportCertNo = exportCertNo;
	}

	public String getMarking() {
		return marking;
	}

	public void setMarking(String marking) {
		this.marking = marking;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((booking == null) ? 0 : booking.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((shipment == null) ? 0 : shipment.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ShipmentItem other = (ShipmentItem) obj;
		if (booking == null) {
			if (other.booking != null)
				return false;
		} else if (!booking.equals(other.booking))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (shipment == null) {
			if (other.shipment != null)
				return false;
		} else if (!shipment.equals(other.shipment))
			return false;
		return true;
	}

}
