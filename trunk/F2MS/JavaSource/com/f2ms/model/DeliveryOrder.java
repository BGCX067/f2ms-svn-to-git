package com.f2ms.model;

import java.util.Date;

import com.f2ms.enumeration.DOStatus;

public class DeliveryOrder extends BaseModel {

	private static final long serialVersionUID = 5134798425203543334L;
	
	private Long id;
	private String bookingNo;
	private String docNo;
	private Date docDate;
	private Integer truckerId;
	private Integer deliveryTo;
	private String truckerName;
	private String deliveryToName;
	private Integer status;
	private String statusReason;	
	private String deliveryAddr1;
	private String deliveryAddr2;
	private String deliveryAddr3;
	private String deliveryAddr4;
	private String remarks;
	
	private Customer truckerObj;
	private Customer deliveryToObj;		
		
	public Customer getTruckerObj() {
		return truckerObj;
	}
	public void setTruckerObj(Customer truckerObj) {
		this.truckerObj = truckerObj;
	}
	public Customer getDeliveryToObj() {
		return deliveryToObj;
	}
	public void setDeliveryToObj(Customer deliveryToObj) {
		this.deliveryToObj = deliveryToObj;
	}
	public String getTruckerName() {
		return truckerName;
	}
	public void setTruckerName(String truckerName) {
		this.truckerName = truckerName;
	}
	public String getDeliveryToName() {
		return deliveryToName;
	}
	public void setDeliveryToName(String deliveryToName) {
		this.deliveryToName = deliveryToName;
	}
	public String getDeliveryAddr3() {
		return deliveryAddr3;
	}
	public void setDeliveryAddr3(String deliveryAddr3) {
		this.deliveryAddr3 = deliveryAddr3;
	}
	public String getDeliveryAddr4() {
		return deliveryAddr4;
	}
	public void setDeliveryAddr4(String deliveryAddr4) {
		this.deliveryAddr4 = deliveryAddr4;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDocNo() {
		return docNo;
	}
	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}
	public Date getDocDate() {
		return docDate;
	}
	public void setDocDate(Date docDate) {
		this.docDate = docDate;
	}
	public Integer getTruckerId() {
		return truckerId;
	}
	public void setTruckerId(Integer truckerId) {
		this.truckerId = truckerId;
	}
	public Integer getDeliveryTo() {
		return deliveryTo;
	}
	public void setDeliveryTo(Integer shipmentMode) {
		this.deliveryTo = shipmentMode;
	}
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getStatusReason() {
		return statusReason;
	}
	public void setStatusReason(String statusReason) {
		this.statusReason = statusReason;
	}	
	public String getDeliveryAddr1() {
		return deliveryAddr1;
	}
	public void setDeliveryAddr1(String deliveryAddr1) {
		this.deliveryAddr1 = deliveryAddr1;
	}
	public String getDeliveryAddr2() {
		return deliveryAddr2;
	}
	public void setDeliveryAddr2(String deliveryAddr2) {
		this.deliveryAddr2 = deliveryAddr2;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getBookingNo() {
		return bookingNo;
	}
	public void setBookingNo(String bookingNo) {
		this.bookingNo = bookingNo;
	}
	public String getStatusDesc() {
		String statusDesc = "";
		
		if(status != null) {
			statusDesc = DOStatus.NEW.getText(status);
		}
		
		return statusDesc;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((bookingNo == null) ? 0 : bookingNo.hashCode());
		result = prime * result
				+ ((deliveryAddr1 == null) ? 0 : deliveryAddr1.hashCode());
		result = prime * result
				+ ((deliveryAddr2 == null) ? 0 : deliveryAddr2.hashCode());
		result = prime * result + ((docDate == null) ? 0 : docDate.hashCode());
		result = prime * result + ((docNo == null) ? 0 : docNo.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		
		result = prime * result + ((remarks == null) ? 0 : remarks.hashCode());
		result = prime * result
				+ ((deliveryTo == null) ? 0 : deliveryTo.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result
				+ ((statusReason == null) ? 0 : statusReason.hashCode());
		result = prime * result
				+ ((truckerId == null) ? 0 : truckerId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		DeliveryOrder other = (DeliveryOrder) obj;
		if (bookingNo == null) {
			if (other.bookingNo != null)
				return false;
		} else if (!bookingNo.equals(other.bookingNo))
			return false;
		if (deliveryAddr1 == null) {
			if (other.deliveryAddr1 != null)
				return false;
		} else if (!deliveryAddr1.equals(other.deliveryAddr1))
			return false;
		if (deliveryAddr2 == null) {
			if (other.deliveryAddr2 != null)
				return false;
		} else if (!deliveryAddr2.equals(other.deliveryAddr2))
			return false;
		if (docDate == null) {
			if (other.docDate != null)
				return false;
		} else if (!docDate.equals(other.docDate))
			return false;
		if (docNo == null) {
			if (other.docNo != null)
				return false;
		} else if (!docNo.equals(other.docNo))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;		
		if (remarks == null) {
			if (other.remarks != null)
				return false;
		} else if (!remarks.equals(other.remarks))
			return false;
		if (deliveryTo == null) {
			if (other.deliveryTo != null)
				return false;
		} else if (!deliveryTo.equals(other.deliveryTo))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (statusReason == null) {
			if (other.statusReason != null)
				return false;
		} else if (!statusReason.equals(other.statusReason))
			return false;
		if (truckerId == null) {
			if (other.truckerId != null)
				return false;
		} else if (!truckerId.equals(other.truckerId))
			return false;
		return true;
	}
}


