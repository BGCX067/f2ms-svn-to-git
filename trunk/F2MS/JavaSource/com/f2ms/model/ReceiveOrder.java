package com.f2ms.model;

import java.util.Date;

import com.f2ms.enumeration.DocumentStatus;

public class ReceiveOrder extends BaseModel {

	private static final long serialVersionUID = -8552525766126525722L;

	private Long id;
	private String docNo;
	private Date docDate;
	private Booking booking = new Booking();
	private Customer receivedFrom = new Customer();
	private Customer receivedBy = new Customer();
	private String description;
	private Customer deliveredTo = new Customer();
	private String deliveredAddressLine;
	private String shipperReferenceNo;
	private Integer quantity;
	private Double volume;
	private Double weight;
	private String marking;
	private String remarks;
	private Integer status = DocumentStatus.NEW.getValue();
	private String statusReason;
	private Double height;
	private Double length;
	private Double width;

	public Booking getBooking() {
		return booking;
	}

	public void setBooking(Booking booking) {
		this.booking = booking;
	}

	public String getDeliveredAddressLine() {
		return deliveredAddressLine;
	}

	public void setDeliveredAddressLine(String deliveredAddressLine) {
		this.deliveredAddressLine = deliveredAddressLine;
	}

	public Customer getDeliveredTo() {
		return deliveredTo;
	}

	public void setDeliveredTo(Customer deliveredTo) {
		this.deliveredTo = deliveredTo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDocDate() {
		return docDate;
	}

	public void setDocDate(Date docDate) {
		this.docDate = docDate;
	}

	public String getDocNo() {
		return docNo;
	}

	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMarking() {
		return marking;
	}

	public void setMarking(String marking) {
		this.marking = marking;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Customer getReceivedBy() {
		return receivedBy;
	}

	public void setReceivedBy(Customer receivedBy) {
		this.receivedBy = receivedBy;
	}

	public Customer getReceivedFrom() {
		return receivedFrom;
	}

	public void setReceivedFrom(Customer receivedFrom) {
		this.receivedFrom = receivedFrom;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getShipperReferenceNo() {
		return shipperReferenceNo;
	}

	public void setShipperReferenceNo(String shipperReferenceNo) {
		this.shipperReferenceNo = shipperReferenceNo;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getStatusReason() {
		return statusReason;
	}

	public void setStatusReason(String statusReason) {
		this.statusReason = statusReason;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Double getHeight() {
		return height;
	}

	public void setHeight(Double height) {
		this.height = height;
	}

	public Double getLength() {
		return length;
	}

	public void setLength(Double length) {
		this.length = length;
	}

	public Double getWidth() {
		return width;
	}

	public void setWidth(Double width) {
		this.width = width;
	}

}
