package com.f2ms.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.f2ms.enumeration.UserStatus;

public class User extends BaseModel {

    private static final long serialVersionUID = -4972262824461800881L;
    private Set<UserRole> roles = new HashSet<UserRole>(0);
    private Long idNo;
    private String username;
    private String password;
    private String confirmPassword;
    private Staff staffObj;
    private String staffCode;
    private String status;
    private Boolean admin;
    private Boolean staff;
    private Boolean manager;
    private Boolean agent;
    private List<String> selectedRoles;
    
    public Long getIdNo() {
		return idNo;
	}

	public void setIdNo(Long idNo) {
		this.idNo = idNo;
	}

	public Set<UserRole> getRoles() {
        return roles;
    }

    public void setRoles(Set<UserRole> roles) {
        this.roles = roles;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }   

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    public Boolean getStaff() {
        return staff;
    }

    public void setStaff(Boolean staff) {
        this.staff = staff;
    }

    public Boolean getManager() {
        return manager;
    }

    public void setManager(Boolean manager) {
        this.manager = manager;
    }        

    public Boolean getAgent() {
		return agent;
	}

	public void setAgent(Boolean agent) {
		this.agent = agent;
	}

    public String getUsername() {
        return username;
    }

    public boolean isAccountNonExpired() {
        return true;
    }

    public boolean isAccountNonLocked() {
        return true;
    }
    
    public boolean isCredentialsNonExpired() {
        return true;
    }

    public boolean isEnabled() {
    	boolean isActive = false;
    	
    	if(StringUtils.equals("1", status)) { 
    		isActive = true;
    	}
    	    	
        return isActive;
    }	
	
	public Staff getStaffObj() {
		return staffObj;
	}

	public void setStaffObj(Staff staffCode) {
		this.staffObj = staffCode;
	}

	public String getStatusDesc() {
		String statusDesc = "";
		
		if(StringUtils.isNotBlank(status)) {
			statusDesc = UserStatus.ACTIVE.getText(Integer.parseInt(status));
		}
		
		return statusDesc;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public List<String> getSelectedRoles() {
		return selectedRoles;
	}

	public void setSelectedRoles(List<String> selectedRoles) {
		this.selectedRoles = selectedRoles;
	}	
    	
}
