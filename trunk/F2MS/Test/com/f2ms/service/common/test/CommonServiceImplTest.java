package com.f2ms.service.common.test;


import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.f2ms.exception.DAOException;
import com.f2ms.model.Staff;
import com.f2ms.model.User;
import com.f2ms.model.UserRole;
import com.f2ms.service.common.ICommonService;

@RunWith(SpringJUnit4ClassRunner.class) 
@ContextConfiguration(locations = {"classpath:WEB-INF/spring-beans.xml"})
@TransactionConfiguration
public class CommonServiceImplTest extends AbstractTransactionalJUnit4SpringContextTests {
	
	@Autowired
	private ICommonService commonService;
	
	@Autowired
	private UserDetailsService userDetailsService;	
	
	@Transactional
	public void testInsertStaff() {
		Staff staff = new Staff();
		
		try {
			commonService.createStaff(staff);
		} catch (DAOException e) {			
			e.printStackTrace();
		}
	}
	
	
	public void testInsertUser() {
		User user = new User();
		user.setUsername("staff001");
		user.setPassword("test");
		user.setStaffCode("STAFF001");
		user.setStatus("A");				
		
		UserRole admin = new UserRole();
		admin.setRoleCode("ROLE_ADMIN");
		admin.setUsername("staff001");
		
		UserRole staff = new UserRole();
		staff.setRoleCode("ROLE_STAFF");
		staff.setUsername("staff001");
		
		Set<UserRole> roles = new HashSet<UserRole>();
		roles.add(admin);
		roles.add(staff);
		
		user.setRoles(roles);
		
		try {
			User created = commonService.createUser(user);						
			
		} catch (DAOException e) {
			
			e.printStackTrace();
		}
	}
	
	@Test
	public void testFindUserByName() {
		UserDetails user = userDetailsService.loadUserByUsername("test");
		
		Collection<GrantedAuthority> col = user.getAuthorities();
		 
		System.out.println(user.getUsername());
	}
	
//	@Test
	public void testFindUserById() {
		try {
			User user = commonService.findUserById(32);
			
			System.out.println(user.getUsername());
		} catch (DAOException e) {			
			e.printStackTrace();
		}				
	}	
}
